
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Kudrat Sidhu | Portfolio</title>



  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
  <!-- font for icons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


  <!-- Theme CSS - Includes Bootstrap -->
  <link href="css/creative.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="#page-top"><img src="img/logo1.png" alt="logo"></a>
      
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto my-2 my-lg-0">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#about">About</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#services">My Work</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Masthead -->
  <header class="masthead">
    <div class="container h-100">
      <div class="row h-100 align-items-center justify-content-center text-center">
        <div class="col-lg-10 align-self-end">
          <h1 class="text-uppercase text-white font-weight-bold">Creative Web Developer</h1>
          <hr class="divider my-4">
        </div>
        <div class="col-lg-8 align-self-baseline">
          <p class="text-white-75 font-weight-light mb-5">I believe Improvement begins with "I"</p>
          <a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">Find Out More</a>
        </div>
      </div>
    </div>
  </header>

  <!-- About Section -->
  <section class="page-section bg-primary" id="about">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8 text-center">
          <h2 class="text-white mt-0" style="font-size: 2.9rem">KUDRAT SIDHU</h2>
          <h3 class="text-white mt-0" style="font-size: 1.4rem">Full-Stack Web Developer</h3>
          <hr class="divider light my-4">
          <p class="text-white-50 mb-4" >Charismatic and Creative Web Developer
          <br/> My best assets are my creativity, my feeling with designing and my passion. 
          </p>
        
        </div>
      </div>
    </div>
  </section>

  <!-- Services Section -->
  <section class="page-section" id="services">
    <div class="container">
      <h2 class="text-center mt-0">My Skills</h2>
      <hr class="divider my-4">
      <div class="row">
        <div class="col-lg-3 col-md-6 text-center">
          <div class="mt-5">
            <i class="fas fa-4x fa-gem text-primary mb-4"></i>
            <h3 class="h4 mb-2">Technical Skills</h3>
            <p class="text-muted mb-0">PHP & MySQL,HTML,CSS3,
            Javascript,jQuery,<br/>core-Java,Bootstrap,Android.</p>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 text-center">
          <div class="mt-5">
            <i class="fas fa-4x fa-laptop-code text-primary mb-4"></i>
            <h3 class="h4 mb-2">Area of Knowledge</h3>
            <p class="text-muted mb-0">MVC Framework(LARAVEL), Vue,
              Custom Theme Development in WORD PRESS,Basic SOLARIS Commands, Vanilla AJAX, <br/>Web-Security, Networking,
            SDLC,XML,XSL</p>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 text-center">
          <div class="mt-5">
            <h3 class="h4 mb-2">Soft Skills</h3>
            <p class="text-muted mb-0"> Effective interpersonal and communication skills.<br/> Superpowers organizational skills paired up with my creativity.<br/>Extrovert and Multilingual. 
 
  </p>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 text-center">
          <div class="mt-5">
            <h3 class="h4 mb-2">Technology Stack</h3>
            <p class="text-muted mb-0">Adobe creative suite(Photoshop, Animate CC, Dreamweaver), Proficient with Microsoft office suite, GitHub, BitBucket.</p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- Portfolio Section -->
  <section id="portfolio">
    <div class="container-fluid p-0">
      <div class="row no-gutters">
        <div class="col-lg-4 col-sm-6">
          <a class="portfolio-box" href="http://wddkudrat.com/pizzatime/public/index.php">
            <img class="img-fluid" src="img/portfolio/thumbnails/18.jpg" alt="">
            <div class="portfolio-box-caption">
              <div class="project-category text-white-50">
                 E-commerce Group Project
              </div>
              <div class="project-name">
                PizzaTime
              </div>
            </div>
          </a>
        </div>
          <div class="col-lg-4 col-sm-6">
          <a class="portfolio-box" href="http://wddkudrat.com/Project/index.html">
            <img class="img-fluid" src="img/portfolio/thumbnails/20.jpg" alt="">
            <div class="portfolio-box-caption">
              <div class="project-category text-white-50">
                Practicing Materialize Css
              </div>
              <div class="project-name">
                Art Attack
              </div>
            </div>
          </a>
        </div>
          <div class="col-lg-4 col-sm-6">
          <a class="portfolio-box" href="http://wddkudrat.com/Html%20project/index.html">
            <img class="img-fluid" src="img/portfolio/thumbnails/21.jpg" alt="">
            <div class="portfolio-box-caption">
              <div class="project-category text-white-50">
                Practicing HTML-CSS 
              </div>
              <div class="project-name">
                TineyRoots
              </div>
            </div>
          </a>
        </div>
         <div class="col-lg-4 col-sm-6">
          <a class="portfolio-box" href="http://wddkudrat.com/pizzatime/docs/Team2_Final_Proposal.pdf">
            <img class="img-fluid" src="img/portfolio/thumbnails/19.jpg" alt="">
            <div class="portfolio-box-caption">
              <div class="project-category text-white-50">
                E-commerce Request For Proposal
              </div>
              <div class="project-name">
                PizzaTime
              </div>
            </div>
          </a>
        </div>
        <div class="col-lg-4 col-sm-6">
          <a class="portfolio-box" href="http://wddkudrat.com/Slider/slider.html">
            <img class="img-fluid" src="img/portfolio/thumbnails/12.jpg" alt="">
            <div class="portfolio-box-caption">
              <div class="project-category text-white-50">
                 Javascript and jQuerry Assignment
              </div>
              <div class="project-name">
                 Slider
              </div>
            </div>
          </a>
        </div>
        <div class="col-lg-4 col-sm-6">
          <a class="portfolio-box" href="http://capstone.wddkudrat.com/">
            <img class="img-fluid" src="img/portfolio/thumbnails/7.jpg" alt="">
            <div class="portfolio-box-caption">
              <div class="project-category text-white-50">
                PHP-CAPSTONE
              </div>
              <div class="project-name">
                Helping Hands Home Maintenance
              </div>
            </div>
          </a>
        </div>
        <div class="col-lg-4 col-sm-6">
          <a class="portfolio-box" href="http://wddkudrat.com/Vue_Assignment/public/booksite/dasboard.html#">
            <img class="img-fluid" src="img/portfolio/thumbnails/10.jpg" alt="">
            <div class="portfolio-box-caption">
              <div class="project-category text-white-50">
                Learning Vue
              </div>
              <div class="project-name">
                Web Page Developed using Vue and Css Framework BULMA
              </div>
            </div>
          </a>
        </div>
        <div class="col-lg-4 col-sm-6">
          <a class="portfolio-box" href="http://laravel.wddkudrat.com/">
            <img class="img-fluid" src="img/portfolio/thumbnails/17.jpg" alt="">
            <div class="portfolio-box-caption">
              <div class="project-category text-white-50">
                LARAVEL PROJECT
              </div>
              <div class="project-name">
                Created Pages and data in views are output using blade syntax (no PHP).
              </div>
            </div>
          </a>
        </div>
        <div class="col-lg-4 col-sm-6">
          <a class="portfolio-box" href="http://wddkudrat.com/Project_Incredible_India/21126_1802%20Final%20Project%20(SidhuKudrat)_copy.html">
            <img class="img-fluid" src="img/portfolio/thumbnails/8.jpg" alt="">
            <div class="portfolio-box-caption p-3">
              <div class="project-category text-white-50">
                 Web Animation
              </div>
              <div class="project-name">
                 Incredible India
              </div>
            </div>
          </a>
        </div>
        <div class="col-lg-4 col-sm-6">
          <a class="portfolio-box" href="http://wddkudrat.com/xml/news_article_webpage.html">
            <img class="img-fluid" src="img/portfolio/thumbnails/13.jpg" alt="">
            <div class="portfolio-box-caption p-3">
              <div class="project-category text-white-50">
                XML and XSL Style Web-Page
              </div>
              <div class="project-name">
                Qwerty News 
              </div>
            </div>
          </a>
        </div>
        <div class="col-lg-4 col-sm-6">
          <a class="portfolio-box" href="http://wddkudrat.com/JavaScript_form/Assignment-2_JavaScript.html">
            <img class="img-fluid" src="img/portfolio/thumbnails/9.jpg">
            <div class="portfolio-box-caption p-3">
              <div class="project-category text-white-50">
                JavaScript 
              </div>
              <div class="project-name">
                Registeration Forms generating COOKIES
              </div>
            </div>
          </a>
        </div>
        <div class="col-lg-4 col-sm-6">
          <a class="portfolio-box" href="http://wddkudrat.com/Android_Proposal/Final-Project.pdf">
            <img class="img-fluid" src="img/portfolio/thumbnails/15.jpg" alt="">
            <div class="portfolio-box-caption p-3">
              <div class="project-category text-white-50">
                Android Group Project
              </div>
              <div class="project-name">
                My Dakiya
              </div>
            </div>
          </a>
        </div>
      </div>
    </div>
  </section>

 

  <!-- Contact Section -->
  <section class="page-section" id="contact">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8 text-center">
          <h2 class="mt-0">Let's Get In Touch!</h2>
          <hr class="divider my-4">
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 ml-auto text-center mb-5 mb-lg-0">
          <a class="d-block" href="https://www.linkedin.com/in/kudrat-sidhu08/">
          <i class="fa fa-linkedin icon fa-3x mb-3 text-muted" style="font-size: 1.8rem"></i></a>
          <div>https://www.linkedin.com/in/kudrat-sidhu08/</div>
        </div>
        <div class="col-lg-4 mr-auto text-center">
          <a class="d-block" href="mailto:kudratsidhu08@gmail.com"><i class="fa fa-envelope icon fa-3x mb-3 text-muted" style="font-size: 1.8rem"></i></a>
          <div>kudratsidhu08@gmail.com</div>
        </div>
      </div>
    </div>
  </section>

  <!-- Footer -->
  <footer class="bg-light py-5">
    <div class="container">
      <div class="small text-center text-muted">--Kudrat Sidhu--</div>
    </div>
  </footer>

  
 

  

</body>

</html>



