@extends('layouts.main')

@section('content')
   <h1>List of birds in Canada</h1>
   
   <p>Canada is home to an impressive number of species of birds that vary from residents, that stay all year around, to breeding birds, that spend a good part of the growing season in Canada to raise their young, migrants who pass through Canada with the seasons, to wintering birds who like to spend a good part of the winter in Canada to escape colder conditions up north. While many species of birds are relatively common as they are part of the ecosystems of the state, it is always a thrill to stumble upon a rare bird or vagrant, that does not really form part of any the Canada ecosystems. Maybe it got lost during its travels between its summer and winter residence or it got displaced by bad weather.</p>

    @foreach($birds as $bird)
   <div class="list-group">
    <a href="/pages/birds/{{$bird->id}}" class="list-group-item list-group-item-action">{{ $bird->name }}</a>
  </div>
   @endforeach
   

@endsection