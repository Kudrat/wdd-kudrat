@extends('layouts.main')

@section('content')
   <h1>Canadian Birds</h1>
   
   <p>Canadian birds most closely resemble those of Eurasia, which was connected to the continent as part of the supercontinent Laurasia until around 60 million years ago.[7] Many families which occur in Canada are also found throughout the Northern Hemisphere or worldwide. However, some families are unique to the New World; those represented in this list are the hummingbirds, the New World vultures, the New World quail, the tyrant flycatchers, the mimids, the wood-warblers, the cardinals, and the icterids.[8] Three species on the list (Ross's goose, whooping crane, and Harris's sparrow) breed only in Canada; the extinct Labrador duck was also a breeding endemic</p>

   


@endsection