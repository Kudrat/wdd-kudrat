<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/',function(){
  $title = 'Home';
  return view('pages.home',compact('title'));
});

Route::get('/pages/about',function(){
  $title = 'About';
  return view('pages.about', compact('title'));
});

Route::get('/pages/contact',function(){
   $title = 'Contact us';
  return view('pages.contact', compact('title'));
});

// Route::get('/pages/birds',function(){
//    $title = 'Birds';
//    return view('pages.birds', compact('title'));
// });

Route::get('/pages/birds','BirdsController@index');

Route::get('/pages/birds/{id}','BirdsController@show'); 
