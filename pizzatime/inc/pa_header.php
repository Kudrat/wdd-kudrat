<header id="top">
   <div class="container">
      <div class="twelve column">
         <?php
            $navItems = array(
                'Home' => 'index.php',
                'Menu' => 'pa_menu.php',
                'Cart' => 'pa_cart.php',
            );
        ?>
        <div id="logo">
           <a href="index.php" title="Home"><img src="../img/logo.png"></a>
        </div>
         <nav>
            <ul>
               <?php foreach ($navItems as $key => $value): ?>
                  <li><a class="<?= $page == $key ? 'highlight' : '' ?>" title="<?= $key ?>"
                     href="<?= $value ?>"><?= $key ?></a></li>
               <?php endforeach; ?>
                  <li><a href="../index.php?logout=1" title="Sign out">Sign out</a>
            </ul>
         </nav>

         <div id="profile">
            <a href="profile.php" title="My profile"><i class="fas fa-user"></i><?=$_SESSION['user_name']?></a>
         </div>

         <div id="mobile-nav">
            <ul>
                <li><a href="profile.php"><i class="fas fa-user user"></i></a></li>
                <li><i class="fas fa-bars bars" id="hamburger"></i></li>
            </ul>
         </div>

         <div id="mobile-menu">
            <ul>
                <li><a href="index.php">Home</a></li>
                <li><a href="pa_menu.php">About</a></li>
                <li><a href="pa_cart.php">Cart</a></li>
                <li><a href="../index.php?logout=1">Sign out</a></li>
            </ul>
         </div>
      </div>
   </div>
</header>
