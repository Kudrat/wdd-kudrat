<header id="top">
   <div class="container">
      <div class="twelve column">
         <?php
            $navItems = array(
                'About' => 'about.php',
                'Contact' => 'contact.php',
            );
        ?>
        <div id="logo">
           <a href="index.php" title="Home"><img src="/../img/logo.png"></a>
        </div>
         <nav>
            <ul>
               <?php foreach ($navItems as $key => $value): ?>
                  <li><a class="<?= $page == $key ? 'highlight' : '' ?>" title="<?= $key ?>"
                     href="<?= $value ?>"><?= $key ?></a></li>
               <?php endforeach; ?>
               <li>
                   <a href="<?= $page == 'Index' ? '#registration' : 'index.php#registration' ?>" id="register" title="Start your registration">Register</a>
               </li>
            </ul>
         </nav>

         <div id="signin" class="show-modal">
            <p title="Sign in"><i class="fas fa-user"></i>Sign in</p>
         </div>

         <div id="mobile-nav">
            <ul>
                <li><i class="fas fa-user user show-modal"></i></li>
                <li><i class="fas fa-bars bars" id="hamburger"></i></li>
            </ul>
         </div>

         <div id="mobile-menu">
            <ul>
              <li><a href="index.php">Home</a></li>
              <li><a href="about.php">About</a></li>
              <li><a href="school.php">School Sign Up</a></li>
              <li><a href="parents.php">Parent Sign Up</a></li>
              <li><a href="contact.php">Contact</a></li>
            </ul>
         </div>

      </div>
   </div>
</header>
