<?php 
    $input = new \classes\Input();
    $input->setToken();
?>
<div id="login-frame">
   <div id="login-align">
      <div id="login-title">
          <h2>Sign in</h2>
          <p>Enter your credentials to login</p>
      </div>
      <form id="login-form" method="post" action="<?=filter_input(INPUT_SERVER, 'PHP_SELF', FILTER_SANITIZE_STRING)?>" 
        novalidate>
         <input type="hidden" id="token" name="token" value="<?=$input->getToken()?>">
         <div class="field">
             <label for="email">Email</label>
             <input type="email" name="email" id="email" placeholder="" 
                title="Username is required" required>
             <span class="input-error" id="email_error_mess" style="visibility:hidden;"></span>
        </div>
        <div class="field">
            <label for="email">Password</label>
            <input type="password" name="password" id="password" placeholder="" 
                title="Password is required" required>
            <span class="input-error" id="pass_error_mess" style="visibility:hidden;"></span>
        </div>
        <input type="submit" id="signin-btn" value="Sign in">
      </form>
      <div class="forgot">
         <a href="">Forgot password?</a>
      </div>
   </div>
</div>

<div id="overlay"></div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        
        $('#email').change(function(e){
            e.preventDefault();
            var email = $('#email').val();
            var token = $('#token').val();
            var data = {};
            data.token = token;
            data.email = email;
            /*console.log(email);*/
            $.post('validate.php', data, function(response){
                /*console.log(response);*/
                if(response.success == 'email_false'){
                    $('#email_error_mess').css('visibility','visible');
                    $('#email_error_mess').text('Please enter valid Email Address');
                    $("#signin-btn").attr('disabled',true);
                    $("#signin-btn").css('background-color', '#eee');
                    $('#pass_error_mess').css('visibility','hidden');
                }else{
                    $('#email_error_mess').css('visibility', 'hidden');
                    $("#signin-btn").attr('disabled', false);
                    $("#signin-btn").css('background-color', '#E31837');
                    $("#signin-btn").hover(function(){
                        $(this).css('background-color', '#aa1229');
                    },function(){
                         $(this).css('background-color', '#E31837');
                    });
                }
                
            });
        }); 
        $("#signin-btn").click(function(e){
            e.preventDefault();
            var password = $('#password').val();
            var email = $('#email').val();
            var token = $('#token').val();
            var data = {};
            data.token = token;
            data.password = password;
            data.email = email;
            $.post('validate.php', data, function(response){
                /*console.log(response);*/
                if(response.success == 'wrong_password'){
                    $('#pass_error_mess').css('visibility','visible');
                    $('#pass_error_mess').text('There was a problem with your credentials');
                }else if(response.success == 'empty_password'){
                    $('#pass_error_mess').css('visibility','visible');
                    $('#pass_error_mess').text('Password cannot be empty');
                }else{
                    $('#pass_error_mess').css('visibility','hidden');
                }
                
                if(response.success == 'true'){
                    window.location = response.destination;
                }
                
            });
        });
    }); 
</script>