<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Domino's Pizza Time">
    <meta name="keywords" content="domino's, pizza, school, university, celebration, party, " lang="en">
    <meta name="author" content="apex developers">
    <meta name="robots" content="index, follow">

    <title><?= $page_name ?></title>

    <link rel="shortcut icon" href="img/dominos.svg?v=2" type="image/x-icon">
    <link href="/../css/index.css" rel="stylesheet" media="all">
    <link href="/../css/<?= $style ?>.css" rel="stylesheet" media="all">
    <link href="https://fonts.googleapis.com/css?family=Barlow+Condensed:300,300i,400,400i,500,500i,600,600i,700,700i&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script defer src="/../js/modal.js"></script>
    <script defer src="../js/mobile_menu.js"></script>
    <script defer src="/../js/menu-btn.js"></script>
    <script defer src="/../js/flash.js"></script>
</head>
