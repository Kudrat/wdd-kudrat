<div id="login-frame">
   <div id="login-align">
      <div id="login-title">
          <h2>Menu options</h2>
      </div>
      <div class="options">
         <ul>
             <li>Cheese pizza</li>
             <li>Pepperoni pizza</li>
             <li>Ham & cheese pizza</li>
             <li>Cheese pizza (big slice)</li>
             <li>Pepperoni pizza (big slice)</li>
             <li>Ham & cheese pizza (big slice)</li>
             <li>Cheese pizza (gluten free)</li>
             <li>Pepperoni pizza (gluten free)</li>
         </ul>
      </div>
   </div>
</div>

<div id="overlay"></div>
