<footer>
   <div class="container">
      <div class="twelve column">
         <?php
            $navItems = array(
                'About' => 'about.php',
                'Contact' => 'contact.php',
                'Domino\'s' => 'https://www.dominos.ca/?lang=en',

            );
        ?>
         <div id="footer-nav">
            <ul>
               <?php foreach ($navItems as $key => $value): ?>
                  <li><a class="<?= $page == $key ? 'highlight' : '' ?>" title="<?= $key ?>"
                     href="<?= $value ?>"><?= $key ?></a></li>
               <?php endforeach; ?>
            </ul>
        </div>

        <div id="footer-social">
           <ul>
              <li>
                 <a href="https://www.facebook.com/DominosCanada" target="_blank" title="Facebook">
                    <i class="fab fa-facebook-square"></i>
                 </a>
              </li>
              <li>
                 <a href="https://twitter.com/DominosCanada" target="_blank" title="Twitter">
                    <i class="fab fa-twitter"></i>
                 </a>
              </li>
              <li>
                 <a href="https://www.instagram.com/dominoscanada/" target="_blank" title="Instagram">
                    <i class="fab fa-instagram"></i>
                 </a>
              </li>
           </ul>
        </div>

         <div id="footer-info">
             <p>According to our most recent assessment of scientific data, foods containing gluten
                levels not exceeding 20 PPM would not pose a health risk to the vast majority of
                individuals with Celiac Disease or to individuals with gluten sensitivity. The level
                of gluten found in our pizza made with Gluten Free Crust is below 20 PPM. However,
                Domino's pizza made with a Gluten Free Crust is prepared in a common kitchen with the
                risk of gluten exposure. Therefore, Domino's DOES NOT recommend this pizza for customers
                with Celiac Disease. Customers with gluten sensitivities should exercise judgement in
                consuming this pizza.</p>
         </div>

         <div id="footer-copyright">
             <p>©2019 Domino's IP Holder LLC. Domino's®, Domino's Pizza® and the modular logo are
                registered trademarks of Domino's® IP Holder LLC.</p>
         </div>

      </div>
   </div>
</footer>
