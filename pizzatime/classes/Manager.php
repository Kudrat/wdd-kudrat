<?php

/**
 * @author Junice Savares <savaresjunice@gmail.com>
 * @version 1.0 2019-08-09
 * @version 1.1 2019-08-13 by Victor Rengifo <victor.rengifo@gmail.com>
 * Added insertion to schools and working with transactions
 */

class Manager
{
    /**
     * Insert inputs into managers table
     * @param  object $dbh - connects to database
     * @return void
     */
	public static function create($dbh)
	{
        $result = false;
        try {
            $dbh->beginTransaction();

            $query= "INSERT INTO
                managers
                (first_name, last_name, email_address,
                password, phone, school_token)
                VALUES
                (:first_name, :last_name, :email_address, 
                :password, :phone, :school_token)";

            $stmt= $dbh->prepare($query);

            $params= array(
                        ':first_name' => $_POST['first_name'],
                        ':last_name' => $_POST['last_name'],
                        ':email_address' => $_POST['email_address'],
                        ':password' => password_hash($_POST['password'], PASSWORD_DEFAULT),
                        ':phone' => $_POST['phone'],
                        ':school_token' => $_POST['school_token'],
            );

            $stmt->execute($params);
            $manager_id = $dbh->lastInsertId();

            // add school
            $query = "select count(*) as school_exist 
                        from schools 
                        where school_token = :stoken";
            $params = array(
                        ':stoken' => $_POST['school_token'],
            );
            $stmt = $dbh->prepare($query);
            $stmt->execute($params);
            $result = $stmt->fetch(\PDO::FETCH_ASSOC);
            if($result["school_exist"]==0) {
                $query = "select school_token,name,address from domino_schools where school_token=:stoken";
                $stmt = $dbh->prepare($query);
                $stmt->execute($params);
                $dschool = $stmt->fetch(\PDO::FETCH_ASSOC);
                        
                $query = "insert into schools (school_token,name,address,restaurant_id) values (:stoken,:name,:address,:restaurant)";
                $params = array(
                    ':stoken' => $_POST['school_token'],
                    ':name' => $dschool['name'],
                    ':address' => $dschool['address'],
                    ':restaurant' => $_POST['restaurant_id'],
                );
                $stmt = $dbh->prepare($query);
                $stmt->execute($params);
                $school_id = $dbh->lastInsertId();
            }
            $dbh->commit();

            setFlash('success','Thank you for registering! Please sign in'); 

        } catch (\Throwable $th) {
            //throw $th;
            $dbh->rollback();
            setFlash('error','Something happens, try again'); 
        }   

	}

    /**
     * Select everything from schools table
     * @param  object $dbh - connects to database
     * @return array - all information about school
     */
	public static function schools($dbh)
	{
		$query = "SELECT  
                *
                FROM domino_schools";

    	$stmt = $dbh->prepare($query);

    	$params = [];

    	$stmt = $dbh->prepare($query);

    	$stmt->execute($params);

    	return $stmt->fetchAll(\PDO::FETCH_ASSOC);

	}//end school function


    /**
     * Select everything from schools table
     * @param  object $dbh - connects to database
     * @return array - all information about school
     */
    public static function restaurants($dbh)
    {
        $query = "SELECT  
                *
                FROM restaurants";

        $stmt = $dbh->prepare($query);

        $params = [];

        $stmt = $dbh->prepare($query);

        $stmt->execute($params);

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);

    }//end school function
	
}//endclass