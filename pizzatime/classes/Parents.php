<?php 

/**
 *@author Kudrat Sidhu <kudratsidhu08@gmail.com>
 *@version 1.0 2019-08-09
 */

class Parents{

   /**
   *Insert inputs into parents table
   *@param  object $dbh -- connects to database
   *@return void
   */
   public static function createParents($dbh){
          //query created 

             $query = "INSERT INTO
                      parents
                     (first_name,last_name,email_address,phone,password)
                      VALUES
                     (:first_name,:last_name,:email_address,:phone,:password)";      
            
            // prepare query
             $stmt = $dbh->prepare($query);
          
           
            //set params
            $params = array(
             ':first_name' => e_attr($_POST['first_name']),
             ':last_name' => e_attr($_POST['last_name']),
             ':email_address' => e_attr($_POST['email_address']),
             ':phone' => e_attr($_POST['phone']),
             ':password' => password_hash(e_attr($_POST['password']), PASSWORD_DEFAULT)
             );

            // execute query
             $stmt->execute($params);
            
            //insert values to lastInsertId
             $parent_id = $dbh->lastInsertId();

             return $parent_id;
            
    }
    
    /**
   *Insert inputs into students table
   *@param  object $dbh -- connects to database and parent_id fetched 
   *                       from parents table.
   *@return void
   */
    public static function createStudents($dbh,$parent_id){
        
        // adding student
            $query1 = "INSERT INTO
                       students
                      (parent_id,school_token,name,classroom_number)
                       VALUES
                      (:parent_id,:school_token,:student_name,:classroom)";       
            
          
             $stmt1 = $dbh->prepare($query1);
           
             $params1 = array(
             ':parent_id' => ($parent_id),
             ':student_name' => e_attr($_POST['student_name']),
             ':school_token' => e_attr($_POST['school_token']),
             ':classroom' => e_attr($_POST['classroom']) 
             );

             $stmt1->execute($params1);
             $student_id = $dbh->lastInsertId();

             header('Location:/index.php');

             setFlash('success','Thank you for registering! Please sign in');
           
             exit;

            
    }

}