<?php

/**
 * @author Victor Rengifo <victor.rengifo@gmail.com>
 * @version 1.0 2019-05-27
 */

namespace classes\Traits;

/**
 * Parse Event information
 */
trait ParseEvent
{

    /**
     * Parse the information acquired from the log's file
     * @param  Array $array_json Each value has json format
     * @return Array PDO::FETCH_ASSOC format
     */
    public function parseEventFile($array_json)
    {
        $a = [];
        foreach ($array_json as $value) {
            $json = json_decode($value, true);
            $a[] = array('id' => $json['key'],
                'event' => $value
            );
        }
        return $a;
    }

// ends class
}
