<?php

/**
 * @author Victor Rengifo <victor.rengifo@gmail.com>
 * @version 1.0 2019-05-27
 */

namespace classes;

use classes\ILogger;

/**
 * Database Logger class
 * Write to a database.log (table) the event
 */
class DatabaseLogger implements ILogger
{

    /**
     * Database handler
     * @var \PDO
     */
    private $dbh;

    /**
     * Database Logger constructor
     * @param \PDO $dbh PDO object with the connection to the db
     */
    public function __construct(\PDO $dbh)
    {
        $this->dbh = $dbh;
    }

    /**
     * Implementation of write method
     * @param  String $event
     * @return Boolean
     */
    public function write($event)
    {
        $result = false;
        
        $query = 'INSERT INTO log (event) VALUES (:event)';
        $params = array(':event' => $event);
        $stmt = $this->dbh->prepare($query);
        $stmt->execute($params);
        $result = true;
        return $result;
    }

    public function read($limit = 10)
    {
        $result = null;
        $query = 'SELECT * FROM log ORDER BY id DESC LIMIT :limit';
        $stmt = $this->dbh->prepare($query);
        $stmt->bindValue(':limit', (int) $limit, \PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

// ends class
}
