<?php

/**
 *@author Kudrat Sidhu <kudratsidhu08@gmail.com>
 *@version 1.0 2019-08-09
 */

namespace classes;

class CartValidator extends Validator{
      /**
     *card validation
     *@param $field
     *@return void
     */
    public function card_num($field){
        if(!preg_match("/^[0-9]{16}$/", filter_input(INPUT_POST, $field) )){
            $this->setError($field, $field. ' is not valid must be 16 digit number');
        }
    }
    
     /**
     *cvv validation function
     *@param $field
     *@return void
     */
    public function cvv($field){
        if(!preg_match("/^[0-9]{3}$/", filter_input(INPUT_POST, $field))){
           $this->setError($field, $field . ' is not valid must be 3 digit number');
        }

    }
}