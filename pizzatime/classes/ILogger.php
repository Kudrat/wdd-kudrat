<?php

/**
 * @author Victor Rengifo <victor.rengifo@gmail.com>
 * @version 1.0 2019-05-27
 */

namespace classes;

/**
 * Interface ILogger
 * methods to implement for Database of File Logger
 */
interface ILogger
{

    public function write($event);
    public function read($limit);

// end class
}
