<?php

/**
 * @author Victor Rengifo <victor.rengifo@gmail.com>
 * PHP Capstone
 * @version 1.0 2019-05-27
 */

namespace classes;

/**
 * Class will allow us to access all Input data (POST, GET, COOKIE) through static methods
 * Class will have following attributes:
        $post
        $get
        $cookie
 * Class will have following public methods (API)
        rawPost()
        cleanPost()
        rawGet()
        cleanGet()
        rawCookie()
        cleanCookie()
 */
class Input
{
    private $post;
    private $get;
    private $cookie;
    private $server;
    private $env;

    private $csrf_token;

    /**
     * Constructor for Input class
     */
    public function __construct()
    {
        $this->post = array();
        $this->get = array();
        $this->cookie = array();
        $this->server = array();
        $this->env = array();

        // getting the content from $_POST, $_GET
        $this->post = filter_input_array(INPUT_POST);
        $this->get = filter_input_array(INPUT_GET);
        $this->cookie = filter_input_array(INPUT_COOKIE);
        $this->server = filter_input_array(INPUT_SERVER);
        $this->env = filter_input_array(INPUT_ENV);
    }

    /**
     * Clean an array
     * @param  Array $array
     * @return Array
     */
    private function cleanArray($array)
    {
        $clean = [];
        if (!empty($array)) {
            foreach ($array as $key => $value) {
                $clean[$key] = htmlspecialchars($value, ENT_QUOTES, 'utf-8');
            }
        }
        return $clean;
    }

    /**
     * Sanitize the value
     * @param  String $value
     * @return String
     */
    private function cleanValue($value)
    {
        return htmlspecialchars($value, ENT_QUOTES, 'utf-8');
    }

    /**
     * using $_GET return a value without sanitize
     * without parameters return the $_GET
     * @param  String $field field
     * @return String | Array
     */
    public function rawGet($field = null)
    {
        if (!is_null($field)) {
            return (!empty($this->get[$field])) ? $this->get[$field] : false ;
            //return $this->get[$field] && false ;
        }
        return $this->get;
    }

    /**
     * using $_GET return a value with sanitize
     * without parameters return the $_GET
     * @param  String $field field
     * @return String | Array
     */
    public function cleanGet($field = null)
    {
        if (!is_null($field)) {
            //return htmlentities($this->get[$field], ENT_QUOTES, 'utf-8');
            return (!empty($this->get[$field])) ? $this->cleanValue($this->get[$field]) : false;
        }
        return $this->cleanArray($this->get);
    }

    /**
     * using $_POST return a value without sanitize
     * without parameters return the $_POST
     * @param  String $field field
     * @return String | Array
     */
    public function rawPost($field = null)
    {
        if (!is_null($field)) {
            return $this->post[$field];
        }
        return $this->post;
    }

    /**
     * using $_POST return a value with sanitize
     * without parameters return the $_POST
     * @param  String $field field
     * @return String | Array
     */
    public function cleanPost($field = null)
    {
        if (!is_null($field)) {
            return (!empty($this->post[$field])) ? $this->cleanValue($this->post[$field]) : false;
        }
        return $this->cleanArray($this->post);
    }

    /**
     * using $_COOKIE return a value without sanitize
     * without parameters return the $_COOKIE
     * @param  String $field field
     * @return String | Array
     */
    public function rawCookie($field = null)
    {
        if (!is_null($field)) {
            return $this->cookie[$field];
        }
        return $this->cookie;
    }

    /**
     * using $_COOKIE return a value with sanitize
     * without parameters return the $_COOKIE
     * @param  String $field field
     * @return String | Array
     */
    public function cleanCookie($field = null)
    {
        if (!is_null($field)) {
            return (!empty($this->cookie[$field])) ? $this->cleanValue($this->cookie[$field]) : false;
        }
        return $this->cleanArray($this->cookie);
    }

    /**
     * using $_SERVER return a value without sanitize
     * without parameters return the $_SERVER
     * @param  String $field field
     * @return String | Array
     */
    public function rawServer($field = null)
    {
        if (!is_null($field)) {
            return $this->server[$field];
        }
        return $this->server;
    }

    /**
     * using $_SERVER return a value with sanitize
     * without parameters return the $_SERVER
     * @param  String $field field
     * @return String | Array
     */
    public function cleanServer($field = null)
    {
        if (!is_null($field)) {
            return (!empty($this->server[$field])) ? $this->cleanValue($this->server[$field]) : false;
        }
        return $this->cleanArray($this->server);
    }

    /**
     * using $_ENV return a value without sanitize
     * without parameters return the $_ENV
     * @param  String $field field
     * @return String | Array
     */
    public function rawEnv($field = null)
    {
        if (!is_null($field)) {
            return $this->env[$field];
        }
        return $this->env;
    }

    /**
     * using $_ENV return a value with sanitize
     * without parameters return the $_ENV
     * @param  String $field field
     * @return String | Array
     */
    public function cleanEnv($field = null)
    {
        if (!is_null($field)) {
            return (!empty($this->env[$field])) ? $this->cleanValue($this->env[$field]) : false;
        }
        return $this->cleanArray($this->env);
    }

    /**
     * set the Token
     */
    public function setToken()
    {
        if (empty($_SESSION['csrf_token'])) {
            $one = md5(rand());
            $salt = md5(rand());
            $hash = md5($one . $salt);
            $_SESSION['csrf_token'] = $hash;
            $this->csrf_token = $hash;
        }
    }

    /**
     * get the token from $_SESSION
     * @return String
     */
    public function getToken()
    {
        return $_SESSION['csrf_token'];
    }

    /**
     * compare the form value with the session
     * @param  String $value
     * @return Boolean
     */
    /*
     public function matchToken($value)
    {
        $res = false;
        if ($value==$this->getToken()) {
            $res = true;
        }
        return $res;
    }
    */

    /**
     * compare the form value with the session
     * @param  String $value
     * @return Boolean
     */
    public function matchToken($value,$msg="",$die=0)
    {
        $res = false;
        if ($value==$this->getToken()) {
            $res = true;
        } else {
            // msg
            if(strlen($msg)==0) $msg = "Something happen with your requirement.. try again";
            setFlash('error',$msg);
            
            if($die){
                // redirect to index.php
                //$destination = BASE_PATH . 'public/index.php';
                //$destination = BASE_PATH . 'public/index.php';
                $destination = '/index.php?logout=1';
                header('location:'.$destination);
                die;
            } else 
            {
                $res = false;
            }
        }
        return $res;
    }
}
