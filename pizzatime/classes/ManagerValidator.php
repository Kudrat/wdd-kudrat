<?php

/**
 * @author Kudrat Sidhu <kudratsidu08@gmail.com>
 * @version 1.0 2019-08-09
 */

namespace classes;

class ManagerValidator extends Validator{
   
    /**
   * Validate the matchEmail using filter_input
   * @param   $field - string $dbh-object 
   * @return void
   */
    public function matchEmail($field,$dbh)
    {
      if(!empty($_POST[$field])){
         $query = "SELECT email_address from managers
                   WHERE email_address LIKE :search";
          
         $stmt = $dbh->prepare($query);

         $params= [
          ':search' => "%{$_POST[$field]}%"
          ];

         $stmt->execute($params);

         $result = $stmt->fetchAll();
          
         if($result){
            $this->setError($field,'User already exists');
          }//endif $result

      }//end if empty

    }//end matchEmail function

}//endclass
     
   
