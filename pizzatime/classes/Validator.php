<?php

/**
 * @author Victor Rengifo <victor.rengifo@gmail.com>
 * @version 1.0 2019-05-27
 */

namespace classes;


// Classes should be created one per file
// The file name for classes should be identical to the class name
// eg: this class should be saved in a file called Validator.php

// Validate any type of data
// Track our errors
// Give us the errors when we ask for them

/**
 * Validator Class
 */
class Validator
{

    private $errors = [];
    private $rules = [];

    /**
     * Evaluated if the field is required
     * @param  String $field field name
     * @return Void
     */
    public function required($field)
    {
        $pattern = '/.{1,}/';
        if (!preg_match($pattern, filter_input(INPUT_POST, $field))) {
            $this->setError($field, $field. ' is required');
        }
    }

  // filter_input(INPUT_POST, $field, FILTER_VALIDATE_EMAIL)
  // options: INPUT_GET
  
  /**
   * Validate the email using filter_input
   * @param  [type] $field [description]
   * @return [type]        [description]
   */
    public function email($field)
    {
        if (!filter_input(INPUT_POST, $field, FILTER_VALIDATE_EMAIL)) {
            $this->setError($field, 'field  is not a valid email');
        }
    }



    /**
     * Return the errors array
     * @return Array
     */
    public function errors()
    {
        return $this->errors;
    }



  /**
   * Validate the email using filter_input
   * @param  [type] $field [description]
   * @return [type]        [description]
   */
    public function url($field)
    {
        if (!filter_input(INPUT_POST, $field, FILTER_VALIDATE_URL)) {
            $this->setError($field, 'field  is not a valid url');
        }
    }

  /**
   * Validate a number using filter_input
   * @param  string $field
   * @return Void
   */
    public function number($field)
    {
        if (!filter_input(INPUT_POST, $field, FILTER_VALIDATE_FLOAT)) {
            $this->setError($field, 'Please enter a number in ' .$field. ' field');
        }
    }

  /**
   * Validate a number using filter_input
   * @param  string $field
   * @param  number $min
   * @param  number $max
   * @return Void
   */
  public function percentage($field, $min=0.01, $max=100)
  {
      if (!filter_input(INPUT_POST, $field, FILTER_VALIDATE_FLOAT)) {
          $this->setError($field, 'Please enter a number in ' .$field. ' field');
      }
      // validate in range
      $value = filter_input(INPUT_POST, $field, FILTER_VALIDATE_FLOAT);
      if($value<$min || $value>$max) {
        $this->setError($field, 'Percentage out of range ('.$min.' to '.$max.') on ' .$field. ' field');
      }
  }

    /**
     * Validate a number as integer using filter_input
     * @param  string $field
     * @return Void
     */
    public function integer($field)
    {
        $pattern = '/^[0-9]{1,}$/';
        if (!preg_match($pattern, filter_input(INPUT_POST, $field))) {
            $this->setError($field, 'field is not an integer');
        }
    }

    /**
     * Validate a string with min and max size
     * @param  String $field name of the field
     * @param  int $min_size minimal size
     * @param  int $max_size maximum size
     * @return Void
     */
    public function string($field, $min_size, $max_size)
    {
        $pattern = '/.{'.$min_size.','.$max_size.'}/';
        if (!preg_match($pattern, filter_input(INPUT_POST, $field))) {
            $this->setError($field, 'field must have between '.$min_size.' and '.$max_size.' characters');
        }
    }



    /**
     * Validate the postal code in Canadian format
     * A1A2A2
     * @param  String $field
     * @return Void
     */
    public function postalCodeCanada($field)
    {
        // remove characters
        $pattern = '/[^a-zA-Z0-9]/';
        $postalcode = preg_replace($pattern, '', filter_input(INPUT_POST, $field));
        $postalcode = strtoupper($postalcode);
        $pattern = '/^[A-Z][0-9][A-Z][0-9][A-Z][0-9]/';
        if (!preg_match($pattern, $postalcode)) {
            $this->setError($field, 'field is not in Canadian Postal Code Format R1R1A1');
        }
    }

    /**
     * Validate the phone in Canadian Style
     * 1112223333
     * @param  String $field
     * @return Boolean
     */
    public function phoneCanada($field)
    {
        $result = false;
        // remove characters
        $pattern = '/[^0-9]/';
        $phone = preg_replace($pattern, '', filter_input(INPUT_POST, $field));
        $phone = strtoupper($phone);
        $pattern = '/.{10}/';
        if (!preg_match($pattern, $phone)) {
            $this->setError($field, 'field is not a Canadian Phone Number');
            $result = false;
        } else {
            $result = true;
        }
        return $result;
    }

    /**
     * Validate the phone in international Style
     * +49-89-636-48018
     * @param  String $field
     * @return Void
     */
    public function phoneInternational($field)
    {
        $result = false;
        $pattern = '/^\+[0-9\-\s()]{1,}$/';
        if (!preg_match($pattern, filter_input(INPUT_POST, $field))) {
            $this->setError($field, 'field is not an International Phone Number');
            $result = false;
        } else {
            $result = true;
        }
        return $result;
    }

    public function phone($field)
    {
        unset($this->errors[$field]);
        // valid if phone is in Canadian Style
        $is_valid = $this->phoneCanada($field);
        if (!$is_valid) {
            // valid if phone is in International Style
            $this->phoneInternational($field);
        }
    }

    /**
     * Validate the date in format yyyy-mm-dd
     * 2019-05-16
     * @param  String $field
     * @return Void
     */
    public function date($field)
    {
        $pattern = '/^[0-9]{4}\-[0-9]{2}\-[0-9]{2}$/';
        if (!preg_match($pattern, filter_input(INPUT_POST, $field))) {
            $this->setError($field, 'field is not a valid date');
        }
    }

    /**
     * Check the complexity of the password
     * @param  String $field field
     * @return Void
     */
    private function passwordComplexity($field)
    {
        $pattern = '/(?=.*[&^%$#@!\(\)*+=~]+)(?=.*[A-Z]+)(?=.*[0-9]+){8,}/';
        if (!preg_match($pattern, filter_input(INPUT_POST, $field))) {
            $this->setError($field, 'password is not accomplished with password policies');
        }
    }

    /**
     * Validate if the password and repassword match
     * @param  String $fiel1  fiel1
     * @param  String $field2 field2
     * @return Void
     */
    private function passwordMatch($field1, $field2)
    {
        if (filter_input(INPUT_POST, $field1)!==filter_input(INPUT_POST, $field2)) {
            $this->setError($field1, 'Passwords don\'t match');
        }
    }

    /**
     * Validate the password
     * 1. Validate the complexity
     * 2. Validate the match with the field2
     * @param  String $field1 password field1
     * @param  String $field2 repassword or field2
     * @return Void
     */
    public function password($field1, $field2)
    {
        $this->passwordComplexity($field1);
        if (empty($this->errors[$field1])) {
            $this->passwordMatch($field1, $field2);
        }
    }



/**
    * Validates the name field
    * @param  string $field
    * @return string Stating to provide a valid name if it is an error
    */
    public function name($field)
    {
        $pattern = '/^[a-zA-Z,\'.\-\s]+$/';
        $string = filter_input(INPUT_POST, $field);
        if (!preg_match($pattern, $string)) {
            $this -> setError($field, 'Please provide a valid name');
        }
    }


  /**
   * Set the error for a specific field
   * @param String $field field
   * @param String $message message of error
   */
    protected function setError($field, $message)
    {
        if (empty($this->errors[$field])) {
            $this->errors[$field] = $message;
        }
    }




    /**
     * Set the array of rules
     *
    $rules = array(
        "[field]" => array("type" => "string","min_size" => 1,"max_size" => 255),
        "email" => array("type" => "email"),
        "phone" => array("type" => "phone","size" => 10),
        "postal_code" => array("type" => "zipcode","min_size" => 6,"max_size" => 7),
        "password" => array("type" => "password","confirm_field" => "repassword")
    );

     * @param [type] $rules [description]
     */
    public function setRules($rules)
    {
        $this->rules = $rules;
    }

    /**
     * Validate the rules
     * @return Void
     */
    public function validate()
    {
        foreach ($this->rules as $field => $rule) {
            switch ($rule['type']) {
                case 'string':
                    $this->string($field, $rule['min_size'], $rule['max_size']);
                    break;
                case 'email':
                    $this->email($field);
                    break;
                case 'url':
                    $this->url($field);
                    break;
                case 'integer':
                    $this->integer($field);
                    break;
                case 'number':
                    $this->number($field);
                    break;
                    case 'percentage':
                    $this->percentage($field, $rule['min_size'], $rule['max_size']);
                    break;
                case 'date':
                    $this->date($field);
                    break;
                case 'phone':
                    $this->phone($field);
                    break;
                case 'zipcode':
                    $this->postalCodeCanada($field);
                    break;
                case 'password':
                    $this->password($field, $rule['confirm_field']);
                    break;
                default:
                    # code...
                    break;
            }
        }
    }
}

