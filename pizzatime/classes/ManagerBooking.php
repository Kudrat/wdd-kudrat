<?php

/**
 * @author Victor Rengifo <victor.rengifo@gmail.com>
 * @version 1.0 2019-08-14
 */

class ManagerBooking
{
    private static $days_after = 5;
    
    /**
     * Check if the restaurant is available for the pizza's day
     * @param   object  $dbh  connection to db
     * @return  boolean 
     */
    private static function validate($dbh) {
        // is_valid
        $is_valid = false;
        // check if the restaurant is available
        $query = "select r.restaurant_id,r.restaurant_name,r.max_orders,
        (
          select count(*) 
          from bookings b 
          where b.restaurant_id = :restaurant
          and b.date = :day 
        ) as orders_for_day
        from restaurants r 
        where r.restaurant_id = :restaurant";
        $params = array(
            ':restaurant' => $_POST['restaurant'], 
            ':day' => $_POST['date'],
        );
        $stmt = $dbh->prepare($query);
        $stmt->execute($params);
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);

        if($result['orders_for_day']<$result['max_orders']) {
            $is_valid = true;
        }
        return $is_valid;
    }
    
    /**
     * Insert data on bookings and booking_item
     * @param  object $dbh - connects to database
     * @return void
     */
	public static function create($dbh)
	{
        $result = false;
        // is allowed
        if(self::validate($dbh)) {
            try {
                $dbh->beginTransaction();
                // insert into bookings
                $query = "insert into bookings 
                        (school_token,restaurant_id,date,markup_price,created_by) 
                        values 
                        (:school_token,:restaurant_id,:date,:markup_price,:created_by)";
                $params = array(
                    ':school_token' => $_POST['school'],
                    ':restaurant_id' => $_POST['restaurant'],
                    ':date' => $_POST['date'],
                    ':markup_price' => $_POST['markup'],
                    ':created_by' => $_SESSION['user'],
                );
                $stmt = $dbh->prepare($query);
                $stmt->execute($params);
                $booking_id = $dbh->lastInsertId();

                // insert into booking_item
                $query = "insert into booking_item 
                        (booking_id,item_id,item_name_snapshot,item_price_snapshot,item_description_snapshot,item_image_snapshot) 
                        select :booking_id as booking_id, item_id,item_name,item_price,item_description,item_image
                        from items";
                $params = array(':booking_id' => $booking_id);
                $stmt = $dbh->prepare($query);
                $stmt->execute($params);

                $dbh->commit();
                setFlash('success','Booking process executed successfully'); 
                $result = true;
            
            } catch (\Throwable $th) {
                setFlash('error','Something happen, please try again');
                $result = false;
            }
        } else {
            setFlash('error','Restaurant is not available for this date');
            $result = false;
        }
        return $result;
	}

    /**
     * Select everything from schools table
     * @param  int $manager
     * @param  object $dbh - connects to database
     * @return array - all information about school
     */
	public static function school($manager,$dbh)
	{
        $query = "SELECT 
          managers.first_name as first_name,
          managers.last_name as last_name,
          managers.email_address,
          managers.phone,           
          schools.school_token as school_token,
          schools.name as school_name,
          schools.address,
          schools.restaurant_id
          FROM 
          managers
          JOIN schools USING(school_token)
		      WHERE 
          manager_id = :manager_id";

        $params = array(
            ':manager_id' => $manager
        );
        $stmt=$dbh->prepare($query);
        $stmt->execute($params);
        return $stmt->fetch(\PDO::FETCH_ASSOC);
	}


    /**
     * Select everything from schools table
     * @param  int $restaurant
     * @param  object $dbh - connects to database
     * @return array - all information about restaurant dates
     */
    public static function restaurantDates($school,$restaurant,$dbh)
    {
        $query = "select r.restaurant_id,r.restaurant_name,date_format(rd.date,'%Y-%m-%d') date,rd.max_orders
        from restaurants r 
        inner join restaurant_dates rd on (rd.restaurant_id=r.restaurant_id) 
        where r.restaurant_id=:restaurant_id 
        and rd.date>=current_date()+:days_after 
        and rd.date not in (select date from bookings where restaurant_id=:restaurant_id and school_token=:school)
        order by 2";

        $params = array(
            ':restaurant_id' => $restaurant,
            ':days_after' => self::$days_after,
            ':school' => $school,
        );
        $stmt=$dbh->prepare($query);
        $stmt->execute($params);
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);   
    }

    /**
     * Get items
     * @param  object $dbh - connects to database
     * @return array - all information about items
     */
    public static function items($dbh)
    {
        $query = "select i.item_id,i.item_name,i.item_description,i.item_price,i.item_image 
        from items i 
        order by i.item_name";
        $stmt=$dbh->prepare($query);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Select everything from schools table
     * @param  int $restaurant
     * @param  object $dbh - connects to database
     * @return array - all information about restaurant dates
     */
    public static function pizza_days($manager,$dbh)
    {
        $query = "select s.school_token,s.name as school_name,s.address as school_address
        ,r.restaurant_name,r.address as restaurant_address
        ,b.booking_id,date_format(b.date,'%Y-%m-%d') date, b.markup_price 
        ,concat(m2.first_name,' ',m2.last_name) as manager
        from managers m 
        inner join schools s on (s.school_token=m.school_token) 
        inner join restaurants r on (r.restaurant_id=s.restaurant_id) 
        left join bookings b on (b.school_token=s.school_token) 
        left join managers m2 on (m2.manager_id=b.created_by)
        where m.manager_id = :manager
        order by b.date desc";

        $params = array(
            ':manager' => $manager,
        );
        $stmt=$dbh->prepare($query);
        $stmt->execute($params);
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);   
    }
	
}
