<?php

/**
 *@author Kudrat Sidhu <kudratsidhu08@gmail.com>
 *@version 1.0 2019-08-09
 */

namespace classes;

class ParentValidator extends Validator{


    /**
   * Validate the matchEmail using filter_input
   * @param  [type] $field [description]
   * @return [type]        [description]
   */
    public function matchEmail($field,$dbh)
    {
      if(!empty($_POST[$field])){
       $query = "SELECT email_address from parents 
                 WHERE email_address LIKE :search";
        
        $stmt = $dbh->prepare($query);

        $params= [
        ':search' => "%{$_POST[$field]}%"
        ];

        $stmt->execute($params);

        $result = $stmt->fetchAll();
        
        if($result){
             
             $this->setError($field,'User already exists');
        }
      }
     
    }


      /**
   * matchToken
   * @param  [type] $field [description]
   * @return [type]        [description]
   */
    public function matchToken($field,$dbh)
    {
      if(!empty($_POST[$field])){
       $query = "SELECT school_token from domino_schools 
                 WHERE school_token LIKE :search";
        
        $stmt = $dbh->prepare($query);

        $params= [
        ':search' => "%{$_POST[$field]}%"
        ];

        $stmt->execute($params);

        $token = $stmt->fetchAll();
        
        if(!$token){
             
             $this->setError($field,'Choose the Correct School Token');
        }
      }

    }

    
}