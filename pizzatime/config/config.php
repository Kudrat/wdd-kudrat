<?php
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 *                                                       * 
 *          Program:    Web Development Diploma          *
 *          Signature:  E-Commerce                       *
 *          Instructor: Steve George                     *
 *          Team 2 by   APEX Dev                         *
 *                                                       *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 
 File: config.php
 Description: 
    Configuration file

*********************************************************/

   // output buffer start
   ob_start();
   // session start
   session_start();

   // if true, error_reporting and display_errors show everything.
   $development_env = true;

   // if true, error_reporting and display_errors show everything.
   $debug_db = true;

   // Parameters
   $siteName = 'Pizzatime by Domino\s'; // website
     
   // setting the environment
   if($development_env){ // development environment
       ini_set('display_errors', 1);
       ini_set('error_reporting', E_ALL);
   } else{ // production environment
       ini_set('display_errors', 0);
       ini_set('error_reporting', 0);
   }

   // base path for the project
   define('BASE_PATH', __DIR__ . '/../');

   // base path for school manager
   //define('SCHOOL_PATH', __DIR__ . '/../public/school');
   define('SCHOOL_PATH', 'school');

   // base path for parent
   //define('PARENT_PATH', __DIR__ . '/../public/parent');
   define('PARENT_PATH', 'parent');

   define('DB_DATEFORMAT','%Y-%m-%d');

   spl_autoload_register('my_autoload');

   function my_autoload($class)
   {
       $class = trim($class, '\\');
       $class = str_replace('\\', '/', $class);
       $class .= '.php';
       $file = BASE_PATH . '/' . $class;
       if(file_exists($file)) {
           require $file;
       }
   }

/*
   if(!isset($_SESSION['csrf_token'])){
    $_SESSION['csrf_token'] = md5(uniqid().time());
   }
// validate csrf token compare with session one
    if('POST' == $_SERVER['REQUEST_METHOD']){
        if(empty($_POST['token']) || $_POST['token'] !== $_SESSION['csrf_token']){
            die('Warning: csrf token mistaken');
        }
    }
*/
    
   // includes
   require_once BASE_PATH . 'config/connect.php';  
   require_once BASE_PATH . 'lib/functions.php';
   require_once BASE_PATH . 'config/log.php';


    //setting the csrf_token
    $input = new \classes\Input();
    $input->setToken();
