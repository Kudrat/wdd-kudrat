<?php

/**
 * @author Victor Rengifo <victor.rengifo@gmail.com>
 * OO-PHP
 * log.php
 * 
 * @version 1.0 2019-05-06 
 */

use \classes\ILogger;
use \classes\DatabaseLogger;
use \classes\FileLogger;


$logfile = BASE_PATH . 'storage/events.log';

$logfile_sqlite = BASE_PATH . 'storage/log.sqlite';

// Log to file (uncomment next line)
//$logger = new FileLogger($logfile);

// Log to MySQL
$logger = new DatabaseLogger($dbh);

function logger(ILogger $logger) {
	// create the event 
	$array_event = array(
		'key' => date('YmdHis'),
		'event' => array(
			'datetime' => date('Y-m-d H:i:s'),
			'from' => $_SERVER['REQUEST_URI'],
			'request_method' => $_SERVER['REQUEST_METHOD'],
			'user_agent' => $_SERVER['HTTP_USER_AGENT'],
			'server' => $_SERVER['SERVER_SIGNATURE'],
			'request_schema' => $_SERVER['REQUEST_SCHEME']
		)
	);
	
	// encode the event
	$event = json_encode($array_event);
	
	// write the event
	$logger->write($event);

}

// write to the logger
logger($logger);