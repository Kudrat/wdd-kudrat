<?php 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 *                                                       * 
 *          Program:    Web Development Diploma          *
 *          Signature:  E-Commerce                       *
 *          Instructor: Steve George                     *
 *          Team 2 by   APEX Dev                         *
 *                                                       *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 
 File: connect.php
 Description: 
    Connection to the DB

*********************************************************/

  // DB parameters
  define('DB_SRV','localhost');
  define('DB_NAME','pizzatime');
  // define('DB_USER','wdd14');
  // define('DB_PASS','mypass');
  define('DB_USER','apexdev');
  define('DB_PASS','W3bD3v3l0pm3nt');

// database signature for MySQL
define('DB_DSN','mysql:host='.DB_SRV.';dbname='.DB_NAME);

// database handler
$dbh = new PDO(DB_DSN,DB_USER,DB_PASS);
// setting errmode
$dbh->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

