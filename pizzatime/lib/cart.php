<?php 

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 *                                                       * 
 *          Program:    Web Development Diploma          *
 *          Signature:  E-Commerce                       *
 *          Instructor: Steve George                     *
 *          Team 2 by   Victor Rengifo                   *
 *                                                       *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 
 File: cart.php
 Description:
    Some functions

*********************************************************/

define('GST',0.05);
define('PST',0.08);

function getCart() {
	if(empty($_SESSION['cart'])) {
		return 'Your cart is empty';
	} else {
		$items = count($_SESSION['cart']);
		return "$items items in cart";
	}
}

/**
 * Single Item Shopping Cart
 * @param Array $item
 */
/*
function addToCart($item) {
	// Single item cart
	$_SESSION['cart'][0] = $item;
}
*/

/**
 * Multiple Item Shopping Cart
 * @param Array $item
 */
function addToCart($item) {
    // Single item cart

    foreach ($item['items'] as $key => $value) {
        if($value > 0) {
            // booking_id | student_id | booking_item_id
            $cartKey = $item['booking_id'] . '|' . $item['student_id'] . '|' .$key;
            $aux = array(
                'booking_id' => $item['booking_id'],
                'student_id' => $item['student_id'],
                'booking_item_id' => $key,
                'qty' => $value
            );
            $_SESSION['cart'][$cartKey] = $aux;
        }
    }

	
}

function cart(){
	return(count($_SESSION['cart']));
}

function getItems($dbh){
	$items = [];
	foreach ($_SESSION['cart'] as $key => $value) {
        
        list($booking,$student,$bookingitem) = explode('|',$key);

        $query = "
        select date_format(b.date,'%Y-%m-%d') as booking_date,s.name as school_name
        ,s.student_id,s.name as student_name,s.classroom_number
        ,bi.booking_item_id,bi.item_name_snapshot as item_name,bi.item_price_snapshot as domino_price
        ,(bi.item_price_snapshot*(1+(b.markup_price/100))) as price
        ,(:qty)*(bi.item_price_snapshot*(1+(b.markup_price/100))) as lineprice
        from booking_item bi
        inner join bookings b on (b.booking_id=bi.booking_id) 
        inner join schools sc on (sc.school_token = b.school_token)
        inner join students s on (s.school_token = sc.school_token)  
        where bi.booking_item_id = :booking_item_id 
        and s.student_id = :student_id";
        
		$params = array(
            ':booking_item_id' => $bookingitem,
            ':student_id' => $student,
            ':qty' => $value['qty'],
        );
		$stmt = $dbh->prepare($query);
		$stmt->execute($params);
		$product = $stmt->fetch(PDO::FETCH_ASSOC);
		$items[$key]['item_name'] = $product['item_name'];
		$items[$key]['qty'] = $value['qty'];
        $items[$key]['unit_price'] = $product['price'];   
        $items[$key]['lineprice'] = $product['lineprice'];  
        
        //complimentary data
        $items[$key]['booking_date'] = $product['booking_date'];
        $items[$key]['school_name'] = $product['school_name'];
        $items[$key]['student_id'] = $product['student_id'];
        $items[$key]['student_name'] = $product['student_name'];
        $items[$key]['classroom_number'] = $product['classroom_number'];
        $items[$key]['booking_item_id'] = $product['booking_item_id'];
	}
	return $items;
}

function calculation($items,$dbh) {
	$subtotal = $gst = $pst = $total = 0;
	$calc = [];
	try {
		foreach ($items as $key => $value) {
            /*
            $query = 'SELECT price FROM book WHERE book_id=:book_id';
			$params = array(':book_id' => $key);
			$stmt = $dbh->prepare($query);
			$stmt->execute($params);
            $book = $stmt->fetch(PDO::FETCH_ASSOC);
            */
			
            //$subtotal += $value['qty'] * $book['price'];
            $subtotal += $value['lineprice'];
		}
		$gst = $subtotal * GST;
		$pst = $subtotal * PST;

		$total = $subtotal + $gst + $pst;

		$calc = array( 
			'subtotal' => number_format($subtotal, 2),
			'pst' => number_format($pst,2),
			'gst' => number_format($gst, 2),
			'total' => number_format($total,2)	
		);		
	} catch (Exception $e) {
		$calc = [];
	}
	return $calc;
}