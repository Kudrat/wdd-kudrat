$(document).ready(function() {
   var $menu = $('#hamburger');
   var $mm = $('#mobile-menu');
   var $overlay = $('#overlay');

   $menu.click(function() {
      $mm.addClass('isvisible');
      $overlay.addClass('isvisible');
      $('body').addClass('noScroll');
   });

   $overlay.on('click', function(event) {
      if ($(event.target).is($overlay) || $(event.target).is('.isvisible')) {
         $mm.removeClass('isvisible');
         $overlay.removeClass('isvisible');
         $('body').removeClass('noScroll');
         $password.val('');
      }
   });

   $(document).keyup(function(event) {
      if (event.which == '27') {
         $mm.removeClass('isvisible');
         $overlay.removeClass('isvisible');
         $('body').removeClass('noScroll');
      }
   });
});
