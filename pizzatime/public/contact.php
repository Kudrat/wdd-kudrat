<?php

require __DIR__ . '/../config/config.php';
/**
 *  Contact
 *  Pizza time
 *  Apex Developers
 */

// Page variables
$page = 'Contact';
$style = '../css/contact';
$page_name = 'Contact us | Pizza Time';

// Any page includes

?><!doctype html>
<html lang="en">
<?php require_once __DIR__ . '/../inc/common_head.php'; ?>
<body>
    <?php require_once __DIR__ . '/../inc/header.php'; ?>
    <div id="wrapper">
        <div class="master-heading">
            <div class="container">
                <div class="twelve column">
                    <h2><i class="fas fa-envelope-open-text"></i> Contact us</h2>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="twelve column">
                <div class="contact">
                    <h3>Message form</h3>
                    <form id="contact-form" method="post" action="">
                        <input type="hidden" name="token" value="">
                        <div class="fields">
                            <div class="field">
                                <label for="first-name">Name <span>*</span></label>
                                <input type="text" name="name" maxlength="50"
                                    title="Name is required" required>
                                <!-- <span class="input-error">Error message</span> -->
                            </div>
                            <div class="field">
                                <label for="email">Email <span>*</span></label>
                                <input type="email" name="email" maxlength="50"
                                    title="Email is required" required>
                                <!-- <span class="input-error">Error message</span> -->
                            </div>
                            <div class="field">
                                <label for="message">Message <span>*</span></label>
                                <textarea name="message" maxlength="1000" title="Message is required" required></textarea>
                                <!-- <span class="input-error">Error message</span> -->
                            </div>
                        </div>
                        <input type="submit" id="contact-btn" value="Send">
                    </form>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="twelve column">
                <div class="required-message">
                    <p><span>*</span> Indicates required field</p>
                </div>
            </div>
        </div>
        <div class="break"></div>
    </div>

    <?php require_once __DIR__ . '/../inc/modal.php'; ?>
    <?php require_once __DIR__ . '/../inc/footer.php'; ?>
</body>
</html>
