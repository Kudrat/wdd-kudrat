<?php

/**
 *  Parent thank you
 *  Pizza time
 *  Apex Developers
 */

 // Any page variables
 $page = 'Thank';
 $style = '/../css/pa_thank_you';
 $page_name = 'Thank you | Pizza Time';

 if ($_SESSION['role'] != 'parent' ) {
    setFlash('error', 'There are some problems .. try again');
    header('Location: /../index.php');
    die;
}

// Any page includes

?><!doctype html>
<html lang="en">
<?php require_once __DIR__ . '/../../inc/pa_head.php'; ?>
<body>
    <div id="wrapper">
        <?php require_once __DIR__ . '/../../inc/pa_header.php'; ?>
        <?php include __DIR__ . '/../../inc/flash.inc.php'; ?>
        <div class="master-heading">
            <div class="container">
                <div class="twelve column">
                    <h2><i class="fas fa-heart"></i> Thank you</h2>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="twelve column">
                <div class="section">
                    <div class="heading">
                        <h3>Order status</h3>
                        <div class="info-box">
                            <p><strong>Your order was completed successfully</strong></p>
                            <p>
                                An email receipt including the details about your order has
                                been sent to the email address provided. Please keep it for your
                                records
                            </p>
                        </div>
                    </div>
                </div>

                <div class="section">
                    <div class="heading">
                        <h3>Pizza time message</h3>
                        <div class="info-box">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                sed do eiusmod tempor incididunt ut labore et dolore magna
                                aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                                ullamco laboris nisi ut aliquip ex ea commodo consequat.oatur
                            </p>
                        </div>
                        <a href="#">Return to home</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="break"></div>
    </div>
    <?php require_once __DIR__ . '/../../inc/footer.php'; ?>
    <div id="overlay"></div>
</body>
</html>
