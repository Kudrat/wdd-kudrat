<?php

/**
 *  Parent cart
 *  Pizza time
 *  Apex Developers
 *  By Victor Rengifo
 */

include_once __DIR__ . '/../../config/config.php';

if ($_SESSION['role'] != 'parent' ) {
    setFlash('error', 'There are some problems .. try again');
    header('Location: /../index.php');
    die;
}

include_once __DIR__ . '/../../lib/cart.php';

if(isset($_GET['empty'])) {
	$_SESSION['cart'] = [];
}

if(cart()){
	$items = getItems($dbh);
	$calcs = calculation($items,$dbh);
}

 // Any page variables
 $page = 'Cart';
 $style = '/../css/pa_cart';
 $page_name = 'Cart | Pizza Time';

// Any page includes

?><!doctype html>
<html lang="en">
<?php require_once __DIR__ . '/../../inc/pa_head.php'; ?>
<body>
    <div id="wrapper">
        <?php require_once __DIR__ . '/../../inc/pa_header.php'; ?>
        <?php include __DIR__ . '/../../inc/flash.inc.php'; ?>
        <div class="master-heading">
            <div class="container">
                <div class="twelve column">
                    <h2><i class="fas fa-shopping-cart"></i> Order review</h2>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="twelve column">
            <?php foreach($items as $item => $line) : ?>
                <div class="section">
                    <div class="heading">
                        <h3>Order Detail</h3>
                        <div class="info-box">
                            <p><strong>Student name:</strong> <?=$line['student_name']?></p>
                            <p><strong>Classroom:</strong> <?=$line['classroom_number']?></p>
                            <p><strong>School name:</strong> <?=$line['school_name']?></p>
                            <p><strong>Event date:</strong> <?=$line['booking_date']?></p>
                            <p><strong><?=$line['item_name']?></strong> x <?=$line['qty']?> ($<?=$line['lineprice']?>)</p>
                        </div>
                    </div>
                </div>

            <?php endforeach; ?>

            <div class="section">
                    <div class="heading">
                        <h3>Order <span><a href="pa_menu.php">Order Summary</a></h3>
                        <div class="info-box">
                            <p class="total"><strong>Sub Total </strong> $<?=$calcs['subtotal']?> CAD</p>
                            <p class="total"><strong>GST </strong> $<?=$calcs['gst']?> CAD</p>
                            <p class="total"><strong>PST </strong> $<?=$calcs['pst']?> CAD</p>
                            <p class="total"><strong>Total </strong> $<?=$calcs['total']?> CAD</p>
                        </div>
                    </div>
                </div>


                <div class="section">
                    <div class="heading">
                        <h3>Payment info</h3>
                    </div>
                </div>
                <div class="payment">
                    <form id="payment-form" method="post" action="">
                        <input type="hidden" name="token" value="">
                            <div class="fields">
                                <div class="field">
                                    <label for="card-name">Card holder name <span>*</span></label>
                                    <input type="text" name="card-name" id="card-name" maxlength="50"
                                        title="Card holder name is required" required>
                                    <!-- <span class="input-error">Error message</span> -->
                                </div>
                                <div class="field">
                                    <label for="last-name">Card number <span>*</span></label>
                                    <input type="text" name="card-number" id="card-number" maxlength="50"
                                        title="Card number is required" required>
                                    <!-- <span class="input-error">Error message</span> -->
                                </div>
                                <div class="field">
                                    <label for="last-name">Card security code <span>*</span></label>
                                    <input type="text" name="card-code" id="card-code" maxlength="3"
                                        title="Card security code is required" required>
                                    <!-- <span class="input-error">Error message</span> -->
                                </div>
                                <div class="field">
                                    <label for="last-name">Card expiration date <span>*</span></label>
                                    <input type="text" name="card-date" id="card-date" maxlength="10"
                                        title="Card expiration date" required>
                                    <!-- <span class="input-error">Error message</span> -->
                                </div>
                            </div>

                        <input type="submit" id="pay-btn" value="Complete order">
                    </form>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="twelve column">
                <div class="token-info">
                    <p><span>*</span> Indicates required field</p>
                </div>
            </div>
        </div>
        <div class="break"></div>
    </div>
    <?php require_once __DIR__ . '/../../inc/footer.php'; ?>
    <div id="overlay"></div>
</body>
</html>
