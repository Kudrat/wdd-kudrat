<?php

require_once __DIR__ . '/../../config/connect.php';
require_once __DIR__ . '/../../config/config.php';

try {
        $id = $_GET['student_id'];
        $booking_id = $_GET['date'];

        $query = "SELECT
                      bookings.booking_id, booking_item.item_id, booking_item.booking_item_id,
                      booking_item.item_name_snapshot, booking_item.item_price_snapshot,
                      booking_item.item_description_snapshot, booking_item.item_image_snapshot
                  FROM students
                  JOIN bookings USING(school_token)
                  JOIN booking_item USING(booking_id)
                  WHERE students.student_id = :id AND bookings.booking_id = :booking_id";

        $stmt = $dbh -> prepare($query);
        $params = array(
            ':id' => $id,
            ':booking_id' => $booking_id
        );
        $stmt -> execute($params);
        $items = $stmt -> fetchAll(PDO::FETCH_ASSOC);

        header('Content-Type: application/json');
        echo json_encode($items);
    } catch (Exception $e) {
        echo $e -> getMessage();
    }
