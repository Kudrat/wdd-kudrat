<?php

include_once __DIR__ . '/../../config/config.php';

/**
 *  School index
 *  Pizza time
 *  Apex Developers
 */

// Any page variables
 $page = 'Home';
 $style = '/../css/pa_index';
 $page_name = 'Pizza Time';
// Any page includes

if ($_SESSION['role'] != 'parent' ) {
    setFlash('error', 'There are some problems .. try again');
    header('Location: /../index.php');
    die;
}

?><!doctype html>
<html lang="en">
<?php require_once __DIR__ . '/../../inc/pa_head.php'; ?>
<body>
    <div id="wrapper">
        <?php require_once __DIR__ . '/../../inc/pa_header.php'; ?>
        <?php include __DIR__ . '/../../inc/flash.inc.php'; ?>

        <div class="main-banner">
            <div class="container">
                <div class="twelve column">
                    <div class="banner">
                        <div class="main-banner-align">
                            <h2>
                                Order school <br>lunches with <br><span id="domino">Domino's</span>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="registration"></div>
        <div class="container">
            <div class="twelve column">
                <div class="registration">
                    <div class="text">
                        <h3>Place your school lunch</h3>
                    </div>
                    <div class="buttons">
                        <ul>
                            <li><a href="pa_menu.php">Schedule now</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="twelve column">
                <div class="text-banner">
                    <div class="text-box-1">
                        <h3>8 delicious toppings</h3>
                        <p>The pizzas can't be customized</p>
                    </div>
                    <div class="text-box-2">
                        <img src="../img/dominos.svg">
                    </div>
                    <div class="text-box-3">
                        <h3>50 Locations in Canada</h3>
                        <p>Restaurants in Manitoba, Saskatchewan and Ontario</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="twelve column">
                <div class="dominos-banner">
                    <div class="banner-2">
                        <div class="main-banner-align-2">
                            <h3>Pizza for dinner?</h3>
                            <a href="https://www.dominos.ca/" target="_blank">Go to Domino's</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="break"></div>
    </div>
    <div id="overlay"></div>
    <?php require_once __DIR__ . '/../../inc/footer.php'; ?>
</body>
</html>
