<?php

require_once __DIR__ . '/../../config/connect.php';
require_once __DIR__ . '/../../config/config.php';

require_once __DIR__ . '/../../lib/cart.php';

/*
// Andre's logic
$cart = array();
if (filter_input(INPUT_SERVER, 'REQUEST_METHOD') == 'POST') {
    //$student = filter_input(INPUT_POST, $student);
    //$date = filter_input(INPUT_POST, $date);

    $post = filter_input_array(INPUT_POST);

    foreach ($post[items] as $key => $value) {
        if($value > 0) {
            $cart[] = array(
                'student_id' => $post['student_id'],
                'booking_id' => $post['booking_id'],
                'item_id' => $key,
                'qty' => $value
            );
        }
    }

    $_SESSION['cart'] = $cart;

}
*/

// VR Logic
if ($_SESSION['role']!="parent") {
    setFlash('error', 'You must be logged into');
    header('Location: /../index.php');
    die;
}

if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    setFlash('error', 'Something happen .. try again');
    header('Location: /../index.php');
    die;
}

addToCart($_POST);
die;