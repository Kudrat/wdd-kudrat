<?php

/**
 *  Parent registration page
 *  Pizza time
 *  Apex Developers
 */

// Any page variables
$page = 'Profile';
$style = '/../css/pa_profile';
$page_name = 'Profile | Pizza Time';


include __DIR__ . '/../../config/config.php';
  
?><!doctype html>
<html lang="en">
<?php require_once __DIR__ . '/../../inc/pa_head.php'; ?>

<?php
 
// If user doesn't log in
if ($_SESSION['role']!="parent") {
    setFlash('error', 'You must be logged into.. try again');
    header('Location: /../index.php');
    die;
}

if (empty($_SESSION['user'])) {
    setFlash('error', 'You must be logged into.. try again');
    header('Location: /../index.php');
    die;
} else {
    $id = intval($_SESSION['user']);
}

// If user is a parent  
/*
if ($_SESSION['role'] == 'parent' ) {
    $id = intval($_SESSION['user']);
} else { // If parent_id in $_SESSION is empty
    setFlash('error', 'There are some problems when getting your information from database');
    header('Location: /../index.php');
    die;
}
*/

$query = "SELECT 
          parents.first_name as first_name,
          parents.last_name as last_name,
          parents.email_address,
          parents.phone, 
          students.school_token,
          students.name as student_name,
          students.classroom_number,
		  domino_schools.name as school_name
          FROM 
          parents 
          JOIN students USING(parent_id) 
          JOIN domino_schools USING(school_token)
		      WHERE 
          parent_id = :parent_id 
          order by students.name asc";

// prepare the query
$stmt = $dbh->prepare($query);

// Prepare params array
$params = array(
    ':parent_id' => $id
);

// execute the query
$stmt->execute($params);

// get the results: parent with 1 or more students
$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
  
?>
<body>
    <div id="wrapper">
        <?php require_once __DIR__ . '/../../inc/pa_header.php'; ?>
        <?php include __DIR__ . '/../../inc/flash.inc.php'?>

        <div class="master-heading">
            <div class="container">
                <div class="twelve column">
                    <h2><i class="fas fa-user"></i> My profile</h2>
                </div>
            </div>
        </div>

        <!-- vr -->

        <div class="container">
            <div class="twelve column">
                <div class="section">
                    <div class="heading">
                        <h3>Personal info</h3>
                        <div class="info-box">
                            <p><strong>First name:</strong> <?=$results[0]['first_name']?></p>
                            <p><strong>Last name:</strong> <?=$results[0]['last_name']?></p>
                            <p><strong>Email:</strong> <?=$results[0]['email_address']?></p>
                            <p><strong>Phone:</strong> <?=$results[0]['phone']?></p>
                        </div>
                    </div>
                </div>

                <?php foreach ($results as $key => $value) : ?>
                <div class="section">
                    <div class="heading">
                        <h3>Student info</h3>
                        <div class="info-box">
                            <p><strong>Full name:</strong> <?=$value["student_name"]?></p>
                            <p><strong>School:</strong> <?=$value["school_name"]?></p>
                            <p><strong>Token:</strong> <?=$value["school_token"]?></p>
                            <p><strong>Classroom:</strong> <?=$value["classroom_number"]?></p>
                        </div>
                    </div>
                    <!-- <div class="add">
                        <a href="">Add another school</a>
                    </div> -->
                </div>
                <?php endforeach; ?>

                <div class="section">
                    <div class="heading">
                        <h3>Pizza lunchs history</h3>
                        <div class="info-box">
                            <p><strong>Aug 16, 2019:</strong> $17,00 CAD</p>
                            <p><strong>Aug 12, 2019:</strong> $12,50 CAD</p>
                        </div>
                    </div>
                </div>
            </div>

            <p class="edit-info">
                <a href="edit.php">Edit profile</a>
            </p>
        </div>
        <!-- vr -->
 
        <div class="break"></div>
    </div>

    <!--?php require_once __DIR__ . '/../../inc/modal.php'; ?-->
    <?php require_once __DIR__ . '/../../inc/footer.php'; ?>
</body>
</html>
