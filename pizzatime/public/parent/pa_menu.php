<?php

require_once __DIR__ . '/../../config/config.php';
/**
 *  Parent menu
 *  Pizza time
 *  Apex Developers
 */

 // Any page variables
 $page = 'Menu';
 $style = '/../css/pa_menu';
 $page_name = 'Menu | Pizza Time';

// Any page includes

try {
    $query1 = "SELECT
                  parents.parent_id, students.student_id AS id, students.name AS student,
                  students.classroom_number, students.school_token AS student_token,
                  domino_schools.school_token, domino_schools.name
              FROM students
              JOIN parents USING(parent_id)
              JOIN domino_schools USING(school_token)
              WHERE parents.parent_id = :parent";
    $params = array(
        ':parent' => $_SESSION['user'],
    );

    $stmt = $dbh->prepare($query1);
    $stmt->execute($params);
    $student = $stmt->fetchAll(PDO::FETCH_ASSOC);

} catch (Exception $e) {
    echo $e -> getMessage();
}

?><!doctype html>
<html lang="en">
<?php require_once __DIR__ . '/../../inc/pa_head.php'; ?>
<body>
    <?php require_once __DIR__ . '/../../inc/pa_header.php'; ?>
        <div class="main-banner">
            <div class="container">
                <div class="twelve column">
                    <div class="banner">
                        <div class="main-banner-align">
                            <h2>
                                Choose your<br><span id="small">kid's favourite</span><br>
                                <span id="domino">pizza</span>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <div id="wrapper">
        <form method="Post" id="menu-form">
            <div class="container">
                <div class="twelve column">
                    <div class="heading">
                        <h2>Event info</h2>
                        <p>Select the student and an available date</p>
                    </div>
                </div>

                <div class="twelve column">
                    <fieldset>
                        <div class="fields">
                            <div class="field">
                                <label for="student-name">Student name <span>*</span></label>
                                <select class="select-css" name="student" id="select" required>
                                    <option selected="" value="">Student</option>
                                    <?php if($student) : ?>
                                       <?php foreach($student as $row) : ?>
                                           <option value="<?= $row['id'] ?>"><?= $row['student'] ?></option>
                                       <?php endforeach; ?>
                                   <?php endif; ?>
                                </select>
                                <!-- <span class="input-error">Error message</span> -->
                                <!-- <div class="student-info">
                                    <p>Canadian "High" High School</p>
                                    <p>Classroom num 3BC52</p>
                                </div>  -->
                            </div>
                            <div class="field">
                                <label for="date">Select date <span>*</span></label>
                                <select class="select-css" name="date" id="select-date" required>
                                    <option selected="" value="">Date</option>
                                </select>
                                        <!-- <span class="input-error">Error message</span> -->
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>

            <div class="container">
                <div class="twelve column">
                    <div class="heading">
                        <h2>Available menu</h2>
                        <p>Choose your pizza slices and add them to cart</p>
                    </div>
                </div>
            </div>

            <div class="columns">
                <div class="container">
                    <div class="twelve column">
                        <div class="items" id="menu-items">
                            <div id="item-placeholder">
                                <p>Select student and date to load the menu options</p>
                            </div>
                        </div>
                    </div>
                    <div class="btn-container">
                        <input type="submit" id="menu-submit" value="Add to cart">
                    </div>
                </div>
            </div>
        </form>

        <div class="container">
            <div class="twelve column">
                <div class="required-message">
                    <p><span>*</span> Indicates required field</p>
                </div>
            </div>
        </div>

        <div class="break"></div>
    </div>

    <?php require_once __DIR__ . '/../../inc/footer.php'; ?>
    <div id="overlay"></div>
    <div id="menu-flash">
        <p>Items added successfully</p>
    </div>
    <script src="../js/menu-btn.js"></script>

    <script>
        function get_dates() {
            $('#select').on('change', function() {
                var id = $(this).find(":checked").val();
                window.current_student_id = id;
                $.ajax ({
                    type: 'GET',
                    url: 'get_dates.php',
                    data: {id: id},
                    dataType: 'json',
                    success: function(result) {
                        $('#select-date').html('');
                        $('#select-date').append('<option>Date</option>');
                        $(result).each(function() {
                            console.log(this.date);
                            var opt = '<option value="' + this.booking_id + '">' + this.date + '</option>';
                            $('#select-date').append(opt);
                            $('.menu-item').css('display', 'none');
                        });
                    }
                });
            });
        }

        function get_items() {
            $('#select-date').on('change', function() {
                var date = $(this).find(":checked").val();
                window.booking_id = date;
                $.ajax ({
                    type: 'GET',
                    url: 'get_items.php',
                    data: {date: date, student_id: window.current_student_id},
                    dataType: 'json',
                    success: function(result2) {
                        console.log(result2);
                        var i = 0;
                        $('#menu-items').html('');
                        $(result2).each(function() {
                            i = i + 1;
                            var item =
                                '<div class="menu-item">' +
                                '<div class="img"><img src="' + this.item_image_snapshot + '"></div>' +
                                '<div class="description"><h3><span>' + this.item_name_snapshot + '</span></h3>' +
                                '<p>' + this.item_description_snapshot + ' - ' + this.item_price_snapshot + '</p></div>' +
                                '<div class="select">' +
                                    '<div class="input">' +
                                        '<input type="text" name="items[]" class="item" data-id="'+ this.booking_item_id +'" id="item-' + this.booking_item_id + '" value="0">' +
                                        '<div class="inc button">+</div><div class="dec button">-</div>' +
                                    '</div>' +
                                '</div>' +
                                '</div>';
                            $('#item-placeholder').css('display', 'none')
                            $('#menu-items').append(item);
                        });
                    }
                });
            });
        }

        $(document).ready(function() {
            get_dates();
            get_items();

            $('#menu-form').submit(function(e) {
                e.preventDefault();
                var items = [];
                $('#menu-form input.item').each(function() {
                    if ($(this).val() > 0) {
                        items[$(this).data('id')] = $(this).val();
                    }
                });

                var data = {};
                data.items = items;
                data.student_id = window.current_student_id;
                data.booking_id = window.booking_id;

                $.post('set_cart.php', data, function(response) {
                    $('#menu-flash').css('visibility', 'visible').slideDown().delay(3000).slideUp('slow');
                });
            });
        });
    </script>
</body>
</html>
