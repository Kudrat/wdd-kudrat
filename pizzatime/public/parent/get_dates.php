<?php

require_once __DIR__ . '/../../config/config.php';


try {
        $id = $_GET['id'];

        $query = "SELECT
                      bookings.booking_id, date_format(bookings.date,'%Y-%m-%d') AS date
                 FROM bookings
                 JOIN students USING(school_token)
                 WHERE students.student_id = :id";

        $stmt = $dbh -> prepare($query);
        $params = array(':id' => $id);
        $stmt -> execute($params);
        $dates = $stmt -> fetchAll(PDO::FETCH_ASSOC);

        header('Content-Type: application/json');
        echo json_encode($dates);
    } catch (Exception $e) {
        echo $e -> getMessage();
    }
