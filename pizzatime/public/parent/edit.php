<?php

// Any page variables
$page = 'Parents';
//$style = '/../css/pa_profile';
$style = '/../css/parents';
$page_name = 'Edit Profile | Pizza time';

include __DIR__ . '/../../inc/pa_head.php';

include __DIR__ . '/../../config/config.php';

// If user is a parent  
if ($_SESSION['role'] == 'parent' ) {
    $id = intval($_SESSION['user']);
} else { // If parent_id in $_SESSION is empty
    setFlash('error', 'There are some problems .. try again');
    header('Location: /../index.php');
    die;
}

$query = "SELECT 
          parents.parent_id,
          parents.first_name as first_name,
          parents.last_name as last_name,
          parents.email_address,
          parents.phone, 
          students.student_id,
          students.school_token,
          students.name as student_name,
          students.classroom_number                 
          FROM 
          parents 
          JOIN students USING(parent_id)
		      WHERE 
          parent_id = :parent_id 
          order by students.name asc";

// prepare the query
$stmt = $dbh->prepare($query);

// Prepare params array
$params = array(
    ':parent_id' => $id
);

// execute the query
$stmt->execute($params);

// get the results: parent with 1 or more students
$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

$v = new classes\Validator();

// Required fields: all fields
$required = ['first_name', 'last_name', 'email_address', 'phone'];

// If REQUEST METHOD is POST
if ('POST' == $_SERVER['REQUEST_METHOD']) {
    foreach ($required as $key => $value) {
      // Required fields validation
      $v->required($value);
    }// end foreach
  
    foreach ($results as $key => $value) {
      $v->required('school_token' . '_' . $value['student_id']); 
      $v->required('student_name' . '_' . $value['student_id']);
      $v->required('classroom_number' . '_' . $value['student_id']); 
    }
  
    if(cleanPOST('how_many_new_students')){
      for($i=1; $i<=cleanPOST('how_many_new_students'); $i++ ){
        $v->required('school_token' . $i); 
        $v->required('student_name' . $i);
        $v->required('classroom_number' . $i);
      }      
    }
  
    // email address and phone format validation
    $v->name('first_name');
    $v->name('last_name');
    $v->email('email_address');
    $v->phone('phone');
      
    $errors = $v->errors();
  
    if (!$errors) {
        try {// Update query for parent          
            $query = "update 
                      parents
                      set
                      first_name = :first_name,
                      last_name = :last_name,
                      email_address = :email_address,
                      phone = :phone,
                      updated_at = now()
                      where
                      parent_id = :parent_id";

            // Prepare query
            $stmt = $dbh->prepare($query);

            // Bind placeholders with inserted values
            $params = array(
                ':first_name' => cleanPOST('first_name'),
                ':last_name' => cleanPOST('last_name'),
                ':email_address' => cleanPOST('email_address'),
                ':phone' => cleanPOST('phone'),
                ':parent_id' => $id
            );

            // execute query
            $stmt->execute($params);
            
            // foreach loop to update more than 1 student field
            foreach ($results as $key => $value) {
              // update query for 1 student
              $query2 = "update 
                         students
                         set
                         school_token = :school_token,
                         name = :name,
                         classroom_number = :classroom_number,
                         updated_at = now()
                         where
                         student_id = :student_id";

              // Prepare query
              $stmt = $dbh->prepare($query2);

              // Bind placeholders with inserted values
              $params2 = array(
                  ':school_token' => cleanPOST('school_token_' . $value['student_id']),
                  ':name' => cleanPOST('student_name_' . $value['student_id']),
                  ':classroom_number' => cleanPOST('classroom_number_' . $value['student_id']),
                  ':student_id' => cleanPOST('student_id_' . $value['student_id'])
              );

              // execute query
              $stmt->execute($params2);
            }
          
            // if 1 or more new student added         
            if(cleanPOST('how_many_new_students')){ 
              for($i=1; $i<=cleanPOST('how_many_new_students'); $i++ ){
                // insert query for second student
                $query3 = "insert into 
                           students
                           (parent_id, school_token, name, classroom_number)
                           values
                           (:parent_id, :school_token, :name, :classroom_number)";

                // Prepare query
                $stmt = $dbh->prepare($query3);

                // Bind placeholders with inserted values
                $params3 = array(
                    ':parent_id' => $id,
                    ':school_token' => cleanPOST('school_token' . $i),
                    ':name' => cleanPOST('student_name' . $i),
                    ':classroom_number' => cleanPOST('classroom_number' . $i)
                );

                // execute query
                $stmt->execute($params3);     
              }                       
            }
            
            setFlash('success', cleanPost('first_name') . ' has been updated successfully.');
            
            header('Location: profile.php');
            die;
            
        } catch (Exception $e) {
            die($e->getMessage());
        }
    } // end if no error
}// end IF POST

?><body>
<div id="wrapper">
  <?php require_once __DIR__ . '/../../inc/pa_header.php'; ?>
  <?php include __DIR__ . '/../../inc/flash.inc.php'; ?>
  <div class="container">
    <div class="twelve column">
      <div class="heading">
        <h2>Profile</h2>
      </div>
    </div>
  </div>
  
  <div class="container">    
    <div class="twelve column">
      <div class="school-reg">
        <form action="<?=$_SERVER['PHP_SELF']?>" 
              method="post" 
              enctype="multipart/form-data"
              >
          <fieldset>
            <div class="form-legend">
                <h3>Parent information</h3>
            </div>
            <div class="fields">
                <div class="field">
                    <label for="first_name">First Name <span>*</span> </label>
                    <input type="text"
                           name="first_name" 
                           id="first_name"
                           value="<?=(empty(cleanPost('first_name')))? $results[0]['first_name'] : cleanPost('first_name')?>" />
                    <!-- Check if output error message -->
                    <?=(isset($errors['first_name'])) ? "<span class='input-error'>{$errors['first_name']}</span>" : '' ?>
                </div>

                <div class="field">
                    <label for="last_name">Last Name <span>*</span> </label>
                    <input type="text"
                           name="last_name" 
                           id="last_name"
                           value="<?=(empty(cleanPost('last_name')))? $results[0]['last_name'] : cleanPost('last_name')?>" />
                    <!-- Check if output error message -->
                    <?=(isset($errors['last_name'])) ? "<span class='input-error'>{$errors['last_name']}</span>" : '' ?>
                </div>
              
                <div class="field">
                    <label for="email_address">Email address <span>*</span> </label>
                    <input type="text"
                           name="email_address" 
                           id="email_address"
                           value="<?=(empty(cleanPost($key)))? $results[0]['email_address'] : cleanPost($key)?>" />
                    <!-- Check if output error message -->
                    <?=(isset($errors['email_address'])) ? "<span class='input-error'>{$errors['email_address']}</span>" : '' ?>
                </div>

                <div class="field">
                    <label for="phone">Phone number <span>*</span> </label>
                    <input type="text"
                           name="phone"
                           id="phone"
                           value="<?=(empty(cleanPost($key)))? $results[0]['phone'] : cleanPost($key)?>" />
                    <!-- Check if output error message -->
                    <?=(isset($errors['phone'])) ? "<span class='input-error'>{$errors['phone']}</span>" : '' ?>
                </div>
            </div>
          </fieldset>
          <!-- double foreach loop to output 1 or more students information -->
          <?php foreach ($results as $key => $value) : ?>
          <fieldset>
            <div class="form-legend">
              <h3>Student information</h3>
            </div>
            <div class="fields">
              <div class="field">
                <?php foreach ($value as $k => $val) : ?>
                <?php if($k != 'parent_id' &&
                         $k != 'first_name' &&
                         $k != 'last_name' &&
                         $k != 'email_address' && 
                         $k != 'phone' &&
                         $k != 'student_id') : ?>
                <label for="<?=$k . '_' . $value['student_id'] ?>"><?=label($k)?> <span>*</span></label>       
                <input type="text"
                       name="<?=$k . '_' . $value['student_id'] ?>"
                       id="<?=$k . '_' . $value['student_id'] ?>"
                       value="<?=(empty(cleanPost($k)))? $val : cleanPost($k)?>" />  
                <!-- Check if output error message -->
                <?=(isset($errors[$k . '_' . $value['student_id']])) ? "<span class='input-error'>{$errors[$k . '_' . $value['student_id']]}</span>" : '' ?>
                <?php endif; ?>
                <?php endforeach; ?>
                <input type="hidden" name="<?='student_id_' . $value['student_id'] ?>" id="<?='student_id_' . $value['student_id'] ?>" value="<?=$value['student_id'] ?>" />
              </div>
            </div>
          </fieldset>
          <?php endforeach; ?>       

          <div id="newStudent"></div>
          
          <div class="add-btn" id="add_student_icon">
            <p><i class="fas fa-plus-circle"></i> Add a student</p>
          </div>
          
          <br />
          
          <input type="submit" id="signup-btn" value="update my profile">
        </form>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="twelve column">
      <div class="required-message">
        <p><span>*</span> Indicates required field</p>
      </div>
    </div>
  </div>
  <div class="break"></div>
</div>
</body>
<script>

      var count = 0;
      window.onload=function(){
          
          document.querySelector('#add_student_icon').addEventListener('click', function(e){
              e.preventDefault();
              loadContent();
          });
      }      
      
      function loadContent()
      {   
        count++;
          var html = "<fieldset>" +
                      "<div class='form-legend'>" +
                        "<h3>Student information</h3>" +
                      "</div>" +
                      "<div class='fields'>" +
                          "<div class='field'>" +
                              "<label for='school_token" + count + "'>School token <span>*</span></label>" +
                              "<input type='text' name='school_token" + count + "'" + "id='school_token" + count + "'" + "maxlength='16'" + "title='School token is required'" + "required>" +
                          "</div>" +
                          
                          "<div class='field'>" +
                              "<label for='student_name" + count + "'>Student Name<span>*</span></label>" +
                              "<input type='text' name='student_name" +count + "'" + "id='student_name" + count + "'" + "maxlength='16'" + "title='Student name is required'" + "required>" +
                          "</div>" +
                          
                          "<div class='field'>" +
                              "<label for='classroom_number" + count + "'>Classroom Number <span>*</span></label>" +
                              "<input type='text' name='classroom_number" +count + "'" + "id='classroom_number" + count + "'" + "maxlength='16'" + "title='Classroom umber is required'" + "required>" +
                          "</div>" +
                    
                          "<input type='hidden' name='how_many_new_students' value='" + count + "'>" + 
                      "</div>" +
                    "</fieldset>"
          
            document.querySelector('#newStudent').insertAdjacentHTML('afterend', html);                
            
      }

  </script>

<!-- Include footer.php -->
<?php include __DIR__ . '/../../inc/footer.php'; ?>
