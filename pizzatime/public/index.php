<?php

include __DIR__ . '/../config/config.php';
/**
 *  Index page
 *  Pizza time
 *  Apex Developers
 */

// Any page variables
 $page = 'Index';
// Any page includes

// sign_out if
if (filter_input(INPUT_GET, 'logout')){
    unset($_SESSION['user']);
    unset($_SESSION['role']);
    unset($_SESSION['csrf_token']);
    unset($_SESSION['user_name']);
    unset($_SESSION['cart']);
    session_regenerate_id();
    setFlash('success', 'You have successfully logged out!');
    header('location:index.php');
    die;
}
     
// session check
if(isset($_SESSION['role'])){
    if($_SESSION['role'] == 'parent'){
    header('Location: /parent/index.php');
        die;
    }elseif($_SESSION['role'] == 'manager'){
        header('Location: /school/index.php');
        die;
    }
}

?><!doctype html>
<html lang="en">
<?php require_once __DIR__ . '/../inc/index_head.php'; ?>
<body>
    <div id="wrapper">
        <?php require_once __DIR__ . '/../inc/header.php'; ?>
        <?php include __DIR__ . '/../inc/flash.inc.php'; ?>
        <div class="main-banner">
            <div class="container">
                <div class="twelve column">
                    <div class="banner">
                        <div class="main-banner-align">
                            <h2>
                                Order school <br>lunches with <br><span id="domino">Domino's</span>
                            </h2>
                            <a href="about.php">How it works</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="registration"></div>
        <div class="container">
            <div class="twelve column">
                <div class="registration">
                    <div class="text">
                        <h3>Start your registration</h3>
                    </div>
                    <div class="buttons">
                        <ul>
                            <li><a href="school.php">I'm a school</a></li>
                            <li><span>Or</span></li>
                            <li><a href="parents.php">I'm a parent</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="twelve column">
                <div class="text-banner">
                    <div class="text-box-1">
                        <h3>8 delicious toppings</h3>
                        <p>The pizzas can't be customized</p>
                    </div>
                    <div class="text-box-2">
                        <img src="img/dominos.svg">
                    </div>
                    <div class="text-box-3">
                        <h3>50 Locations in Canada</h3>
                        <p>Restaurants in Manitoba, Saskatchewan and Ontario</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="twelve column">
                <div class="dominos-banner">
                    <div class="banner-2">
                        <div class="main-banner-align-2">
                            <h3>Pizza for dinner?</h3>
                            <a href="https://www.dominos.ca/" target="_blank">Go to Domino's</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="break"></div>
    </div>

    <?php require_once __DIR__ . '/../inc/modal.php'; ?>
    <?php require_once __DIR__ . '/../inc/footer.php'; ?>
</body>
</html>
