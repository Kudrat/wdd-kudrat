<?php
include __DIR__ . '/../config/config.php';
/**
 *  About page
 *  Pizza time
 *  Apex Developers
 */

 // Any page variables
 $page = 'About';
 $style = 'about';
 $page_name = 'About | Domino\'s Pizza Time';

// Any page includes

?><!doctype html>
<html lang="en">
<?php require_once __DIR__ . '/../inc/common_head.php'; ?>
<body>
    <div id="wrapper">
        <?php require_once __DIR__ . '/../inc/header.php'; ?>
        <?php include __DIR__ . '/../inc/flash.inc.php'; ?>
        <div class="main-banner">
            <div class="container">
                <div class="twelve column">
                    <div class="banner">
                        <div class="main-banner-align">
                            <h2>
                               GIVE STUDENTS<br><span id="big">MEMORABLE</span><br><span id="student">LUNCHES</span>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="twelve column">
                <div class="heading">
                    <div class="text">
                        <h3>It's Pizza Time!</h3>
                        <p>
                            Pizza time is a project that brings the best of Domino's Pizza to schools. Coordinators <br>
                            can easily arrange children's lunch without having to go to the restaurant to order
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="twelve column">
                <div class="heading after">
                    <div class="text">
                        <h3>How it works</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="steps">
                <div class="four column">
                    <div class="step-img">
                        <img src="img/school.jpg">
                    </div>
                    <div class="step-text">
                        <h3>Step 1: school</h3>
                        <p>
                            The school coordinator signs up,<br>
                            selects an available date and the<br>
                            price of each slice of pizza
                        </p>
                    </div>
                </div>

                <div class="four column">
                    <div class="step-img">
                        <img src="img/parent.jpg">
                    </div>
                    <div class="step-text">
                        <h3>Step 2: parents</h3>
                        <p>
                            Registered parents choose their<br>
                            child's favorite pizza slices and<br>
                            pay online
                        </p>
                    </div>
                </div>

                <div class="four column">
                    <div class="step-img">
                        <img src="img/kids-2.jpg">
                    </div>
                    <div class="step-text">
                        <h3>Step 3: student</h3>
                        <p>
                            Students receive and enjoy<br>
                            their pizza during lunch time<br>
                            on pizza day
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="twelve column">
                <div class="registration">
                    <div class="text">
                        <h3>Start your registration</h3>
                    </div>
                    <div class="buttons">
                        <ul>
                            <li><a href="school.php">I'm a school</a></li>
                            <li><span>Or</span></li>
                            <li><a href="parents.php">I'm a parent</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="break"></div>
    </div>

    <?php require_once __DIR__ . '/../inc/modal.php'; ?>
    <?php require_once __DIR__ . '/../inc/footer.php'; ?>
</body>
</html>
