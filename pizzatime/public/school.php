<?php

/**
 *  School registration page
 *  Pizza time
 *  Kowabunga
 */


require __DIR__ . '/../config/config.php';
include __DIR__ . '/../classes/Manager.php';
include __DIR__ . '/../classes/ManagerValidator.php';

$input = new \classes\Input();
// by VR
//$input->setToken();

// Any page variables
$page = 'School';
$style = 'school';
$page_name = 'School registration | Domino\'s Pizza Time';
$schools = Manager::schools($dbh);
$restaurants = Manager::restaurants($dbh);
$v = new \classes\ManagerValidator();

// if post method, validate each form input
if ('POST' == $_SERVER['REQUEST_METHOD']) {

    $input->matchToken(filter_input(INPUT_POST,'csrf_token'),'',1);

    //All required fields
    $required = array(
    'first_name',
    'last_name',
    'phone',
    'email_address',
    'email_confirmation',
    'password',
    'pwd-confirmation',
    'school_token',
    'restaurant_id'
    );

    foreach ($required as $key => $value) {
        $v->required($value);
    }
    $v->name('first_name');
    $v->name('last_name');
    $v->matchEmail('email_address', $dbh);
    $v->password('password', 'pwd-confirmation');
 
    $errors=$v->errors();

    // If there are no errors, user's information is displayed.
    if(empty($errors)) {
            try {
                //Insert input inside managers table
                Manager::create($dbh);
            }catch (Exception $e) {
                die($e->getMessage());
            } //end catch
            header('Location: index.php');
    }// enderrors
} //endpost

?><!doctype html>
<html lang="en">

<?php require_once __DIR__ . '/../inc/common_head.php'; ?>
<body>
    <div id="wrapper">
        <?php require_once __DIR__ . '/../inc/header.php'; ?>
        <?php include __DIR__ . '/../inc/flash.inc.php'; ?>

        <div class="container">
            <div class="twelve column">
                <div class="heading">
                    <h2>Sign up</h2>
                    <p>Fill out the form to create your school coordinator profile</p>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="twelve column">
                <div class="school-reg">
                    <form id="signup-form" method="post" action="">
                        <input type="hidden" name="csrf_token" value="<?=$input->getToken()?>">

                        <fieldset>
                            <div class="form-legend">
                                <h3>Coordinator information</h3>
                            </div>
                            <div class="fields">
                                <div class="field">
                                    <label for="first_name">First name <span>*</span></label>
                                    <input type="text" name="first_name" id="first_name" maxlength="50"
                                        title="First name is required" value="<?=cleanPOST('first_name')?>">
                                        
                                    <?=(!empty($errors['first_name'])) ?
                                    "<span class='input-error'>{$errors['first_name']}</span>" : '' ?>

                                </div>
                                <div class="field">
                                    <label for="last_name">Last name <span>*</span></label>
                                    <input type="text" name="last_name" id="last_name" maxlength="50"
                                        title="Last name is required" value="<?=cleanPOST('last_name')?>">
                                    
                                    <?=(!empty($errors['last_name'])) ?
                                        "<span class='input-error'>{$errors['last_name']}</span>" : '' ?>

                                </div>
                                <div class="field">
                                    <label for="email_address">Email address <span>*</span></label>
                                    <input type="email" name="email_address" id="email_address" maxlength="50"
                                        title="Email is required" value="<?=cleanPOST('email_address')?>">
                                    
                                    <?=(!empty($errors['email_address'])) ?
                                        "<span class='input-error'>{$errors['email_address']}</span>" : '' ?>

                                </div>
                                <div class="field">
                                    <label for="email_confirmation">Confirm email address <span>*</span></label>
                                    <input type="email" name="email_confirmation" id="email_confirmation" maxlength="50"
                                        title="Password is required" value="<?=cleanPOST('email_confirmation')?>">
                                    
                                    <?=(!empty($errors['email_confirmation'])) ?
                                        "<span class='input-error'>{$errors['email_confirmation']}</span>" : '' ?>

                                </div>
                                <div class="field">
                                    <label for="phone">Phone number <span>*</span></label>
                                    <input type="phone" name="phone" id="phone" maxlength="50"
                                        title="Phone is required" value="<?=cleanPOST('phone')?>">
                                    
                                    <?=(!empty($errors['phone'])) ?
                                        "<span class='input-error'>{$errors['phone']}</span>" : '' ?>

                                </div>
                                <div class="field">
                                    <label for="password">Password <span>*</span></label>
                                    <input type="password" name="password" id="password" maxlength="16"
                                        title="Password is required"value="<?=cleanPOST('password')?>">
                                    
                                    <?=(!empty($errors['password'])) ?
                                        "<span class='input-error'>{$errors['password']}</span>" : '' ?>

                                </div>
                                <div class="field">
                                    <label for="pwd-confirmation">Confirm password <span>*</span></label>
                                    <input type="password" name="pwd-confirmation" id="pwd-confirmation" maxlength="16"
                                        title="Password is required" value="<?=cleanPOST('pwd-confirmation')?>">
                                    
                                    <?=(!empty($errors['pwd-confirmation'])) ?
                                        "<span class='input-error'>{$errors['pwd-confirmation']}</span>" : '' ?>

                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <div class="form-legend">
                                <h3>School information</h3>
                            </div>
                            <div class="fields">
                               
                                <div class="field">
                                    <label for="school_token">School name<span>*</span></label>
                                    <select class="select-css" name="school_token" id="school_token">
                                        <option value="">Select your school</option>

                                        <!-- make select option sticky -->
                                         <?php foreach($schools as $value){ 
    
                                            $selected = '';

                                            if($value['school_token']==cleanPOST('school_token')) {
                                                $selected = 'selected';
                                            }
                                         ?>

                                        <option value="<?php echo $value['school_token']; ?>" <?php echo $selected ?>><?php echo $value['name']; ?></option>
                                        <?php } ?>

                                    </select>

                                    <?=(!empty($errors['school_token'])) ?
                                        "<span class='input-error'>{$errors['school_token']}</span>" : '' ?>
                                </div>

                                <div class="field">
                                    <label for="restaurant_id">Restaurant name<span>*</span></label>
                                    <select class="select-css" name="restaurant_id" id="restaurant_id">
                                        <option value="">Select your nearest restaurant</option>

                                        <!-- make select option sticky -->
                                         <?php foreach($restaurants as $value){ 
    
                                            $selected = '';

                                            if($value['restaurant_id']==cleanPOST('restaurant_id')) {
                                                $selected = 'selected';
                                            }
                                         ?>

                                        <option value="<?php echo $value['restaurant_id']; ?>" <?php echo $selected ?>><?php echo $value['restaurant_name']; ?></option>
                                        <?php } ?>

                                    </select>

                                    <?=(!empty($errors['restaurant_id'])) ?
                                        "<span class='input-error'>{$errors['restaurant_id']}</span>" : '' ?>
                                </div>

                                </div>
                            </div>
                        </fieldset>
                        <input type="submit" id="signup-btn" value="Create my profile">
                    </form>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="twelve column">
                <div class="required-message">
                    <p><span>*</span> Indicates required field</p>
                </div>
            </div>
        </div>
        <div class="break"></div>
    </div>

    <?php require_once __DIR__ . '/../inc/modal.php'; ?>
    <?php require_once __DIR__ . '/../inc/footer.php'; ?>
</body>
</html>



