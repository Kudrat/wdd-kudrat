<?php
include __DIR__ . '/../config/config.php';

if('POST' != $_SERVER['REQUEST_METHOD']) {
    header('location:index.php');
    die;
}

// csrf token validation
$input = new \classes\Input();
$tokenIsValid = $input->matchToken(filter_input(INPUT_POST,'token'));

if(!$tokenIsValid){
    die('wrong token');
}

if(isset($_POST['email']) && !isset($_POST['password'])){
    $data1 = [];
    if(!filter_var($_POST['email'],FILTER_VALIDATE_EMAIL)){
        $data['success'] = 'email_false';
    }else{
        $data['success'] = 'email_true';
    }
    header('Content-Type:application/json');
    echo json_encode($data);
    die;
}

if(isset($_POST['password'])){
    $data2 = [];
    if($_POST['password'] == ''){
        $data2['success'] = 'empty_password';
    }else{
        $query = 'select * from parents where email_address = :email';
        $params = array(':email' => $_POST['email']);
        $stmt = $dbh->prepare($query);
        $stmt->execute($params);
        $user = $stmt->fetch(PDO::FETCH_ASSOC);
        if(!$user){
            $query = 'select * from managers where email_address = :email';
            $params = array(':email' => $_POST['email']);
            $stmt = $dbh->prepare($query);
            $stmt->execute($params);
            $user = $stmt->fetch(PDO::FETCH_ASSOC);
        }
        if($user){
            session_regenerate_id();
            if(!password_verify($_POST['password'], $user['password'])){
                $data2['success'] = 'wrong_password';
            }else{              
                if(isset($user['manager_id'])){
                    $_SESSION['user'] = $user['manager_id'];
                    $_SESSION['role'] = 'manager';
                    $_SESSION['user_name'] = $user['first_name'] . " " .$user['last_name'] ;

                    setFlash('success', 'You have successfully logged in');
                    $destination = 'school/index.php';
                    //$destination = 'about.php';
                }elseif(isset($user['parent_id'])){
                    $_SESSION['user'] = $user['parent_id'];
                    $_SESSION['role'] = 'parent';
                    $_SESSION['user_name'] = $user['first_name'] . " " .$user['last_name'] ;
                    setFlash('success', 'You have successfully logged in');
                    
                    $destination = 'parent/index.php';
                    //$destination = 'parents.php';
                }
                
                $data2['success'] = 'true';
                $data2['destination'] = $destination;

            }
        }else{
            $data2['success'] = false;
        }
        
    }
    header('Content-Type:application/json');
    echo json_encode($data2);
    die;
}