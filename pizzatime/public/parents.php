<?php

/**
 *  Parent registration page
 *  Pizza time
 *  Kowabunga
 */

/**
 *@author Kudrat Sidhu <kudratsidhu08@gmail.com>
 *@version 1.0 2019-08-09
 */

                                    

// Any page variables


$page = 'Parents';
$style = 'parents';
$page_name = 'Parent registration | Domino\'s Pizza Time';


                                          // Any page includes
include __DIR__ . '/../config/config.php';
include __DIR__ . '/../classes/ParentValidator.php';
include __DIR__ . '/../classes/Parents.php';

$input = new \classes\Input();
$input->setToken();



 $v = new \classes\ParentValidator();



 // Set flag that form has not been submitted successfully...
 $success = false;

 if ($_SERVER['REQUEST_METHOD'] == 'POST') 
 {
    
    if(!$input->matchToken(filter_input(INPUT_POST,'csrf_token'))) {
        // msg
        setFlash('error','Something happen with your requirement.. try again');
        // redirect to index.php
        header('location:'.$_SERVER['PHP_SELF']);
        die;
    }
    
    // All required fields...
   
   $required = ['first_name',
                'last_name',
                'email_address',
                'email_confirmation',
                'phone',
                'password',
                'password_confirmation',
                'school_token',
                'classroom',
                'student_name'];

                             // Make sure there is a POST value for each
                              //required field 
  
     foreach ($required as $key => $value) 
     {
              $v->required($value);
     }
                   
     $v->email('email_address');
     $v->name('first_name');
     $v->name('last_name');
     $v->name('student_name');
     $v->matchEmail('email_address',$dbh);
     $v->password('password','password_confirmation');
     $v->matchToken('school_token',$dbh);
    
         

     if (!$v->errors()) {
         try {
           
            $parent_id=Parents::createParents($dbh);

            Parents::createStudents($dbh,$parent_id);

           }catch (Exception $e) {
            
             die($e->getMessage());
         }
     } // end if
} //end if post

$errors = $v->errors();

// Any page includes


?><!doctype html>
<html lang="en">
<?php require_once __DIR__ . '/../inc/common_head.php'; ?>


<style>
  .error{
     color: red;
     font-size: 1.6rem;
  }
</style>


<body>
    <div id="wrapper">
        <?php require_once __DIR__ . '/../inc/header.php'; ?>
        <?php require_once __DIR__ . '/../inc/flash.inc.php'; ?>

        <div class="container">
            <div class="twelve column">
                <div class="heading">
                    <h2>Sign up</h2>
                    <p>Fill out the form to create your parent profile</p>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="twelve column">
                <div class="school-reg">

                    <form id="signup-form" method="post"
                     action="<?=$_SERVER['PHP_SELF'] ?>" novalidate>
                        <!-- CSRF TOKEN -->
                        <input type="hidden" name="csrf_token" value="<?=$input->getToken()?>">


                        <fieldset>
                            <div class="form-legend">
                                <h3>Parent information</h3>
                            </div>
                            <div class="fields">
                                <div class="field">

                                    <label for="first_name">First name <span>*</span></label>
                                    <input type="text" name="first_name" id="first_name" maxlength="50"
                                    value="<?=cleanPost('first_name')?>"/>
                                    <?=(isset($errors['first_name'])) ? "<span class='error'>{$errors['first_name']}</span>" : '' ?>
                                </div>
                                <div class="field">
                                    <label for="last_name">Last name <span>*</span></label>
                                    <input type="text" name="last_name" id="last_name" maxlength="50"
                                    value="<?=cleanPost('last_name')?>"/>
                                    <?=(isset($errors['last_name'])) ? "<span class='error'>{$errors['last_name']}</span>" : '' ?>
                                   
                                </div>
                                <div class="field">
                                    <label for="email_address">Email address <span>*</span></label>
                                    <input type="email" name="email_address" id="email_address" maxlength="50"
                                    value="<?=cleanPost('email_address')?>"/>
                                    <?=(isset($errors['email_address'])) ? "<span class='error'>{$errors['email_address']}</span>" : '' ?>
                                  
                                </div>
                                <div class="field">
                                    <label for="email_confirmation">Confirm email address <span>*</span></label>
                                    <input type="email" name="email_confirmation" id="email_confirmation" maxlength="50"
                                    value="<?=cleanPost('email_confirmation')?>"/>
                                    <?=(isset($errors['email_confirmation'])) ? "<span class='error'>{$errors['email_confirmation']}</span>" : '' ?>

                                </div>
            
                                <div class="field">
                                    <label for="phone">Phone number <span>*</span></label>
                                    <input type="phone" name="phone" 
                                    id="phone" maxlength="50"

                                    value="<?=cleanPost('phone')?>"/>
                                    <?=(isset($errors['phone'])) ? "<span class='error'>{$errors['phone']}</span>" : '' ?>

                                </div>

                                <div class="field">
                                    <label for="password">Password <span>*</span></label>
                                    <input type="password" 
                                    name="password" id="password" maxlength="16"

                                    value="<?=cleanPost('password')?>"/>
                                    <?=(isset($errors['password'])) ? "<span class='error'>{$errors['password']}</span>" : '' ?>
                                </div>
                                <div class="field">
                                    <label for="password_confirmation">Confirm password <span>*</span></label>
                                    <input type="password" name="password_confirmation" 
                                    id="password_confirmation" maxlength="16"
                                    value="<?=cleanPost('password_confirmation')?>"/>
                                    <?=(isset($errors['password_confirmation'])) ? "<span class='error'>{$errors['password_confirmation']}</span>" : '' ?>
                                </div>
                        
                            </div>
                        </fieldset>

                        <fieldset>
                            <div class="form-legend">
                                <h3>Student information</h3>
                            </div>

                             
                             <input type="hidden" name="parent_id">


                            <div class="fields">
                                <div class="field">
                                    <label for="school_token">School token <span>*</span></label>
                                    <input type="text" name="school_token" id="school_token" maxlength="16"
                                    value="<?=cleanPost('school_token')?>"/>
                                    <?=(isset($errors['school_token'])) ? "<span class='error'>{$errors['school_token']}</span>" : '' ?>
                                </div>

                                <div class="field">
                                    <label for="classroom">Classroom number <span>*</span></label>
                                    <input type="text" name="classroom" id="classroom" maxlength="50"
                                    value="<?=cleanPost('classroom')?>"/>
                                    <?=(isset($errors['classroom'])) ? "<span class='error'>{$errors['classroom']}</span>" : '' ?>
                                </div>

                                <div class="field">
                                    <label for="student_name">Student name <span>*</span></label>
                                    <input type="text" name="student_name" id="student_name" maxlength="16"
                                    value="<?=cleanPost('student_name')?>"/>
                                    <?=(isset($errors['student_name'])) ? "<span class='error'>{$errors['student_name']}</span>" : '' ?>
                                </div>
                             </div>
                        </fieldset>
                        <input type="submit" id="signup-btn" value="Create my profile">
                    </form>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="twelve column">
                <div class="required-message">
                    <p><span>*</span> Indicates required field</p>
                </div>
            </div>
        </div>
        <div class="break"></div>
    </div>

    <?php require_once __DIR__ . '/../inc/modal.php'; ?>
    <?php require_once __DIR__ . '/../inc/footer.php'; ?>
</body>
</html>
