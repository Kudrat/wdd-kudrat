<?php

include __DIR__ . '/../../config/config.php';
include __DIR__ . '/../../classes/ManagerBooking.php';

/**
 *  Parent cart
 *  Pizza time
 *  Apex Developers
 */

 // Any page variables
 $page = 'Cart';
 $style = '/../css/sc_cart';
 $page_name = 'Event | Pizza Time';

// If user doesn't log in
if ($_SESSION['role']!="manager") {
    setFlash('error', 'You must be logged into');
    header('Location: /../index.php');
    die;
}

$pizzadays = ManagerBooking::pizza_days($_SESSION['user'],$dbh);

?><!doctype html>
<html lang="en">
<?php require_once __DIR__ . '/../../inc/pa_head.php'; ?>
<body>
    <div id="wrapper">
        <?php require_once __DIR__ . '/../../inc/sc_header.php'; ?>
        <?php include __DIR__ . '/../../inc/flash.inc.php'; ?>

        <div class="master-heading">
            <div class="container">
                <div class="twelve column">
                    <h2><i class="fas fa-school"></i> Event review</h2>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="twelve column">
                <div class="section">
                    <div class="heading">
                        <h3>School info</h3>
                        <div class="info-box">        
                            <p><strong>Name:</strong> <?=$pizzadays[0]['school_name']?></p>
                            <p><strong>Address:</strong> <?=$pizzadays[0]['school_address']?></p>
                            <p><strong>Token:</strong> <?=$pizzadays[0]['school_token']?></p>
                            <p><strong>Restaurant:</strong> <?=$pizzadays[0]['restaurant_name']?></p>
                            <p><strong>Rest. Address:</strong> <?=$pizzadays[0]['restaurant_address']?></p>
                        </div>
                    </div>
                </div>
                
                <div class="section">
                    <div class="heading">
                        <h3>
                            Pizza's days <!--<span><a href="pa_menu.php">Change event</a>-->
                        </h3>
                        <?php 
                            if(!empty($pizzadays[0]['booking_id'])) {
                                foreach ($pizzadays as $key => $value) {
                                ?>
                            <div class="info-box">
                                <p><strong>Created by:</strong> <?=$value['manager']?></p>
                                <p><strong>Day:</strong> <?=$value['date']?></p>
                                <p><strong>Markup price:</strong> <?=$value['markup_price']?> %</p>
                            </div>
                                <?php
                                }
                            } else {
                        ?>
                        <div class="info-box">
                            <p><strong>Sorry</strong>, you don't have pizza's days</p>
                        </div>                            
                        <?php
                            }
                        ?>
                    </div>
                </div>

            </div>
        </div>

        <div class="break"></div>
    </div>
    <?php require_once __DIR__ . '/../../inc/footer.php'; ?>
    <div id="overlay"></div>
</body>
</html>
