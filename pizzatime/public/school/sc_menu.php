<?php

include __DIR__ . '/../../config/config.php';
include __DIR__ . '/../../classes/ManagerBooking.php';
/**
 *  School coordinator menu
 *  Pizza time
 *  Apex Developers
 */

 // Any page variables
 $page = 'School lunch';
 $style = '/../css/sc_menu';
 $page_name = 'School lunch | Pizza Time';

 $input = new \classes\Input();

// If user doesn't log in
if ($_SESSION['role']!="manager") {
    setFlash('error', 'You must be logged into to see the profile page');
    header('Location: /../index.php');
    die;
}

/**
 * when submit
 */
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    
    // verify csrf_token
    $input->matchToken(filter_input(INPUT_POST,'csrf_token'),'',1);
    // validate data
    $v = new \classes\Validator();

    // individual validations
    // rules
    $rules = array(
        "school" => array("type" => "string","min_size" => 1,"max_size" => 45),
        "date" => array("type" => "string","min_size" => 10,"max_size" => 10),
        "restaurant" => array("type" => "integer"),
        "markup" => array("type" => "percentage","min_size" => 1,"max_size" => 100),
    );
   
    $v->setRules($rules);
    // executing the validation process based on type   
    $v->validate();
    
    $errors = $v->errors();

    if(!$errors) {
        // save data
        $result = ManagerBooking::create($dbh);
        if($result) {
            // redirect
            header('location:sc_thank_you.php');
            die;
        } else {

        }   
    }
}
// end submit


/**
 * Getting data to show in the form
 */
// get schools
$school_data = ManagerBooking::school($_SESSION['user'],$dbh);

// get dates for restaurant
$restaurant_data= ManagerBooking::restaurantDates($school_data['school_token'],$school_data['restaurant_id'],$dbh);

// restaurant info
$restaurantinfo_data = array(
    "restaurant_id" => $restaurant_data[0]["restaurant_id"],
    "restaurant_name" => $restaurant_data[0]["restaurant_name"],
);

// get items
$item_data = ManagerBooking::items($dbh);



?><!doctype html>
<html lang="en">
<?php require_once __DIR__ . '/../../inc/pa_head.php'; ?>
<body>
    <div id="wrapper">
        <?php require_once __DIR__ . '/../../inc/sc_header.php'; ?>
        <?php include __DIR__ . '/../../inc/flash.inc.php'; ?>
        <div class="main-banner">
            <div class="container">
                <div class="twelve column">
                    <div class="banner">
                        <div class="main-banner-align">
                            <h2>
                                Choose your<br><span id="small">school event's</span><br>
                                <span id="domino">pizza day</span>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include __DIR__ . '/../../inc/flash.inc.php'; ?>

        <form action="<?=$_SERVER['PHP_SELF']?>" method="post" name="scmenu" id="scmenu">
            <input type="hidden" name="csrf_token" value="<?=$input->getToken()?>">

            <div class="container">
                <div class="twelve column">
                    <div class="heading">
                        <h2>Set a pizza event</h2>
                        <p>Schedule a restaurant and set the markup percentage</p>
                    </div>
                </div>

                <div class="twelve column">
                    <fieldset>
                        <div class="fields">
                            <div class="field">
                                <label for="first-name">School name <span>*</span></label>
                                <select class="select-css" name="school" id="school">
                                        <option value="<?=$school_data['school_token']?>"><?=$school_data['school_name']?></option>
                                </select>
                                <!-- <span class="input-error">Error message</span> -->
                                <div class="school-info">
                                    <p>School token <?=$school_data['school_token']?></p>
                                </div>
                            </div>

                            <div class="field">
                                <label for="first-name">Select date <span>*</span></label>
                                <select class="select-css" name="date" id="date">
                                <?php foreach ($restaurant_data as $key => $value) : ?>
                                    <?php  
                                        ($value['date']==cleanPost('date')) ? $selected = " selected" : $selected = '';
                                    ?>
                                    <option value="<?=$value['date']?>" <?=$selected?>><?=$value['date']?></option>
                                <?php endforeach; ?>
                                </select>
                                <!-- <span class="input-error">Error message</span> -->
                            </div>

                            <div class="field">
                                <label for="first-name">Select restaurant <span>*</span></label>
                                <select class="select-css" name="restaurant" id="restaurant">
                                    <option value="<?=$restaurantinfo_data['restaurant_id']?>"><?=$restaurantinfo_data['restaurant_name']?></option>
                                </select>
                                <!-- <span class="input-error">Error message</span> -->
                            </div>

                            <div class="items">
                                <?php foreach ($item_data as $key => $value) : ?>
                                <div class="menu-item">
                                    <div class="img"><img src="<?=$value['item_image']?>"></div>
                                    <div class="description">
                                        <h3><span><?=$value['item_name']?></span></h3>
                                        <p><?=$value['item_description']?> - $<?=$value['item_price']?></p>
                                    </div>
                                </div>
                                <?php endforeach; ?>
                            </div>

                            <div class="field">
                                <label for="first-name">Markup Percentage <span>*</span></label>
                                <input type="text" name="markup" id="markup" maxlength="3" 
                                    value="<?=cleanPOST('markup')?>"
                                    title="Percentage is required" placeholder="%" required>
                                <?=(!empty($errors['markup'])) ?
                                    "<span class='input-error'>{$errors['markup']}</span>" : '' ?>
                            </div>
                        </div>
                    </fieldset>
                    <div class="btn-container">
                        <input type="submit" id="event-submit" value="Save">
                    </div>
                </div>
            </div>
        </form>
        <div class="container">
            <div class="twelve column">
                <div class="required-message">
                    <p><span>*</span> Indicates required field</p>
                </div>
            </div>
        </div>

        <div class="break"></div>
    </div>
    <div id="overlay"></div>
    <?php require_once __DIR__ . '/../../inc/footer.php'; ?>
</body>
</html>
