<?php

include __DIR__ . '/../../config/config.php';

/**
 *  School thank you
 *  Pizza time
 *  Apex Developers
 */

 // If user doesn't log in
if ($_SESSION['role']!="manager") {
    setFlash('error', 'You must be logged into to see the profile page');
    header('Location: /../index.php');
    die;
}

 // Any page variables
 $page = 'Thank';
 $style = '/../css/pa_thank_you';
 $page_name = 'Thank you | Pizza Time';

// Any page includes

?><!doctype html>
<html lang="en">
<?php require_once __DIR__ . '/../../inc/pa_head.php'; ?>
<body>
    <div id="wrapper">
        <?php require_once __DIR__ . '/../../inc/sc_header.php'; ?>

        <div class="master-heading">
            <div class="container">
                <div class="twelve column">
                    <h2><i class="fas fa-heart"></i> Thank you</h2>
                </div>
            </div>
        </div>
        <?php include __DIR__ . '/../../inc/flash.inc.php'; ?>

        <div class="container">
            <div class="twelve column">
                <div class="section">
                    <div class="heading">
                        <h3>Order status</h3>
                        <div class="info-box">
                            <p><strong>Your pizza lunch was completed successfully</strong></p>
                            <p>
                                An email receipt including the details about your order has
                                been sent to the email address provided. Please keep it for your
                                records.  Or consider to use your <strong>Pizza's Days</strong> in the menu
                            </p>
                        </div>
                    </div>
                </div>

                <div class="section">
                    <div class="heading">
                        <h3>Pizza time message</h3>
                        <div class="info-box">
                            <p>
                                Remember, the orders are accepted 5 days previous to the pizza's day
                            </p>
                        </div>
                        <a href="sc_menu.php">+ School Lunch</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="break"></div>
    </div>
    <?php require_once __DIR__ . '/../../inc/footer.php'; ?>
    <div id="overlay"></div>
</body>
</html>
