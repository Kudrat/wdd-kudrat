<?php

/**
 *  School coordinator profile
 *  Pizza time
 *  Apex Developers
 */

include __DIR__ . '/../../config/config.php';

 // Any page variables
 $page = 'Profile';
 $style = '/../css/pa_profile';
 $page_name = 'Profile | Pizza Time';
  
?><!doctype html>
<html lang="en">
<?php require_once __DIR__ . '/../../inc/pa_head.php'; ?>

<?php
 
// If user doesn't log in
if ($_SESSION['role']!="manager") {
    setFlash('error', 'You must be logged into to see the profile page');
    header('Location: /../index.php');
    die;
}

if (empty($_SESSION['user'])) {
    setFlash('error', 'You must be logged into to see the profile page');
    header('Location: /../index.php');
    die;
} else {
    $id = intval($_SESSION['user']);
}

$query = "SELECT 
          managers.first_name as first_name,
          managers.last_name as last_name,
          managers.email_address,
          managers.phone, 
          domino_schools.school_token as school_token,
          domino_schools.name as school_name,
          domino_schools.address
          FROM 
          managers
          JOIN domino_schools USING(school_token)
		      WHERE 
          manager_id = :manager_id";

// prepare the query
$stmt = $dbh->prepare($query);

// Prepare params array
$params = array(
    ':manager_id' => $id
);

// execute the query
$stmt->execute($params);

// get the results: parent with 1 or more students
$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
  
?>
<body>
    <div id="wrapper">
        <?php require_once __DIR__ . '/../../inc/sc_header.php'; ?>
        <?php include __DIR__ . '/../../inc/flash.inc.php'?>

        <div class="master-heading">
            <div class="container">
                <div class="twelve column">
                    <h2><i class="fas fa-user"></i> My profile</h2>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="twelve column">
                <div class="section">
                    <div class="heading">
                        <h3>Personal info</h3>
                        <div class="info-box">
                            <p><strong>First name:</strong> <?=$results[0]['first_name']?></p>
                            <p><strong>Last name:</strong> <?=$results[0]['last_name']?></p>
                            <p><strong>Email:</strong> <?=$results[0]['email_address']?></p>
                            <p><strong>Phone:</strong> <?=$results[0]['phone']?></p>
                        </div>
                    </div>
                </div>

                <?php foreach ($results as $key => $value) : ?>
                <div class="section">
                    <div class="heading">
                        <h3>School info</h3>
                        <div class="info-box">
                            <p><strong>Name:</strong> <?=$value["school_name"]?></p>                          
                            <p><strong>School Token:</strong> <?=$value["school_token"]?></p>
                            <p><strong>Address:</strong> <?=$value["address"]?></p>
                        </div>
                    </div>
                    <!-- <div class="add">
                        <a href="">Add another school</a>
                    </div> -->
                </div>
                <?php endforeach; ?>                
            </div>

            <p class="edit-info">
                <a href="edit.php">Edit profile</a>
            </p>
        </div>
        <!-- vr -->
      
       
        <div class="container">
            <div class="twelve column">
                <div class="token-info">
                    <p><span>*</span> Tokens cannot be edited by the users</p>
                </div>
            </div>
        </div>
        <div class="break"></div>
    </div>

    <div id="overlay"></div>

    
    <?php require_once __DIR__ . '/../../inc/footer.php'; ?>
</body>
</html>
