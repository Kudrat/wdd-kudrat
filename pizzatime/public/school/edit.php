<?php
include __DIR__ . '/../../config/config.php';

use classes\Validator;

// Any page variables
$page = 'Managers';
$style = '/../css/parents';
$page_name = 'Managers|Managers Profile Edit';

include __DIR__ . '/../../inc/pa_head.php';

// If user is a parent  
if ($_SESSION['role'] == 'manager' ) {
    $id = intval($_SESSION['user']);
} else { // If parent_id in $_SESSION is empty
    setFlash('error', 'There are some problems .. try again');
    header('Location: /../index.php');
    die;
}

$query = "SELECT 
          managers.first_name as first_name,
          managers.last_name as last_name,
          managers.email_address,
          managers.phone, 
          domino_schools.school_token as school_token,
          domino_schools.name as school_name,
          domino_schools.address
          FROM 
          managers
          JOIN domino_schools USING(school_token)
		      WHERE 
          manager_id = :manager_id";

// prepare the query
$stmt = $dbh->prepare($query);

// Prepare params array
$params = array(
    ':manager_id' => $id
);

// execute the query
$stmt->execute($params);

// get the result
$result = $stmt->fetch(PDO::FETCH_ASSOC);

$v = new Validator();

// Required fields: all fields
$required = ['first_name', 'last_name', 'email_address', 'phone'];

// If REQUEST METHOD is POST
if ('POST' == $_SERVER['REQUEST_METHOD']) {
    foreach ($required as $key => $value) {
      // Required fields validation
      $v->required($value);
    }// end foreach 
  
    // email address and phone format validation
    $v->name('first_name');
    $v->name('last_name');
    $v->email('email_address');
    $v->phone('phone');
      
    $errors = $v->errors();
    
    foreach ($required as $key => $value) {
      // Required fields validation
      $v->required($value);
    }// end foreach 
  
    // email address and phone format validation
    $v->email('email_address');
    $v->phone('phone');
      
    $errors = $v->errors();
  
    if (!$errors) {
        try {// Update query for manager          
            $query = "update 
                      managers
                      set
                      first_name = :first_name,
                      last_name = :last_name,
                      email_address = :email_address,
                      phone = :phone,
                      updated_at = now()
                      where
                      manager_id = :manager_id";

            // Prepare query
            $stmt = $dbh->prepare($query);

            // Bind placeholders with inserted values
            $params = array(
                ':first_name' => cleanPOST('first_name'),
                ':last_name' => cleanPOST('last_name'),
                ':email_address' => cleanPOST('email_address'),
                ':phone' => cleanPOST('phone'),
                ':manager_id' => $id
            );

            // execute query
            $stmt->execute($params);
            
            
            setFlash('success', cleanPost('first_name') . ' has been updated successfully.');
            
            header('Location: profile.php');
            die;            
        } catch (Exception $e) {
            die($e->getMessage());
        }
    } // end if no error
}// end IF POST

?><body>
<div id="wrapper">
  <?php require_once __DIR__ . '/../../inc/sc_header.php'; ?>
  <?php include __DIR__ . '/../../inc/flash.inc.php'; ?>  
  <div class="container">
    <div class="twelve column">
      <div class="heading">
        <h2>Profile</h2>
      </div>
    </div>
  </div>
  
  <div class="container">    
    <div class="twelve column">
      <div class="school-reg">
        <form action="<?=$_SERVER['PHP_SELF']?>" 
              method="post" 
              enctype="multipart/form-data"
              >
          <fieldset>
            <div class="form-legend">
                <h3>Manager information</h3>
            </div>
            <div class="fields">
                <div class="field">
                    <label for="first_name">First Name <span>*</span> </label>
                    <input type="text"
                           name="first_name" 
                           id="first_name"
                           value="<?=(empty(cleanPost('first_name')))? $result['first_name'] : cleanPost('first_name')?>" />
                    <!-- Check if output error message -->
                    <?=(isset($errors['first_name'])) ? "<span class='input-error'>{$errors['first_name']}</span>" : '' ?>
                </div>

                <div class="field">
                    <label for="last_name">Last Name <span>*</span> </label>
                    <input type="text"
                           name="last_name" 
                           id="last_name"
                           value="<?=(empty(cleanPost('last_name')))? $result['last_name'] : cleanPost('last_name')?>" />
                    <!-- Check if output error message -->
                    <?=(isset($errors['last_name'])) ? "<span class='input-error'>{$errors['last_name']}</span>" : '' ?>
                </div>
              
                <div class="field">
                    <label for="email_address">Email address <span>*</span> </label>
                    <input type="text"
                           name="email_address" 
                           id="email_address"
                           value="<?=(empty(cleanPost($key)))? $result['email_address'] : cleanPost($key)?>" />
                    <!-- Check if output error message -->
                    <?=(isset($errors['email_address'])) ? "<span class='input-error'>{$errors['email_address']}</span>" : '' ?>
                </div>

                <div class="field">
                    <label for="phone">Phone number <span>*</span> </label>
                    <input type="text"
                           name="phone"
                           id="phone"
                           value="<?=(empty(cleanPost($key)))? $result['phone'] : cleanPost($key)?>" />
                    <!-- Check if output error message -->
                    <?=(isset($errors['phone'])) ? "<span class='input-error'>{$errors['phone']}</span>" : '' ?>
                </div>
            </div>
          </fieldset>
          
          <br />
          
          <input type="submit" id="signup-btn" value="update my profile">
        </form>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="twelve column">
      <div class="required-message">
        <p><span>*</span> Indicates required field</p>
      </div>
    </div>
  </div>
  <div class="break"></div>
</div>
</body>


<!-- Include footer.php -->
<?php include __DIR__ . '/../../inc/footer.php'; ?>
