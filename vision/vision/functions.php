<?php
/**
 * MyVision functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package MyVision
 */

if ( ! function_exists( 'vision_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function vision_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on MyVision, use a find and replace
		 * to change 'vision' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'vision', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in two location.
		//https://developer.wordpress.org/reference/function/register_nav_menu/
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'int-cansustain' ),
			'social' => esc_html__( 'social', 'int-cansustain' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'vision_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );

		  /***
     * Block Related add_theme_supports
     * @link https://developer.wordpress.org/block-editor/developers/themes/theme-support/
     */

    /**
     * Block Color Palettes
     * @link https://developer.wordpress.org/block-editor/developers/themes/theme-support/#block-color-palettes 
     */
    add_theme_support( 'editor-color-palette', array(
    	array(
    		'name' => __( 'strong magenta', 'vision' ),
    		'slug' => 'strong-magenta',
    		'color' => '#a156b4',
    	),
    	array(
    		'name' => __( 'light grayish magenta', 'vision' ),
    		'slug' => 'light-grayish-magenta',
    		'color' => '#d0a5db',
    	),
    	array(
    		'name' => __( 'very light gray', 'vision' ),
    		'slug' => 'very-light-gray',
    		'color' => '#eee',
    	),
    	array(
    		'name' => __( 'very dark gray', 'vision' ),
    		'slug' => 'very-dark-gray',
    		'color' => '#444',
    	),
    ) );

    /**
     * Disabling custom colors in block Color Palettes
     * Theme users can't create their own colours. Must use the theme colours
     * designated in add_theme_support( 'editor-color-palette', array())
     * @link https://developer.wordpress.org/block-editor/developers/themes/theme-support/#disabling-custom-colors-in-block-color-palettes
     */
    add_theme_support( 'disable-custom-colors' );

    /**
     * Wide Alignment
     * This theme supports align wide for blocks that support it 
     * @link https://developer.wordpress.org/block-editor/developers/themes/theme-support/#wide-alignment
     */
    add_theme_support( 'align-wide' );
    
    /**
     * Block Font Sizes
     * Font size overrides for this theme 
     * @link https://developer.wordpress.org/block-editor/developers/themes/theme-support/#block-font-sizes
     */
    add_theme_support( 'editor-font-sizes', array(
    	array(
    		'name' => __( 'Small', 'vision' ),
    		'size' => 12,
    		'slug' => 'small'
    	),
    	array(
    		'name' => __( 'Normal', 'vision' ),
    		'size' => 16,
    		'slug' => 'normal'
    	),
    	array(
    		'name' => __( 'Large', 'vision' ),
    		'size' => 36,
    		'slug' => 'large'
    	),
    	array(
    		'name' => __( 'Larger', 'vision' ),
    		'size' => 50,
    		'slug' => 'huge'
    	)
    ));

    /**
     * Disabling custom font sizes
     * Theme only allows our custom font sizes 
     * @link https://developer.wordpress.org/block-editor/developers/themes/theme-support/#disabling-custom-font-sizes
     */
    add_theme_support('disable-custom-font-sizes');

    /**
     * Custom Editor Styles
     * Enable custom editor styles to ensure our editor styles look like the theme styles 
     * @link https://developer.wordpress.org/block-editor/developers/themes/theme-support/#editor-styles
     */
    add_theme_support('editor-styles');

    /** 
     * Dark Backgrounds in Editor
     * Enables dark theme in the editor if our theme utilizes dark backgrounds with light text 
     * Requires add_theme_support('editor-styles') to work!
     * @link https://developer.wordpress.org/block-editor/developers/themes/theme-support/#dark-backgrounds
     */
    //add_theme_support( 'dark-editor-style' );
    
    /**
     * Enqueuing the editor style
     * @link https://developer.wordpress.org/block-editor/developers/themes/theme-support/#enqueuing-the-editor-style
     * This is our style sheet that will be loaded for our theme specific editor styles
     * Requires add_theme_support('editor-styles') to work!
     */
    add_editor_style( 'style-editor.css' );

    /**
     * Default Block Styles
     * Uses cores default styles for our blocks
     * @link https://developer.wordpress.org/block-editor/developers/themes/theme-support/#default-block-styles
     */
    add_theme_support( 'wp-block-styles' );

		/**
		*Block related add_theme_supports
		*
		*/
	}
endif;
add_action( 'after_setup_theme', 'vision_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function vision_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'vision_content_width', 640 );
}
add_action( 'after_setup_theme', 'vision_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function vision_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'vision' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'vision' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'vision_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function vision_scripts() {
	wp_enqueue_style( 'vision-style', get_stylesheet_uri() );

	wp_enqueue_script( 'vision-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'vision-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'vision_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

