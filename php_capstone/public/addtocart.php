<?php

require __DIR__ . '/../config/config.php';
require __DIR__ . '/../config/functions.php';

if ('POST' !== $_SERVER['REQUEST_METHOD']) {
    show_source(__FILE__);
    die('Direct access not allowed');
}

addToCart($_POST);
$_SESSION['message'] = 'Item added to cart';
// redirect to the page that sent us(the referer)
header('Location: ' . $_SERVER['HTTP_REFERER']);
die;
