<?php

//included files
include  __DIR__ . '/../config/config.php';
include __DIR__ . '/../config/functions.php';
include __DIR__ . '/../classes/Validator.php';


$TITLE = 'UserLogin';
$body_id="userlogin";
$title = "Login";



if(filter_input(INPUT_GET, 'logout')){
    session_destroy();
    setflash('success', "You've successfully logged out");

    header('Location:login.php');
    die;
}



$v = new Validator();
// validating by using filter_input
if ('POST' == filter_input(INPUT_SERVER, 'REQUEST_METHOD')) {
//validator both fields which require
    $v->required('email');
    $v->email('email');
    $v->required('password');


//if no v Errors
    if (!$v->errors()) {
        $post = filter_input_array(INPUT_POST);

//query data base for user with email
        $query = 'SELECT * FROM users WHERE email = :email';
//stmt prepare $query
        $stmt = $dbh->prepare($query);
//params 
        $params = array(':email'=> e_attr($post['email']));
//executed the stmt
        $stmt->execute($params);
//fetch data assigne to $user
        $user = $stmt->fetch(PDO::FETCH_ASSOC);


        $form_password = $post['password'];
        $stored_password = $user['password'];




//verify the form password matches the stored password
        if (password_verify($form_password, $stored_password)) {
            session_regenerate_id();
           // passord matches to admin credentials admin able to view logs, able to create update delete and insert data
            if($user['role'] == 'admin'){
                $_SESSION['admin'] = 1;
             

            }

            $_SESSION['user_id'] = $user['user_id'];
              
            //if session logged_in true set flash message
            $_SESSION['logged_in'] = 1;
            setFlash('success', 'You logged in successfully!');

            
            header('Location: users_detail.php');
            

            die;
        } else {
            setFlash('error', 'There was a problem with your credentials');
}//if password verify ends
}//if errors end
}//if post end


$errors= $v->errors();

include __DIR__ . '/../include/header.inc.php';


?><div id="wrapper">

    <?php include __DIR__ . '/../config/flash.inc.php'; ?>

    <main>
      



        <div id="login">
            <h1><?=$heading?></h1>

            <?php require __DIR__ . '/../config/errors.php'; ?> 

            <form method="post" action="<?=filter_input(INPUT_SERVER, 'PHP_SELF', FILTER_SANITIZE_STRING)?>" >
                <input type="hidden" name="csrf_token" value="<?=getToken()?>">
                <p>
                    <input type="email" name="email" placeholder="email"
                    value="<?=clean('email')?>" />
                </p>

                <p>
                    <input type="password" name="password" placeholder="password"
                    value="<?=clean('password')?>" />
                </p>

                <p> 
                    <input value="Login" type="submit" /><!--Buttons-->
                </p>  

            </form>

            <div id = 'welcome'>
                <h2>Welcome</h2>
                <p>Helping Hands is the local Winnipeg service providers. They are well known around the city for the quality of work, fair pricing and punctuality. Helping Hands provides basic common services of day today life. Services related to House-keeping, Plumbing, Carpentry, Electricalwork, Laundry, Pest-control, Beauty, Laptop Repair and many more. Redevelopment of their website will be designed to retain current customers and increase new visitors. This will be achieved by modernising the website and add new features such as social media, links, online registration system to have easy excess to services and user friendly with functional navigation.  </p>

            </div>
        </div>

    </main>
</div>





<!--footer included -->
<?php

include __DIR__ . '/../include/footer.inc.php';

?>


