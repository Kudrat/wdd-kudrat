<?php

//config and functions file inluded
include __DIR__ . '/../config/config.php';
include __DIR__ . '/../config/functions.php';




$TITLE = 'User Details';
$body_id="user_details";
$title = "PROFILE";

//If session empty show Flash message from flash.inc
if (empty($_SESSION['logged_in'])) {

    setFlash('error', 'Registered Sucessfully, You must be logged in to view this page');
    //if successful then headed towards location
    header('Location:login.php');
    die;
}

//No user_id set message and die
if (empty($_SESSION['user_id'])) {
    die('User id required');
}

//sanitization
$id = intval($_SESSION['user_id']);


//Create query to select the fields below from user table with user_id
$query = "SELECT first_name,
                last_name,
                street,
                city,
                postal_code,
                province,
                country,
                phone_no,
                email
                FROM users 
                WHERE user_id = :user_id";

//prepare the query
$stmt = $dbh->prepare($query);

//Prepare params array
$params = array(
    ':user_id' => $id
);

//execute the query
$stmt->execute($params);

//get the result
$result = $stmt->fetch(PDO::FETCH_ASSOC);

include __DIR__ . '/../include/header.inc.php';

?><div id="wrapper">

    <?php include __DIR__ . '/../config/flash.inc.php';?>

    <main>
      <!--  dynamic way to get heading -->
       <h1><?=$heading?></h1>

        <div id="detail">
           <!--  php if stmt begins -->
            <?php if ($result) : ?>
               
                <h2 style="color: #000"><?=e_attr($result['first_name'])?> Welcome</h2>

                <ul>
                  <!--   for each result display key and value -->
                    <?php foreach ($result as $key => $value) : ?>
                       
                        <li><strong><?=$key?></strong>: <?=$value?></li>
                    
                    <?php endforeach; ?>
                    </ul>

            <?php else : ?>
                       
                        <h2>Sorry there was a problem adding users</h2>

            <?php endif; ?><!-- php if ends -->

                </div> <!-- details div ends
 -->
            </main> <!-- main ends -->
        </div> <!-- wrapper ends -->





        <!--footer included -->
        <?php

        include __DIR__ . '/../include/footer.inc.php';

        ?>


