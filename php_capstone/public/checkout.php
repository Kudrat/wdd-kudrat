<?php



//config and function files included
require __DIR__ . '/../config/config.php';
require __DIR__ . '/../config/functions.php';
include __DIR__ . '/../classes/Validator.php';


 $v = new Validator();


if(empty($_SESSION['user_id'])){
    header('Location: /login.php');
    die();
}

$TITLE = 'Checkout';
$body_id="checkout";
$title = "checkout";


//variables assign functions and variables returnvalue results in total value
if ($cart) {
    $subtotal = $cart['line_total'];
    $pst = getPst($subtotal);
    $gst = getGst($subtotal);
    $total = $subtotal + $pst + $gst;
} else {
//else die
    die('shopping cart is empty');
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $required = ['card_number'];

     foreach ($required as $key => $value) {
               $v->required($value);

     }//end post func

            $v->cvv('cvv');
            $v->card_num('card_number');

   if (!$v->errors()) {
   try {
    $query = "INSERT INTO
    orders
    (type_srv,service_cost,number_of_hours,GST,PST,sub_total,total,card_number)
    VALUES
    (:type_srv,:service_cost,:number_of_hours,:GST,:PST,:sub_total,:total,:card_number)";

// prepare query
    $stmt = $dbh->prepare($query);
//params
    $params = array(
        ':type_srv' => $cart['service_title'],
        ':service_cost'=>$cart['service_cost'],
        ':number_of_hours'=>$cart['hours'],
        ':GST'=>$gst,
        ':PST'=>$pst,
        ':sub_total'=>$subtotal,
        ':total'=>$total,
        ':card_number'=>substr($_POST['card_number'], -4)


    );
    var_dump($params);
// execute query
    $stmt->execute($params);

//insert values to lastInsert user_id
    $order_id = $dbh->lastInsertId();
    
    $_SESSION['order_id']= $order_id;
    header('Location: invoice.php');

    exit;
} catch (Exception $e) {
             die($e->getMessage());
         }
     } // end if
} // END IF POST


$errors = $v->errors();


include __DIR__ . '/../include/header.inc.php';


?>


<div id="wrapper">
    <main>

        <h1><?=$heading?></h1>

           <?php include __DIR__ . '/../config/errors.php'; ?>
           
        <div class="container">

            <!--   getCart() if add items count items in cart -->


            <?php if (!cart()) : ?>
                <h1>Your cart is empty</h1>
                <h2>Go and buy something</h2>
                <p><a href="services.php">Start Shopping</a></p>
            <?php else : ?>
                    <!-- output cart -->
                    <div id="table_checkout">  
                        <table>
                            <tr style="background-color: #ffcc00">
                                <th>Service</th>
                                <th>Cost Price</th>
                                <th>Hours</th>
                                <th>Line Price</th>
                            </tr>

                            <!--   in cart fetch all these details -->
                            <tr>
                                <td><?=e_attr($cart['service_title'])?></td>
                                <td><?=e_attr($cart['service_cost'])?></td>
                                <td>$<?=e_attr($cart['hours'])?></td>
                                <td>$<?=e_attr($cart['line_total'])?></td>
                            </tr>

                            <tr>
                                <td colspan="3">
                                    SUBTOTAL:
                                </td>
                                <td>
                                    $<?=$subtotal?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    PST:
                                </td>
                                <td>
                                    <!--  display value with 2 decimal spces -->
                                    $<?=number_format($pst, 2)?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    GST:
                                </td>
                                <td>
                                    <!-- display values to two decimal spaces -->
                                    $<?=number_format($gst, 2)?>
                                </td>
                            </tr>
                              <tr>
                                <td colspan="3">
                                    TOTAL:
                                </td>
                                <td  style="background-color: #ffcc00">
                                    $<?=number_format($total, 2)?>
                                </td>
                            </tr>

                        </table>
                    </div>
                    <br/>
                    <br/>

                   
                        <form action="<?=$_SERVER['PHP_SELF'] ?>" 
                            method="post"
                            novalidate>
                            <input type="hidden" name="csrf_token" value="<?=getToken()?>">


                                <label>Accepted Cards</label>
                                <div class="icon-container">
                                  <i class="fa fa-cc-visa" style="color:navy;"></i>
                                  <i class="fa fa-cc-amex" style="color:blue;"></i>
                                  <i class="fa fa-cc-mastercard" style="color:red;"></i>
                                  <i class="fa fa-cc-discover" style="color:orange;"></i>
                                </div>

                            <div class="row">
                                <div class="col-25">
                                    <label for="card_name">Card Name</label>
                                </div>
                                <div class="col-75">
                                    <input type="text" id="card_name" name="card_name" placeholder="Card Name">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-25">
                                    <label for="card_number">Card Number</label>
                                </div>
                                <div class="col-75">
                                    <input type="text" id="card_number" name="card_number" placeholder="Card Number" required="required" value="<?=clean('card_number')?>" /><?=(isset($errors['card_number'])) ? "<span class='error'>{$errors['card_number']}</span>" : '' ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-25">
                                    <label for="expire">Expiry Date</label>
                                </div>
                                <div class="col-75">
                                    <input type="text" id="expire" name="expire" placeholder="Expiration Date">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-25">
                                    <label for="cvv">CVV</label>
                                </div>
                                <div class="col-75">
                                    <input type="text" id="cvv" name="cvv" placeholder="cvv" value="<?=clean('cvv')?>" /><?=(isset($errors['cvv'])) ? "<span class='error'>{$errors['cvv']}</span>" : '' ?>
                                </div>
                            </div>

                            <div class="row">
                               <button class="button" value="payment">Payment</button>
                            </div>
                        </form>
                 

                </div>
            </main>
        </div>

<!-- <a href="payment.php">Checkout Now</a></p>
-->

            <?php endif; ?>

<!--footer included -->
<?php

include __DIR__ . '/../include/footer.inc.php';

?>
