<?php
     /*
            *
  ########*   *########
  #     *       *     #
  #   *           *   #
  # *               * #
  *   Kudrat Sidhu    *
*   Php Register file    *
  *                    *
  # *               * #
  #   *           *   #
  #     *       *     #
  ########*   *########
            *

*/  // variables assign and header included

   $TITLE = 'Registeration';
   $body_id="sign_up";
   $title = "Our Vision";
 
   include  __DIR__ . '/../config/config.php';
   include __DIR__ . '/../config/functions.php';
   require __DIR__ . '/../classes/Validator.php';
    
      $v = new Validator();

      // Set flag that form has not been
      // submitted successfully...
          $success = false;


      // If the request is POST (a form submission) thsi code is set...

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
 // All required fields...
     $required = ['first_name', 'last_name', 'street','city','postal_code','province',
                  'country','phone_no','email','password'];

     // Make sure there is a POST value for each
     // required field
  
     foreach ($required as $key => $value) {
               $v->required($value);
     }//end post func
                   

               $v->email('email');
             //  $v->integer('age');
               // $v->string('first_name');
               // $v->string('last_name');

              // $v->password('password');
              //  $v->postal_code('postal_code');
          
  
     if (!$v->errors()) {
         try {
          // create query
             $query = "INSERT INTO
                      users
                     (first_name,last_name,street,city,postal_code,
                      province,country,phone_no,email,password)
                      VALUES
                     (:first_name,:last_name,:street,:city,:postal_code, :province,:country,:phone_no,:email,:password)";
            
            // prepare query
             $stmt = $dbh->prepare($query);
            //params
             $params = array(
             ':first_name' => e_attr($_POST['first_name']),
             ':last_name' => e_attr($_POST['last_name']),
             ':street' => e_attr($_POST['street']),
             ':city' => e_attr($_POST['city']),
             ':postal_code' => e_attr($_POST['postal_code']),
             ':province' => e_attr($_POST['province']),
             ':country' => e_attr($_POST['country']),
             ':phone_no' => e_attr($_POST['phone_no']),
             ':email' => e_attr($_POST['email']),
             ':password' => password_hash(e_attr($_POST['password']), PASSWORD_DEFAULT)
             );

            // execute query
             $stmt->execute($params);
            
            //insert values to lastInsert user_id
             $user_id = $dbh->lastInsertId();

             $_SESSION['user_id'] = $user_id;

             header('Location: users_detail.php');
            
             exit;
         } catch (Exception $e) {
             die($e->getMessage());
         }
     } // end if
} // END IF POST

$errors = $v->errors();

  include __DIR__ . '/../include/header.inc.php';

?> <div id="wrapper">
        <main>
     
             <h1><?=$heading?></h1>

                    <?php include __DIR__ . '/../config/errors.php'; ?>

                    <?php if (!$success) : ?>
        <form method="post"
              action="<?=$_SERVER['PHP_SELF'] ?>"
              novalidate>
        <!--  convert the value to the unique token -->
        <input type="hidden" name="csrf_token" value="<?=getToken()?>">
       
       
        <p><label for="first_name">First Name</label>
           <input type="text"
                  id="first_name"
                  name="first_name"
                  size="30"
                  placeholder="First Name"
                  required="required"
                  value="<?=clean('first_name')?>" />
                        <?=(isset($errors['first_name'])) ? "<span class='error'>{$errors['first_name']}</span>" : '' ?></p>
                 <!-- makes values stick -->
        
        <p><label for="last_name">Last Name</label>
           <input type="text"
                  id="last_name"
                  name="last_name"
                  size="30" 
                  placeholder="Last Name"
                  value="<?=clean('last_name')?>" />
                        <?=(isset($errors['last_name'])) ? "<span class='error'>{$errors['last_name']}</span>" : '' ?>   </p>
                  <!-- makes values stick -->
        
        <p><label for="street">Street</label>
           <input type="text"
                  id="street"
                  name="street"
                  size="30" 
                  placeholder="Street"
                  value="<?=clean('street')?>" />
                        <?=(isset($errors['street'])) ? "<span class='error'>{$errors['street']}</span>" : '' ?>  
        </p>
                  <!-- makes values stick -->
        
        <p><label for="city">City</label>
           <input type="text"
                  id="city"
                  name="city"
                  size="30" 
                  placeholder="City"
                  value="<?=clean('city')?>" />
                        <?=(isset($errors['city'])) ? "<span class='error'>{$errors['city']}</span>" : '' ?>
                  <!-- makes values stick -->
        </p>

        <p><label for="postal_code">Postal Code</label>
           <input type="text"
                  id="postal_code"
                  name="postal_code"
                  size="30" 
                  placeholder="Postal Code"
                  value="<?=clean('postal_code')?>" />
                        <?=(isset($errors['postal_code'])) ? "<span class='error'>{$errors['postal_code']}</span>" : '' ?>
                  <!-- makes values stick -->
         </p>

        <p><label for="province">Province</label>
           <input type="text"
                  id="province"
                  name="province"
                  size="30" 
                  placeholder="Province"
                  value="<?=clean('province')?>" />
                        <?=(isset($errors['province'])) ? "<span class='error'>{$errors['province']}</span>" : '' ?> 
                  <!-- makes values stick -->
         </p>

        <p><label for="country">Country</label>
           <input type="text"
                  id="country"
                  name="country"
                  size="30" 
                  placeholder="Country"
                  value="<?=clean('country')?>" />
                        <?=(isset($errors['country'])) ? "<span class='error'>{$errors['country']}</span>" : '' ?>
       </p>
                  <!-- makes values stick -->
        

         <p><label for="phone_no">Mobile</label>
           <input  type="tel"
                   id="phone_no"
                   name="phone_no"
                   placeholder="Mobile Number"
                   required="required"
                   value="<?=clean('phone_no')?>" />
                        <?=(isset($errors['phone_no'])) ? "<span class='error'>{$errors['phone_no']}</span>" : '' ?></p>
                   <!-- makes values stick -->
        

               

        <p><label  for="email">Email</label>
           <input  type="email"
                   id="email"
                   name="email"
                   placeholder="Email"
                   required="required"
                   value="<?=clean('email')?>" />
                        <?=(isset($errors['email'])) ? "<span class='error'>{$errors['email']}</span>" : '' ?> 
           </p>
       

        <p><label  for="password">Password</label>
           <input  type="password"
                   id="password"
                   name="password"
                   placeholder="Password"
                   required="required"
                   value="<?=clean('password')?>" />
                        <?=(isset($errors['password'])) ? "<span class='error'>{$errors['password']}</span>" : '' ?> </p>
      

        <p><label  for="confirm_password">Confirm Password</label>
           <input  type="password"
                   id="confirm_password"
                   name="confirm_password"
                   placeholder="Confirm Password"
                   required="required"
                   value="<?=clean('confirm_password')?>" />
                        <?=(isset($errors['confirm_password'])) ? "<span class='error'>{$errors['confirm_password']}</span>" : '' ?>
            
          <p> 
                    
               <button class="button" value="Submit" type="submit" >Submit</button><!--Buttons-->
        
            
     </form> 
   
                    <?php else : ?>
        <h2>Thank you for Registering!</h2>

  
                    <?php endif; ?>

<pre>

        <?php // print_r($_SERVER) ?>

</pre>




    <!-- <div id="box1">
         <h2>The Most Reliable Name in House Maintenance</h2>
         <div id="content1">
          <h3>Experienced</h3>
          <p> Handy has been connecting people to professional
              house cleaning and other home services since 2012. 
              In our short history, house cleaning professionals
              using the Handy platform have helped clean millions
              of homes and apartments in NYC, SF, Boston, Chicago,
              Los Angeles, London, and beyond. Handy is your one-stop
              shop for whenever you need professional house cleaning help. </p>
         </div>
         <div id="content2">
          <h3>Reliable</h3>
          <p> When you schedule a cleaner or maid service through Handy 
              to come to your home, you can rest assured knowing that someone
              will be at your home ready to go at the time you specified.
              They’ll help return your bedrooms, bathrooms, kitchen, living room,
              and more to the like-new condition that you remember from when you
              first moved in!  </p>
         </div>
         <div id="content3">
          <h3>Convenient </h3>
          <p> Late nights at work preventing you from keeping your apartment tidy?
              Too busy taking the kids to school, practices, and playdates to vacuum
              the floors? We know life can be crazy and unpredictable, and when you’re
              tired and overworked, the last thing anybody wants to do is clean their
              home. Whatever the reason you’re looking for home cleaning help, Handy
              has you covered.  </p>
         </div>
         
         <div id="content4">
           <h3>Flexible </h3>
           <p> Handy isn’t just for people looking for a weekly cleaning. Are you moving 
               out of your apartment? Make sure you get your whole security deposit back
               and schedule a cleaning with some added extras. Or maybe you just signed a
               new lease, and want someone to do a deep cleaning before you move in.
               Make Handy your #1 cleaning services provider, whether you’re looking
               for a house cleaning, apartment cleaning, deep cleaning, move-in clean,
               move-out clean, or more! </p>
         </div>
        </div> -->
     </main>
    </div>




    
  <!--footer included -->
<?php
    
   include __DIR__ . '/../include/footer.inc.php';

?>
   
