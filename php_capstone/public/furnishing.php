<?php
/*
*
########*   *########
#     *       *     #
#   *           *   #
# *               * #
*   Kudrat Sidhu    *
*   Php frunishing file   *
*                    *
# *               * #
#   *           *   #
#     *       *     #
########*   *########
*

*/
$TITLE = 'Furnishing';
$body_id="furnishing";
$title = "Our Vision";

include  __DIR__ . '/../config/config.php';
include __DIR__ . '/../config/functions.php';
include __DIR__ . '/../include/header.inc.php';

?>
<div id="wrapper"> 
    <main>
        <div id="main_f_a"> <!-- customised image with width and height-->
            <img src="images/yellow_set.jpg" alt="furnishing image"     style="width: 940px; height: 500px "/>
        </div>

        <div id="main_para1">
            <p> AS WE EVOLVES OUR HOMES SHOULD TOO </p>
        </div>

        <!-------------Form Started------------------->
        <form method="post"
        action="http://www.scott-media.com/test/form_display.php
        autocomplete="on"
        >

        <h1><?=$heading?></h1>

        <p><label for="place">WHERE DO YOU NEED A HELPING HAND?<br/></label>
            <input type="text"
            id="place"
            name="place"
            size="30"
            placeholder="Search for an address......"
            required="required"/>
        </p>


        <p><label  for="date">WHEN SHOULD WE SEND OUR TEAM<br/></label>
            <input  type="date"
            id="date"
            name="date"
            />
        </p>



        <p><label for="phn">GIVE YOUR CONTACT NUMBER<br/></label>
            <input type="tel"
            id="phn"
            name="phn"
            placeholder="Your Contact......"
            required="required"/>
        </p>


        <!--Indented like this to avoid space-->                    
        <p><label  for="time">ARE THERE ANY TIME CONSTRAINTS?</label></p>
        <textarea
        rows="5" cols="35" id="time" name="time">
        Let us know about any time constraints?
    </textarea>

    <!--Indented like this to avoid space-->                    
    <p><label  for="work">WHAT DO YOU NEED TO BE DONE</label></p>
    <textarea
    rows="5" cols="35" id="work" name="work">
    Give us a brief description about the job you need to be done.......
</textarea>



<p> 
    <input value="Submit" type="submit" /><!--Buttons-->
    <input type="reset" value="Reset"/>
</p> 

</form>

<div id="price"><!--This is price chart along with form-->
    <h3>FURNITURE MUST HAVE A PERSONALITY AND AS WELL AS BE BEAUTIFUL</h3>
    <p>
        Helping hands furnishes the instrumentalities (furniture and appliances and 
        other movable accessories including curtains and rugs)
        that make a home (or other area) livable.
        <br/>
        <br/>
        For the first 1.3 hour - $100
        For each additional hour- $75
        Our team have experience in all
        types of furniture and brands. 
        Additional labour (more workers), 
        <br/>
        <br/>
        <strong style="color: #ffbf00" >Questions about our rates?</strong>

    </p>
</div>

<div id="before_after"><!--before and after work images-->
    <h3>OUR WORK BEFORE AND AFTER FURNISHING</h3>

    <div id="b_a1"> </div>
    <div id="b_a2"> </div>
    <div id="b_a3"> </div>
    <div id="b_a4"> </div>
    <div id="b_a5"> </div>
    <div id="b_a6"> </div>
</div>
</main>

</div>

<?php

include __DIR__ . '/../include/footer.inc.php';

?>
