<?php

//config and functions file inluded
include __DIR__ . '/../../config/config.php';
include __DIR__ . '/../../config/functions.php';

if(empty($_SESSION['user_id']) || empty($_SESSION['admin'])){
    setFlash('error' , 'You are not authorise to view this page');
    header('Location: /login.php');
    die();
}




$TITLE = 'Customer Details';
$body_id="users";
$title = "customer_details";

$query = "SELECT * FROM users";

//prepare the query
$stmt = $dbh->prepare($query);

//execute the query
$stmt->execute();

//get the result
$users = $stmt->fetchAll(PDO::FETCH_ASSOC);


include __DIR__ . '/../../include/adminheader.inc.php';

?><div id="wrapper">

    <?php include __DIR__ . '/../../config/flash.inc.php';?>
<main>
    <div class="container">

   

<h1><?=$heading?></h1>

<table>
  
   
    <tr>
        <th>Id:</th>
        <th>First-Name</th>
        <th>Last-Name</th>
        <th>Role</th>
        <th>Street</th>
        <th>City</th>
        <th>Postal-Code</th>
        <th>Province:</th>
        <th>Phone:</th>
        <th>E-mail</th>
    </tr>
    <?php foreach ($users as $value) : ?>
    <tr>
       <td><?=$value['user_id']?></td>
       <td><?=$value['first_name']?></td>
       <td><?=$value['last_name']?></td>
       <td><?=$value['role']?></td>
       <td><?=$value['street']?></td>
       <td><?=$value['city']?></td>
       <td><?=$value['postal_code']?></td>
       <td><?=$value['province']?></td>
       <td><?=$value['phone_no']?></td>
       <td><?=$value['email']?></td>
      
    </tr>
     <?php endforeach; ?>
    
</table>

</div>
           
 </main> <!-- main ends -->
      </div> <!-- wrapper ends -->


       <!--footer included -->
        <?php

        include __DIR__ . '/../../include/footer.inc.php';

        ?>


