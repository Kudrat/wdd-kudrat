<?php
/*
*
########*   *########
#     *       *     #
#   *           *   #
# *               * #
*   Kudrat Sidhu    *
*   Php Service file     *
*                    *
# *               * #
#   *           *   #
#     *       *     #
########*   *########
*

*/
//varibales declared and header included
$TITLE = 'Services';
$body_id="products";
$title = "Our Vision";


//included config and function files
include  __DIR__ . '/../../config/config.php';
include __DIR__ . '/../../config/functions.php';

if(empty($_SESSION['user_id']) || empty($_SESSION['admin'])){
    setFlash('error' , 'You are not authorise to view this page');
    header('Location: /login.php');
    die();
}


//if not empty get the search string and match with the service_title and service_description
if (!empty($_GET['search'])) {
    $query = "SELECT 
              * 
              FROM services 
              WHERE 
              service_title LIKE :search
              OR service_short_description LIKE :search";

//params assign specific word searched to :search
    $params= [
        ':search' => "%{$_GET['search']}%"
    ];

// prepare the query
    $stmt = $dbh->prepare($query);

// execute the query
    $stmt->execute($params);
// get the result
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
} elseif (!empty($_GET['type'])) {
//if not empty and getting type of service then select all information from services.
    $query = "SELECT 
              * 
              FROM services 
              WHERE 
              service_type = :type";


    $params= [
        ':type' => e_attr($_GET['type'])
    ];

// prepare the query
    $stmt = $dbh->prepare($query);

// execute the query
    $stmt->execute($params);
// get the result
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
} else {
// Create query to select an author based on id
    $query = "SELECT * FROM services";

// prepare the query
    $stmt = $dbh->prepare($query);

// execute the query
    $stmt->execute();
// get the result
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
}

include __DIR__ . '/../../include/adminheader.inc.php';

?>
<div id="wrapper">
    <div class="container">
       <?php include __DIR__ . '/../../config/flash.inc.php'; ?>
     <main>

            <div id = srv_img><img src="images/services/tools.jpg" alt="main_image" /></div>
            <!-- dynamic way of representing heading -->


            <div id='listview_nav'>
                <ul>
                  <!--  type of service declared === to type in DB -->
                    <li><a href="/admin/products.php?type=Home-Cleaning">Home Cleaning</a></li>
                    <li><a href="/admin/products.php?type=Construction">Construction</a></li>
                    <li><a href="/admin/products.php?type=Gardening">Gardening</a></li>
                    <li><a href="/admin/products.php?type=Furnishing">Furnishing</a></li>
                    <li><a href="/admin/products.php?type=Electronics">Electronics</a></li>
                    <li><a href="/admin/products.php?type=<?=urlencode('Architecture and Design')?>">Architect</a></li>
                    <li><a href="/admin/products.php?type=<?=urlencode('Architecture and Design')?>">Beauty & Spa</a></li>
              </ul>
            </div> 

            <!-- action of the form performedon page itself -->
            <div id='product_search'>
            <h1><?=$heading?></h1>
            <form action="<?=$_SERVER['PHP_SELF']?>" method= "get" novalidate>
              <!--   convert value to encrypted token -->
                <input type="hidden" name="csrf_token" value="<?=getToken()?>"/>
                <input type="text" name="search" placeholder="Search.."/>
                <button class="button" value="Search">Search</button>
            </form>
            </div>
            <br/>
            

                <?php foreach ($result as $value) : ?>
                    <!-- fectching details from Database in listView -->
                    
                       <table>
                         <tr><th colspan="2"><?=e_attr($value['service_title'])?></th></tr>

                        
                       <tr>
                        <td>

                        <img src="/images/services/<?=$value['image']?>" alt="<?=$value['service_title']?>" />
                      

                        </td>

                        <td>
                     
                        <p><strong>Description :</strong> <?=e_attr($value['service_short_description'])?></p>
                        <ul>
                        <li><strong>Service-Cost :</strong><?=e_attr($value['service_cost'])?></li>
                       
                        </ul>

                            <a href="/admin/create.php">CREATE</a>
                            <a href="/admin/edit.php?id=<?=$value['service_id']?>">UPDATE</a>
                            <a href="/admin/delete.php?id=<?=$value['service_id']?>">DELETE</a>

                        </td>
                      </tr>
                        <!--LINK-->

                    </table>

                <?php endforeach; ?><!-- end for each -->

           </main>

       </div>


</div>

<?php
//footer included
include __DIR__ . '/../../include/footer.inc.php';

?>
