<?php

//config and functions file inluded
include __DIR__ . '/../../config/config.php';
include __DIR__ . '/../../config/functions.php';

if(empty($_SESSION['user_id']) || empty($_SESSION['admin'])){
    setFlash('error' , 'You are not authorise to view this page');
    header('Location: /login.php');
    die();
}//sql injection only admin able to access these pages


$TITLE = 'Admin';
$body_id="admin";
$title = "Admin";

$query = "SELECT 
          count(order_id) as count,
          max(service_cost) as max_cost,
          min(service_cost) as min_cost,
          max(number_of_hours) as max_hrs,
          min(number_of_hours) as min_hrs 
          FROM orders";

//prepare the query
$stmt = $dbh->prepare($query);

//execute the query
$stmt->execute();

//get the result from Database
$result = $stmt->fetch(PDO::FETCH_ASSOC);

//header included
include __DIR__ . '/../../include/adminheader.inc.php';

?><div id="wrapper">
  <!--   flash included -->
    <?php include __DIR__ . '/../../config/flash.inc.php';?>
<main>
    <div class="container">

   
<!-- dynamic way of creating website -->
<h1><?=$heading?></h1>

<table>
    
     <tr>
        <th>Attributes</th>
        <th>Values</th>
    </tr>
   
    <tr>
        <th>Number of Orders</th>
        <td><?=$result['count']?></td>
    </tr>

    <tr>
        <th>Maximum Service Cost</th>
        <td><?=$result['max_cost']?></td>

    </tr>

    <tr>
        <th>Minimun Service Cost</th>
        <td><?=$result['min_cost']?></td>
    </tr>

    <tr>
        <th>Maximum Number of Hours</th>
        <td><?=$result['max_hrs']?></td>
   </tr>

    <tr>
        <th>Minimum Number of Hours</th>
        <td><?=$result['min_hrs']?></td>
    </tr>
        
    
</table>

</div>
           
 </main> <!-- main ends -->
      </div> <!-- wrapper ends -->


       <!--footer included -->
        <?php

        include __DIR__ . '/../../include/footer.inc.php';

        ?>


