<?php

//config and functions file inluded
include __DIR__ . '/../../config/config.php';
include __DIR__ . '/../../config/functions.php';


if(empty($_SESSION['user_id']) || empty($_SESSION['admin'])){
    setFlash('error' , 'You are not authorise to view this page');
    header('Location: /login.php');
    die();
}

$TITLE = 'Delete Service';
$body_id="delete";
$title = "delete_srv";



 if(!empty($_GET['id'])){
    //query
    $query = "SELECT *
                FROM services WHERE service_id = :service_id";
    //prepare the query
    $stmt = $dbh->prepare($query);
    //params set
    $params = [':service_id' => intval($_GET['id'])];
    //stmt execute
    $stmt->execute($params);

    //get the result
    $srv = $stmt->fetch(PDO::FETCH_ASSOC);



 }

 if('POST' == $_SERVER['REQUEST_METHOD']){
 
 $query = 'DELETE FROM services
                    WHERE
                    service_id = :service_id';

                    $params= [
                       ':service_id'=> e_attr($_POST['service_id'])
                    ];

                // prepare the query
                    $stmt = $dbh->prepare($query);

                          
                if($stmt->execute($params)) {
                  setflash('success','Sucessfully Deleted Record');
                  header('Location:/admin/products.php');
                  die;
                } else {
                  $errors['DELETE'] = 'There was a problem in deleting the services!';
                } 
}

include __DIR__ . '/../../include/adminheader.inc.php';

?><div id="wrapper">
     <main>
      
        <h1><?=$heading?></h1>
            <div class="container">
               <?php include __DIR__ . '/../../config/flash.inc.php'; ?>
  
  <form class="form-horizontal" 
        action="<?=$_SERVER['PHP_SELF'] ?>" 
        method="post"
        novalidate>

    <div class="form-group">
      <label class="control-label col-sm-2" for="service_title">Service:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="service_title" placeholder="Service Title" name="service_title" value="<?=e_attr($srv['service_title'])?>"/>
      </div>
       <input type="hidden" name="csrf_token" value="<?=getToken()?>">
           <input type="hidden" name="service_id" value="<?=$srv['service_id']?>">
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="service_type">Service-Type:</label>
      <div class="col-sm-10">          
        <input type="text" class="form-control" id="service_type" placeholder="Type of service" name="service_type" value="<?=e_attr($srv['service_type'])?>"/>
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="service_available">Service-Available:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="service_available" placeholder="Service available" name="service_available" value="<?=e_attr($srv['service_available'])?>"/>
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="service_cost">Service-Cost:</label>
      <div class="col-sm-10">          
        <input type="text" class="form-control" id="service_cost" placeholder="Cost of service" name="service_cost" value="<?=e_attr($srv['service_cost'])?>"/>
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="total_man_required">Total Man Required:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="total_man_required" placeholder="Number of Men required for service" name="total_man_required" value="<?=e_attr($srv['total_man_required'])?>"/>
      </div>
    </div>
    
     <div class="form-group">
    
        <label class="control-label col-sm-2" for="service_long_description" style="text-align: left;">Long-Description</label>
      <div class="col-sm-10">
        <textarea style="width: 100%; height: 100px;" name="service_long_description" id="service_long_description"><?=e_attr($srv['service_long_description'])?></textarea>
    </div>

    </div>



    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="button">DELETE</button>
      </div>
    </div>
  </form>
</div>


    </main> <!-- main ends -->
</div> <!-- wrapper ends -->


       <!--footer included -->
        <?php

        include __DIR__ . '/../../include/footer.inc.php';

        ?>


