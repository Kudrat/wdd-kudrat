<?php

//config and functions file inluded
include __DIR__ . '/../../config/config.php';
include __DIR__ . '/../../config/functions.php';

if(empty($_SESSION['user_id']) || empty($_SESSION['admin'])){
    setFlash('error' , 'You are not authorise to view this page');
    header('Location: /login.php');
    die();
}
//sql injection only admin able to access these pages


$TITLE = 'Create';
$body_id="create";
$title = "create";

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  //query to insert data
 $query = "INSERT INTO
    services
    (service_title ,service_type ,service_available ,service_cost , service_short_description, service_long_description, service_equipment_name, total_man_required, services_time, extra_charges,image)
    VALUES
    (:service_title ,:service_type ,:service_available ,:service_cost , :service_short_description, :service_long_description, :service_equipment_name, :total_man_required, :services_time, :extra_charges,:image)";

// prepare query
    $stmt = $dbh->prepare($query);
//params
    $params = array(
      ':service_title' => e_attr($_POST['service_title']),
      ':service_type' => e_attr($_POST['service_type']),
      ':service_cost' => e_attr($_POST['service_cost']),
      ':service_short_description' => e_attr($_POST['service_short_description']),
      ':service_long_description' => e_attr($_POST['service_long_description']),
      ':service_available' => e_attr($_POST['service_available']),
      ':service_equipment_name' => e_attr($_POST['service_equipment_name']),
      ':total_man_required' => e_attr($_POST['total_man_required']),
      ':services_time' => e_attr($_POST['services_time']),
      ':image' => e_attr($_POST['image']),
      ':extra_charges' => e_attr($_POST['extra_charges'])
   );
// execute query
    $stmt->execute($params);

//insert values to lastInsert service_id
    $service_id = $dbh->lastInsertId();


    setflash('success','Sucessfully Created Record');
    header('Location: products.php');

   // exit;

}

include __DIR__ . '/../../include/adminheader.inc.php';

?><div id="wrapper">
  <main>

        <h1><?=$heading?></h1>
            <div class="container">
  
          
  
  <form class="form-horizontal" 
        action="<?=$_SERVER['PHP_SELF'] ?>" 
        method="post"
        enctype="multipart/form-data"
        novalidate>

    <div class="form-group">
      <label class="control-label col-sm-2" for="service_title" style="text-align: left;">Service:</label>
      <div class="col-sm-10">
        
        <input type="text" class="form-control" id="service_title" placeholder="Service Title" name="service_title" value="<?=clean('service_title')?>" />
      </div>
           <!-- CSRF TOKEN is set -->
           <input type="hidden" name="csrf_token" value="<?=getToken()?>">
           
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="service_type" style="text-align: left;">Service-Type:</label>
      <div class="col-sm-10">          
        <input type="text" class="form-control" id="service_type" placeholder="Type of service" name="service_type" value="<?=clean('service_type')?>" />
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="service_available" style="text-align: left;">Service-Available:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="service_available" placeholder="Service available" name="service_available" value="<?=clean('service_available')?>" />
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="service_cost" style="text-align: left;">Service-Cost:</label>
      <div class="col-sm-10">          
        <input type="text" class="form-control" id="service_cost" placeholder="Cost of service" name="service_cost" value="<?=clean('service_cost')?>" />
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="total_man_required" style="text-align: left;">Total Man Required:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="total_man_required" placeholder="Number of Men required for service" name="total_man_required" value="<?=clean('total_man_required')?>" />
      </div>
    </div>

     <div class="form-group">
      <label class="control-label col-sm-2" for="extra_charges" style="text-align: left;">Extra-Charges:</label>
      <div class="col-sm-10">          
        <input type="text" class="form-control" id="extra_charges" placeholder="Extra-Charges" name="extra_charges" value="<?=clean('extra_charges')?>" />
      </div>
    </div>

     <div class="form-group">
      <label class="control-label col-sm-2" for="service_equipment_name" style="text-align: left;">Equipment:</label>
      <div class="col-sm-10">          
        <input type="text" class="form-control" id="service_equipment_name" placeholder="service_equipment_name" name="service_equipment_name" value="<?=clean('service_equipment_name')?>" />
      </div>
    </div>

      <div class="form-group">
      <label class="control-label col-sm-2" for="services_time" style="text-align: left;">Service-Time</label>
      <div class="col-sm-10">          
        <input type="text" class="form-control" id="services_time" placeholder="services_time" name="services_time" value="<?=clean('services_time')?>" />
      </div>
    </div>
     
   

      <div class="form-group">
    
        <label class="control-label col-sm-2" for="service_short_description" style="text-align: left;">Short-Description</label>
      <div class="col-sm-10">
        <textarea style="width: 100%; height: 60px;" name="service_short_description" id="service_short_description" value="<?=clean('service_short_description')?>"></textarea>
    </div>

    </div>

     <div class="form-group">
    
        <label class="control-label col-sm-2" for="service_long_description" style="text-align: left;">Long-Description</label>
      <div class="col-sm-10">
        <textarea style="width: 100%; height: 100px;" name="service_long_description" id="service_long_description" value="<?=clean('service_long_description')?>"></textarea>
    </div>

    </div>

    <div class="form-group">

     <label  class="control-label col-sm-2"  for="image" style="text-align: left;">Image</label>
       <div class="col-sm-10">
          <input type="file" class="form-control" name="image" id="image" /><br/>
                 
       </div>
     </div>


    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="button">Create</button>
      </div>
    </div>
  </form>

</div>

    </main> <!-- main ends -->
  
</div> <!-- wrapper ends -->


       <!--footer included -->
        <?php

        include __DIR__ . '/../../include/footer.inc.php';

        ?>


