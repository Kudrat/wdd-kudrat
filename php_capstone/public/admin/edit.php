<?php

//config and functions file inluded
include __DIR__ . '/../../config/config.php';
include __DIR__ . '/../../config/functions.php';


if(empty($_SESSION['user_id']) || empty($_SESSION['admin'])){
    setFlash('error' , 'You are not authorise to view this page');
    header('Location: /login.php');
    die();
}

$TITLE = 'EDIT';
$body_id="edit";
$title = "edit";



 if(!empty($_GET['id'])){
    //query
    $query = "SELECT *
                FROM services WHERE service_id = :service_id";
    //prepare
    $stmt = $dbh->prepare($query);
    //params
    $params = [':service_id' => intval($_GET['id'])];

    $stmt->execute($params);

    //get the result
    $srv = $stmt->fetch(PDO::FETCH_ASSOC);



 }

 if('POST' == $_SERVER['REQUEST_METHOD']){
 //update query
 $query = 'UPDATE services 
                    SET 
                    service_title = :service_title,
                    service_type = :service_type,
                    service_cost = :service_cost,
                    service_available = :service_available,
                    service_cost = :service_cost,
                    extra_charges = :extra_charges,
                    service_equipment_name = :service_equipment_name,
                    total_man_required = :total_man_required,
                    service_short_description = :service_short_description,
                    service_long_description = :service_long_description
                    WHERE
                    service_id = :service_id';
                    //set params
                    $params= [
                        ':service_title' => e_attr($_POST['service_title']),
                        ':service_type' => e_attr($_POST['service_type']),
                        ':service_cost' => e_attr($_POST['service_cost']),
                        ':service_available' => e_attr($_POST['service_available']),
                        ':service_cost'=> e_attr($_POST['service_cost']),
                        ':extra_charges'=> e_attr($_POST['extra_charges']),
                        ':service_equipment_name'=>e_attr($_POST['service_equipment_name']),
                        ':total_man_required' => e_attr($_POST['total_man_required']),
                        ':service_short_description'=>e_attr($_POST['service_short_description']),
                        ':service_long_description' =>e_attr($_POST['service_long_description']),
                        ':service_id' =>e_attr($_POST['service_id'])
                    ];

                // prepare the query
                    $stmt = $dbh->prepare($query);

                          
                if($stmt->execute($params)) {
                  //set the flash message
                  setflash('success','Sucessfully Updated the Record');
                  header('Location:/admin/products.php');
                  die;
                } else {
                  $errors['UPDATE'] = 'There was a problem updating that book!';
                } 
}

include __DIR__ . '/../../include/adminheader.inc.php';

?><div id="wrapper">
     <main>
     
        <h1><?=$heading?></h1>
            <div class="container">
  
  <form class="form-horizontal" 
        action="<?=$_SERVER['PHP_SELF'] ?>" 
        method="post"
        novalidate>

    <div class="form-group">
      <label class="control-label col-sm-2" for="service_title" style="text-align: left;">Service:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="service_title" placeholder="Service Title" name="service_title" value="<?=e_attr($srv['service_title'])?>"/>
      </div>
           <input type="hidden" name="csrf_token" value="<?=getToken()?>">
           <input type="hidden" name="service_id" value="<?=$srv['service_id']?>">
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="service_type" style="text-align: left;">Service-Type:</label>
      <div class="col-sm-10">          
        <input type="text" class="form-control" id="service_type" placeholder="Type of service" name="service_type" value="<?=e_attr($srv['service_type'])?>"/>
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="service_available" style="text-align: left;">Service-Available:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="service_available" placeholder="Service available" name="service_available" value="<?=e_attr($srv['service_available'])?>"/>
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="service_cost" style="text-align: left;">Service-Cost:</label>
      <div class="col-sm-10">          
        <input type="text" class="form-control" id="service_cost" placeholder="Cost of service" name="service_cost" value="<?=e_attr($srv['service_cost'])?>"/>
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="total_man_required" style="text-align: left;">Total Man Required:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="total_man_required" placeholder="Number of Men required for service" name="total_man_required" value="<?=e_attr($srv['total_man_required'])?>"/>
      </div>
    </div>

     <div class="form-group">
      <label class="control-label col-sm-2" for="extra_charges" style="text-align: left;">Extra-Charges:</label>
      <div class="col-sm-10">          
        <input type="text" class="form-control" id="extra_charges" placeholder="Extra-Charges" name="extra_charges" value="<?=e_attr($srv['extra_charges'])?>"/>
      </div>
    </div>

     <div class="form-group">
      <label class="control-label col-sm-2" for="service_equipment_name" style="text-align: left;">Equipment:</label>
      <div class="col-sm-10">          
        <input type="text" class="form-control" id="service_equipment_name" placeholder="service_equipment_name" name="service_equipment_name" value="<?=e_attr($srv['service_equipment_name'])?>"/>
      </div>
    </div>
     
     <div class="form-group">

     <label  class="control-label col-sm-2"  for="image" style="text-align: left;">Image</label>
       <div class="col-sm-10">
          <input type="file" class="form-control" name="image" id="image" /><br/>
          <img style="width: 200px; height: 180px;"
               src="/images/services/<?=$srv['image']?>" alt="<?=$srv['service_title']?>" />
       </div>
     </div>

      <div class="form-group">
    
        <label class="control-label col-sm-2" for="service_short_description" style="text-align: left;">Short-Description</label>
      <div class="col-sm-10">
        <textarea style="width: 100%; height: 60px;" name="service_short_description" id="service_short_description"><?=e_attr($srv['service_short_description'])?></textarea>
    </div>

    </div>

     <div class="form-group">
    
        <label class="control-label col-sm-2" for="service_long_description" style="text-align: left;">Long-Description</label>
      <div class="col-sm-10">
        <textarea style="width: 100%; height: 100px;" name="service_long_description" id="service_long_description"><?=e_attr($srv['service_long_description'])?></textarea>
    </div>

    </div>


    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="button">Update</button>
      </div>
    </div>
  </form>
</div>


    </main> <!-- main ends -->
</div> <!-- wrapper ends -->


       <!--footer included -->
        <?php

        include __DIR__ . '/../../include/footer.inc.php';

        ?>


