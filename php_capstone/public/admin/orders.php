<?php

//config and functions file inluded
include __DIR__ . '/../../config/config.php';
include __DIR__ . '/../../config/functions.php';

if(empty($_SESSION['user_id']) || empty($_SESSION['admin'])){
    setFlash('error' , 'You are not authorise to view this page');
    header('Location: /login.php');
    die();
}


$TITLE = 'Orders';
$body_id="orders";
$title = "Orders";

$query = "SELECT * FROM orders";

//prepare the query
$stmt = $dbh->prepare($query);

//execute the query
$stmt->execute();

//get the result
$orders = $stmt->fetchAll(PDO::FETCH_ASSOC);


include __DIR__ . '/../../include/adminheader.inc.php';

?><div id="wrapper">

    <?php include __DIR__ . '/../../config/flash.inc.php';?>
<main>
    <div class="container">

   

<h1><?=$heading?></h1>

<table>
  
   
    <tr>
        <th>Order Id</th>
        <th>Created at</th>
        <th>Hours per Service</th>
        <th>type_srv</th>
        <th>Sub-total</th>
        <th>Total</th>
    </tr>
    <?php foreach ($orders as $value) : ?>
    <tr>
       <td><?=$value['order_id']?></td>
       <td><?=$value['created_at']?></td>
       <td><?=$value['number_of_hours']?></td>
       <td><?=$value['number_of_hours']?></td>
       <td><?=$value['sub_total']?></td>
       <td><?=$value['total']?></td>
    </tr>
     <?php endforeach; ?>
    
</table>

</div>
           
 </main> <!-- main ends -->
      </div> <!-- wrapper ends -->


       <!--footer included -->
        <?php

        include __DIR__ . '/../../include/footer.inc.php';

        ?>


