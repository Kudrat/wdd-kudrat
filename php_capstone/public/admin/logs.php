<?php


include __DIR__ . '/../../config/config.php';
include __DIR__ . '/../../config/functions.php';

if(empty($_SESSION['user_id']) || empty($_SESSION['admin'])){
    setFlash('error' , 'You are not authorise to view this page');
    header('Location: /login.php');
    die();
}

$TITLE = 'Log View';
$body_id='logs';
$title = 'LOG VIEW';



// Create query to select an author based on id
$query = "SELECT * FROM log  ORDER BY id Desc LIMIT 10 ";

// prepare the query
$stmt = $dbh->prepare($query);

// execute the query
$stmt->execute();
// get the result
$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

include __DIR__ . '/../../include/adminheader.inc.php';


?> <div id="wrapper">

  
 <div class="container">
    <main>
      
        <!-- dynamic way to get heading -->
        <h1><?=$heading?></h1>

        <div id="detail">
            <?php if ($result) : ?>

                <table>
                     <tr>
                        <b><th>Id</th></b>
                        <b><th>Events</th></b>

                     </tr>
                    <!-- foreach result get values of id an event -->
                    <?php foreach ($result as $value) : ?>
                      <tr>
                        <td> <?=$value['id']?> </td>
                        <td> <?=$value['event']?> </td>
                     </tr>
                    <?php endforeach; ?>

                </table>



            <?php else : ?>
                    <h2>Sorry there is error in uploading details</h2>

            <?php endif; ?>

            </div>
        

        </main>

    </div>
    </div>





    <!--footer included -->
    <?php

    include __DIR__ . '/../../include/footer.inc.php';

    ?>


