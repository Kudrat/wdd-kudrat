 
 --user database created..
   CREATE DATABASE helpinghandsusersform;

 
   
--Users table created with basic user informtion
  CREATE TABLE users (
	      user_id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	      role VARCHAR(255) NOT NULL DEFAULT 'normal',
	      first_name VARCHAR(255),
	      last_name VARCHAR(255),
	      street VARCHAR(255),
	      city Text,
	      postal_code VARCHAR(10),
          province Text,
          country Text,
          phone_no VARCHAR(32),
	      email VARCHAR(32),
	      password VARCHAR(255),
	      confirm_password VARCHAR(255),
	      created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	      updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP  
	  	);

   INSERT INTO users VALUES 
		  (1,'normal','Kudrat','Sidhu','686 Sheppard Street','Winnipeg','R2PJ09','MB','Canada','204-8691742','kudratsidhu08@gmail.com','kudrat95','kudrat95','2019-05-10 15:59:47','2019-05-10 15:59:47');
	

  /*After that updated roles*/

  update users SET role='admin' WHERE user_id = 1;	 

  /*Log Table created*/
   CREATE table log (id int (11) NOT NULL PRIMARY KEY AUTO_INCREMENT, event Text); 