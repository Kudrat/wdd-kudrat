<?php


//varibales declared and header included
$TITLE = 'Services-View';
$body_id="service_view";
$title = "service_view";

//config and function files included
require __DIR__ . '/../config/config.php';
require __DIR__ . '/../config/functions.php';


if(empty($_SESSION['user_id'])){
    setFlash('error' , 'Login to View the cart');
    header('Location: /login.php');
    die();
}




//variables assign functions and variables returnvalue results in total value
if ($cart) {
    $subtotal = $cart['line_total'];
    $pst = getPst($subtotal);
    $gst = getGst($subtotal);
    $total = $subtotal + $pst + $gst;
} 

  



  if(isset($_GET['empty'])){

    $_SESSION['cart'] =[];
  }




//included header
include __DIR__ . '/../include/header.inc.php';


?>

  
  <div id="wrapper">

     <?php include __DIR__ . '/../config/flash.inc.php'; ?>
    <main>
      
       <div class="container">
            <h1><?=$heading?></h1>
   <!--   getCart() if add items count items in cart -->
       

        <?php if (!cart()) : ?>
            <h2 style="color: #000;">Go and buy something</h2>
            <p><a href="services.php">Start Shopping</a></p>
        <?php else : ?>
                

                <!-- output cart -->
                <table>
                    <tr>
                        <th>Service</th>
                        <th>Cost Price</th>
                        <th>Hours</th>
                        <th>Line Price</th>
                    </tr>
                   
                      <!--   in cart fetch all these details -->
                        <tr>
                            <td><?=e_attr($cart['service_title'])?></td>
                            <td>$ <?=e_attr($cart['service_cost'])?></td>
                            <td><?=e_attr($cart['hours'])?> hours</td>
                            <td>$<?=e_attr($cart['line_total'])?></td>
                        </tr>
                    
                    <tr>
                        <td colspan="3">
                            SUBTOTAL:
                        </td>
                        <td>
                            $<?=$subtotal?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            PST:
                        </td>
                        <td>
                           <!--  display value with 2 decimal spces -->
                            $<?=number_format($pst, 2)?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            GST:
                        </td>
                        <td>
                            <!-- display values to two decimal spaces -->
                            $<?=number_format($gst, 2)?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            TOTAL:
                        </td>
                        <td>
                            $<?=number_format($total, 2)?>
                        </td>
                    </tr>

                </table>

                <p><a href="services.php">Continue Shopping</a> | 
                   <a href="service_view.php?empty">Empty Cart</a> |
                   <a href="checkout.php">Checkout</a> 
                </p>

            <?php endif; ?>
           
               <h3>Our other popular services add to cart</h3>
               <table>
                <tr>
                 <td>
                  <a target="_blank" href="services.php">
                    <img src="images/services/spa.jpg" alt="Beauty and Spa" width="200" height="200">
                  </a>
                 </td>
                 <td>
                     Add a description of the image here
                 </td>

                <tr>
                  <td>
                  <a target="_blank" href="service.php">
                    <img src="images/table.jpg" alt="Furnishing" width="200" height="200">
                  </a>
                 </td>
                 <td>
                     Add a description of the image here
                 </td>
                </tr>

                <tr>
                    <td>
                  <a target="_blank" href="service.php">
                    <img src="images/services/electrical1.jpg" alt="Electronics" width="200" height="200">
                  </a>
              </td>
                  <td>Add a description of the image here</td>
                </tr>

                <tr>
                  <td>
                  <a target="_blank" href="service.php">
                    <img src="images/services/plumber1.jpg" alt="Home-Cleaning" width="200" height="200">
                  </a>
                 </td>
                 <td>
                    Add a description of the image here
                </td>
            </tr>
               
               </table>

      

</div><!-- /container -->
</main>
</div>

           <?php
//footer included
            include __DIR__ . '/../include/footer.inc.php';

            ?>
