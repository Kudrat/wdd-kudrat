<?php
/*
*
########*   *########
#     *       *     #
#   *           *   #
# *               * #
*   Kudrat Sidhu    *
*   Php Service file     *
*                    *
# *               * #
#   *           *   #
#     *       *     #
########*   *########
*

*/
//varibales declared and header included
$TITLE = 'Services-Details';
$body_id="service_details";
$title = "service_details";

//include config and function
include  __DIR__ . '/../config/config.php';
include __DIR__ . '/../config/functions.php';


//get id and query selects * from services based on id
$service_id = $_GET['id'];


// Create query to select an author based on id
$query = "SELECT * FROM services where service_id = :service_id ";

//params
$paramas = [ ':service_id'=> $service_id ];

//prepare the query
$stmt = $dbh->prepare($query);

// execute the query
$stmt->execute($paramas);
// get the result
$result = $stmt->fetch(PDO::FETCH_ASSOC);

//included header
include __DIR__ . '/../include/header.inc.php';



?>

<div id="wrapper">
    <main>
        <div class="container">
                <h1><?=$heading?></h1>
              
                    <!-- dynamic way of representing heading -->
                    <!-- <h1><?=$heading?></h1> -->
                    <div class="container">

                        <h3><?=e_attr($result['service_title'])?></h3>
                        <!-- fetch all the details from DB in this format -->
                        <img src="/images/services/<?=$result['image']?>" alt="<?=$result['service_title']?>" />
                        <ul>
                            <li><strong>Service-Type:</strong> <?=
                             e_attr($result['service_type'])?></li>
                            <li><strong>Service-Cost:</strong><?=
                             e_attr($result['service_cost'])?></li>
                            <li><strong>Total Man:</strong><?=
                             e_attr($result['total_man_required'])?></li>
                            <li><strong>Service Available</strong><?=
                            e_attr($result['service_available'])?></li>
                            <li><strong>Service Equipment Use</strong><?=
                            e_attr($result['service_equipment_name'])?></li>
                            <li><strong>Minimun Hours</strong><?=
                            e_attr($result['services_time'])?></li>
                             <li><strong>Extra charges</strong><?=
                            e_attr($result['extra_charges'])?></li>

                        </ul>
                        <p><?=e_attr($result['service_short_description'])?></p>
                        <p><?=e_attr($result['service_long_description'])?></p>
                        <p>
                            <!-- form action to addto -->
                            <form action="addtocart.php" method="post">
                                <!-- get token convert value to csrf format -->
                                <input type="hidden" name="csrf_token" value="<?=getToken()?>">
                                <h3>Number of hours</h3>
                                <select name="qty">
                                    <option value="1">1 hour</option>
                                    <option value="2">2 hours</option>
                                    <option value="3">3 hours</option>
                                    <option value="4">4 hours</option>
                                    <option value="5">5 hours</option>
                                    <option value="6">6 hour</option>
                                    <option value="7">7 hours</option>
                                    <option value="8">8 hours</option>
                                    <option value="9">9 hours</option>
                                    <option value="10">10 hours</option>
                                    <option value="11">11 hour</option>
                                    <option value="12">12 hours</option>
                                    <option value="13">13 hours</option>
                                    <option value="14">14 hours</option>
                                    <option value="15">15 hours</option>
                                    <option value="16">16 hour</option>
                                    <option value="17">17 hours</option>
                                    <option value="18">18 hours</option>
                                    <option value="19">19 hours</option>
                                    <option value="20">20 hours</option>
                                </select>
                                <input type="hidden" name="service_id" value="<?=$result['service_id']?>" />
                                 <button class="button" value="addtocart">Add to Cart</button>
                            </form>
                        </div>
                    
                     </div>
                  

                </main>

            </div>

            <?php
//footer included
            include __DIR__ . '/../include/footer.inc.php';

            ?>
