<?php

/**
 * Book Model
 * All database functionality for the Book table
 */

class Book
{
	
	public static function all()
	{
		global $dbh;
		$query ='SELECT book.*,
				author.name as author,
				publisher.name as publisher,
				format.name as format,
				genre.name as genre
				FROM
				book
				JOIN author USING (author_id)
				JOIN publisher USING (publisher_id)
				JOIN format USING (format_id)
				JOIN genre USING (genre_id)
				ORDER BY book.title ASC';
		$stmt = $dbh->query($query);
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}

}
// END FILE



















// Ignore this... it simply shows the source
// of this file for teaching purposes
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
    show_source(__FILE__);
}

