<?php

define('GST', .05);
define('PST', .07);

function getCart() {
	if(empty($_SESSION['cart'])) {
		return 'Your cart is empty';
	} else {
		$count = count($_SESSION['cart']);
		return "$count items in cart"; 
	}
}

/**
 * Single Item Shopping Cart
 * @param Array $item
 */
function addToCart($item)
{
	$item_id = $item['book_id'];
	// Single item cart
	$_SESSION['cart'][$item_id] = $item;
}

function cart(){
	return count($_SESSION['cart']);
};

function getItems()
{
	global $dbh;
	$items = [];
	foreach($_SESSION['cart'] as $key => $item) {
        $query = 'SELECT book_id, title, price
                    FROM book
                  WHERE book_id = :book_id
                ';
        $params = array(
            ':book_id' => $key
        );
        $stmt = $dbh->prepare($query);
        $stmt->execute($params);
        $book = $stmt->fetch(PDO::FETCH_ASSOC);
        $items[$book['book_id']]['title'] = $book['title'];
        $items[$book['book_id']]['qty'] = $item['qty'];
        $items[$book['book_id']]['unit_price'] = $book['price'];
        $items[$book['book_id']]['line_price'] = $item['qty'] * $book['price'];
    }

    return $items;
}
// END FILE
// 

function getCartSubTotal($items)
{
	$sub = 0;
	foreach($items as $item) {
		$sub += $item['line_price'];
	}
	return $sub;
}

function getPst($sub)
{
	return $sub * PST; 
}

function getGST($sub)
{
	return $sub * GST;
}



















// Ignore this... it simply shows the source
// of this file for teaching purposes
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
    show_source(__FILE__);
}

