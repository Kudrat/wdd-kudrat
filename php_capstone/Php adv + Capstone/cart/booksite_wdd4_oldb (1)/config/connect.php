<?php

// Change all these constants to match your requirements
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_HOST', '127.0.0.1');
define('DB_NAME', 'booksite');
// End constants


define('DB_DSN', 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME);
// define('DB_DSN', 'mysql:host=localhost;dbname=books');

$dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// END FILE



















// Ignore this... it simply shows the source
// of this file for teaching purposes
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
    show_source(__FILE__);
}

