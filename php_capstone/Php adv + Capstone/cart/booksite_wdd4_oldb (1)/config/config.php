<?php

$environment = 'dev'; // prod or dev

if('dev' == $environment){
	ini_set('display_errors', 1);
	ini_set('error_reporting', E_ALL);
} else {
	ini_set('display_errors', 0);
}

// start output buffering... wait until the entire script
// is finished before beginning output
ob_start();

// Start the PHP Session
// make sure to include this file
session_start();

// If we don't have a cart in the session
// create an empty
if(!isset($_SESSION['cart'])) {
	$_SESSION['cart'] = [];
}

// END FILE



















// Ignore this... it simply shows the source
// of this file for teaching purposes
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
    show_source(__FILE__);
}

