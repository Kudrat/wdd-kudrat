<?php

require __DIR__ . '/../config/config.php';
require __DIR__ . '/../config/connect.php';
require __DIR__ . '/../classes/Book.php';
require __DIR__ . '/../lib/functions.php';

if(isset($_GET['empty'])) {
    $_SESSION['cart'] = [];
}

if(cart()) {

    $items = getItems();
    $subtotal = getCartSubTotal($items);
    $pst = getPst($subtotal);
    $gst = getGst($subtotal);
    $total = $subtotal + $pst + $gst;

}

?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="" />
    <title>View Cart</title>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th {
            text-align: left;
            padding: 6px;
        }
        td {
            border: 1px solid #cfcfcf;
            padding: 6px;
        }

        #cart {
            display: inline-block;
            position: absolute;
            top:0px;
            right: 0px;
            color: #FFF;
            background: #000;
            font-size: .9;
            padding: 10px;
        }
        .container {
            width: 900px;
            margin: 0 auto;
            position: relative;
        }

        #cart a:link, #cart a:visited {
            color: #fff;
            font-size: .9;
        }
        #cart a:hover, #cart a:active {
            color: #fff;
            font-size: .9;
        }
    </style>
</head>
<body>

<div class="container">

    <div id="cart">
        <?=getCart()?>
        <div class="view">
            <a href="view_cart.php">view cart</a>
        </div>
    </div>

<?php if(!cart()) : ?>
    <h1>Your cart is empty</h1>
    <h2>Go and buy something</h2>
    <p><a href="index.php">Start Shopping</a></p>
<?php else: ?>

<h1>Your shopping cart</h1>

<!-- output cart -->
<table>
    <tr>
        <th>Item</th>
        <th>Quantity</th>
        <th>Unit Price</th>
        <th>Line Price</th>
    </tr>
    <?php foreach($items as $item) : ?>
    <tr>
        <td><?=$item['title']?></td>
        <td><?=$item['qty']?></td>
        <td>$<?=$item['unit_price']?></td>
        <td>$<?=$item['line_price']?></td>
    </tr>
    <?php endforeach ?>
    <tr>
        <td colspan="3">
            SUBTOTAL:
        </td>
        <td>
            $<?=$subtotal?>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            PST:
        </td>
        <td>
           $<?=number_format($pst, 2)?>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            GST:
        </td>
        <td>
            $<?=number_format($gst, 2)?>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            TOTAL:
        </td>
        <td>
            $<?=number_format($total, 2)?>
        </td>
    </tr>

</table>

<p><a href="index.php">Continue Shopping</a> | 
<a href="view_cart.php?empty">Empty Cart</a> | 
<a href="payment.php">Checkout Now</a></p>


<?php endif; ?>

</div><!-- /container -->

</body>
</html>