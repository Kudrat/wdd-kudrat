<?php

require __DIR__ . '/../config/config.php';
require __DIR__ . '/../config/connect.php';
require __DIR__ . '/../classes/Book.php';
require __DIR__ . '/../lib/functions.php';

try {
$books = Book::all();
} catch (Exception $e) {
	echo $e->getMessage();
}

?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="" />
	<title>Book Store</title>
	<style>
		table {
			border-collapse: collapse;
			width: 100%;
		}

		th {
			text-align: left;
			padding: 6px;
		}
		td {
			border: 1px solid #cfcfcf;
			padding: 6px;
		}

		#cart {
			display: inline-block;
			position: absolute;
			top:0px;
			right: 0px;
			color: #FFF;
			background: #000;
			font-size: .9;
			padding: 10px;
		}
		.container {
			width: 900px;
			margin: 0 auto;
			position: relative;
		}

		#cart a:link, #cart a:visited {
			color: #fff;
			font-size: .9;
		}
		#cart a:hover, #cart a:active {
			color: #fff;
			font-size: .9;
		}
	</style>
</head>
<body>

	<div class="container">

	<div id="cart">
		<?=getCart()?>
		<div class="view">
			<a href="view_cart.php">view cart</a>
		</div>
	</div>

<h1>Book Store</h1>

<table>
	<tr>
		<th>Title</th>
		<th>Author</th>
		<th>Num Pages</th>
		<th>Format</th>
		<th>Price</th>
		<th></th>
	</tr>
	<?php foreach($books as $row) : ?>
	<tr>
		<td><?=$row['title']?></td>
		<td><?=$row['author']?></td>
		<td><?=$row['num_pages']?></td>
		<td><?=$row['format']?></td>
		<td><?=$row['price']?></td>
		<td>
				<form action="cart.php" method="post">
					<select name="qty">
						<option>1</option>
						<option>2</option>
						<option>3</option>
						<option>4</option>
						<option>5</option>
					</select>
					<input type="hidden" name="book_id" value="<?=$row['book_id']?>" />
					<button>add to cart</button>
				</form>
		</td>
	</tr>
	<?php endforeach ?>
</table>

</div><!-- /container -->


</body>
</html>
