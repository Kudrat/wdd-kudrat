<?php 

  //buffer overflow
  ob_start();

  
  session_start();

  //set session

  if(empty($_SESSION['csrf_token'])){

  	$_SESSION['csrf_token'] = md5( rand() );

  }

  if('POST' == $_SERVER['REQUEST_METHOD']){
  	 if($_POST['csrf_token'] != $_SESSION['csrf_token']){

  	 	die('CSRF TOKEN MISMATCH');
  	 }
  }