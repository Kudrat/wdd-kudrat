<?php

// Classes should be created one per file
// The file name for classes should be identical to the class name
// eg: this class should be saved in a file called Validator.php

// Validate any type of data

// Track our errors

// Give us the errors when we ask for them

class Validator
{

	private $errors = [];

	/**
	 * Ensure a value is provided for required field
	 * @param  String $field the field name
	 * @return void
	 */
	public function required($field) {
		if(!filter_input(INPUT_POST, $field)){
			 $this->setError($field, $field . ' is a required.');
		}
	}

	//filter now consider best pactoce for accessing superglobal variable for example  to acess a field name 'email' in $_POST , we would do this

	//filter_input(INPUT_POST, $field, FILTER_VALIDATE_EMAIL);


	public function email($field){

		//if email not valid set error
		     //set error


	   if(!filter_input(INPUT_POST,$field, FILTER_VALIDATE_EMAIL)){
	   	//if use again we don't want to override the previous msg
	   	//   if(empty($this->errors[$field])){
	   	//    $this->error[$field] = 'PLease enter the valid email';
	   	// }

	   	  $this->setError($field, 'Please provide a valid email address');

	   }

	}

	

	/**
	 * Return the errors array
	 * @return Array
	 */
	
	public function errors()
	
	 {
		 return $this->errors;
	}

     private function setError($field, $message){
        if(empty($this-> errors[$field])){
        	$this-> errors[$field] = $message ;
        }
     }
}