<?php
 /*            
            *  
  ########*   *########
  #     *       *     #       
  #   *           *   #       
  # *               * #            
  *   Kudrat Sidhu    *                   
*     Php Include file   *                     
  *                    *                  
  # *               * #              
  #   *           *   #       
  #     *       *     #
  ########*   *########
            *

*/



?>
<!doctype html>

<html lang="en">
  <head> 
      <title><?=$TITLE?></title> <!--dynamic way of creating title -->
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width">
      
      <link rel="icon" href="images/logo_pj1_icon.png" type="image/gif">
     
      
      <link rel="stylesheet" 
          href="style/mobile_style_sheet1.css"
          type="text/css"
          media="screen and (max-width: 750px)"
        />
    
      <link rel="stylesheet" 
            href="style/desktop_style_sheet1.css"
            type="text/css"
            media="screen and (min-width:751px)"
       />
    


     <!-- Conditional comments for IE browsers -->

     <!-- [if LTE IE 8]>
        <link rel="stylesheet" href="style/old_ie1.css" type="text/css" />
      <![endif] -->

     
      <?php
            $active = "navstyle";  //Breadcrumbs(highlighted activ class created)
          
          //embedded css file included on basis of title if titl == our vision then this  embedded css works
           if($title == "Our Vision") { 
            include __DIR__ . '/../public/style/embeddedabout.css';
          }
          // each page assign body id if body_id == id on each page then heading will be displayed dynamically
          if($body_id == 'provides'){
              $heading = "SERVICES";
          }elseif ($body_id == 'furnishing') {
              $heading = "Furnishing";
          }elseif ($body_id == 'sign_up') {
              $heading = "Registeration";
          }elseif($body_id == 'information'){
              $heading = "Our Vision";
          }elseif($body_id == 'user_details'){
              $heading = "User Profile";
          }else($body_id == 'userlogin'){
              $heading = "Login"
          }
              
          
      ?>
      
      <script src="style/old_ie1.js"></script>
      <style>
          #sign_up .errors, .error {
             color: #990000;
             font-size: 16px;
          }

      </style>
    </head>
    
  
<body>
   <div id="<?=$body_id?>"><!--echo id = "body_id" as per different pages-->
    <header>
      <div id="inner_header">
        <div id="logo"><img src="images/logo_pj1.png" alt="Logo" /></div>
        <div id="logo_mobile"><img src="images/logo_pj1_mobile.png" alt="Logo" /></div>
        <div id="name">Helping Hands<br/><p> Home Maintenance</p></div>
        
      </div>
    </header>
      
    <nav>
           <!--Hamburger Menu-->
          <a href="#" id="menubutton">
          <span id="topbar"> </span>
          <span id="middlebar"></span>
          <span id="bottombar"></span>

      </a>
       
        <ul id="navlist">
          <!-- if body_id == specific id on each page then it outputs active class with navstyle highlighting the particular navlist block (showing breadcrumbs)-->
          <li><a href="index.php"
               class="<?php 
                   if($body_id == 'homepage'){
                      echo $active;   
                   }?>" >Home</a></li>
          
          <li class="menu2">
                  <a href="services.php"
                    class="<?php 
                   if($body_id == 'provides'){
                      echo $active;     
                   }?>" >Services</a>
           
            <ul class="submenu2">
              <li><a href="services.php">Home Cleaning</a></li>
              <li><a href="furnishing.php">Furniture Assembly</a></li>
              <li><a href="services.php">Interior Designing</a></li>
              <li><a href="services.php">Moving Help</a></li>
              <li><a href="services.php">Carpenting</a></li>
              <li><a href="services.php">Plumbing</a></li>
              <li><a href="services.php">Electrical</a></li>
              <li><a href="services.php">Maid Services</a></li>
              <li><a href="services.php">House Keeping</a></li>
            </ul>
          </li>

            <li class="menu3"><a href="furnishing.php"
                class="<?php 
                   if($body_id == 'furnishing'){
                      echo $active;     
                   }?>" >Recent-Launches</a>
              <ul class="submenu3">
                <li><a href="index.php">Lawn Maintenance</a></li>
                <li><a href="furnishing.php">Furnishing</a></li>
                <li><a href="index.php">Beauty Services</a></li>
               </ul>
             </li>
            <li><a href="about_us.php"
                  class="<?php 
                   if($body_id == 'information'){
                      echo $active;     
                   }?>" >About us</a></li>
        
         <?php if(empty($_SESSION['logged_in'])) :?>

               <li><a href="login.php"
                       class="<?php 
                         if($body_id == 'userlogin'){
                            echo $active;     
                         }?>">Login</a></li>


          <li><a href="register.php"
                 class="<?php 
                   if($body_id == 'sign_up'){
                      echo $active;     
                   }?>">Register</a></li>
         
         <?php else :?>
              
               <li><a href="users_detail.php"
                       class="<?php 
                         if($body_id == 'user_details'){
                            echo $active;     
                         }?>">Profile</a></li>

              <li><a href="login.php?logout=1"
                       class="<?php 
                         if($body_id == 'userlogin'){
                            echo $active;     
                         }?>">Logout</a></li>
        <?php endif;?>



        </ul>
      </nav>