 
 --user database created..
   CREATE DATABASE helpinghandsusersform;

 
   
--Users table created with basic user informtion
  CREATE TABLE users (
	      user_id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	      first_name VARCHAR(255),
	      last_name VARCHAR(255),
	      street VARCHAR(255),
	      city Text,
	      postal_code VARCHAR(10),
          province Text,
          country Text,
          phone_no VARCHAR(32),
	      email VARCHAR(32),
	      password VARCHAR(255),
	      confirm_password VARCHAR(255),
	      created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	      updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP  
	  	);

   INSERT INTO users VALUES 
		  (1,'Harnoor','Sidhu','686 Sheppard Street','Winnipeg','R2PJ09','MB','Canada','204-8691742','harnoor08@gmail.com','harnoor95$','204-890-0987','2019-04-09 15:59:47','2019-04-09 15:59:47');
		  