<?php 
     /*            
            *  
  ########*   *########
  #     *       *     #       
  #   *           *   #       
  # *               * #            
  *   Kudrat Sidhu    *                   
*      Php Home file     *                     
  *                    *                  
  # *               * #              
  #   *           *   #       
  #     *       *     #
  ########*   *########
            *

*/
   // variables assign and header included 
   $TITLE = 'Home';
   $body_id="homepage";
   $title = "Our Vision";
  
   include  __DIR__ . '/../config/config.php';
   include __DIR__ . '/../include/header.inc.php';

?>      
      
    <div id="wrapper">  
      <!-- Main started-->
      <main>
        <div id="main_img">
          <img src="images/Uplash_img1.jpg" alt="main_image"/>
        </div>
        <div id= "register"> <!-- Registeration Button is created -->
          <a href="register.php"><p>Quick Service | Register</p></a>
        </div>
        <div id="para1">
          <p> Don't Risk Fall<br/>
              Give us a Call</p>
        </div>
        
        <div id="srv_style">
         <div id="tool"> SERV<img src="images/Tool.png" alt="tools"/>CES</div>
         <div id="tool_mobile"> SERV<img src="images/Tool_mobile.png" alt="tools"/>CES</div>
        </div>
        <!-- Images for desktop-->
        <div id="srv_tools_desktop"> <!-- SVG images clickable representing different services -->
         <div id="img1"><a href="services.php"><img src="images/hammer.png" title="Construction" alt="service"/></a></div>
         <div id="img2"><a href="services.php"> <img src="images/Plug.png" title="Electrical Maintenance" alt="service"/></a></div>
         <div id="img3"><a href="services.php"> <img src="images/screwdriver.png" title="Maintenance" alt="service"/></a></div>
         <div id="img4"><a href="services.php"> <img src="images/laptop.png" title="Gudgets Maintenance" alt="service"/></a></div>
         <div id="img5"><a href="services.php"> <img src="images/beauty.png" title="Beauty Support" alt="service"/></a></div>
         <div id="img6"><a href="services.php"> <img src="images/bin.jpg" title="Garbage Clearance" alt="service"/></a></div>
         <div id="img7"><a href="services.php"> <img src="images/Carpet.png" title="Carpet Clearing" alt="service"/></a></div>
         <div id="img8"><a href="services.php"> <img src="images/carpenter.jpg" title="Carpenter" alt="service"/></a></div>
         <div id="img9"><a href="services.php"> <img src="images/housekeeping.jpg" title="House Cleaning" alt="service"/></a></div>
         <div id="img10"><a href="services.php"> <img src="images/roofing.jpg" title="Roofing" alt="service"/></a></div>
         <div id="img11"><a href="services.php"> <img src="images/lawn.png" title="Lawn Maintenance" alt="service"/></a></div>
         <div id="img12"><a href="services.php"> <img src="images/pest.jpg" title="Pest Control" alt="service"/></a></div>
         <div id="img13"><a href="services.php"> <img src="images/lawn.png" title="Lawn Maintenance" alt="service"/></a></div>
         <div id="img14"><a href="services.php"> <img src="images/pest.jpg" title="Pest Control" alt="service"/></a></div>
         <!-- Desktop images-->
        </div>
         <div id="srv_tools_mobile"> <!-- SVG images for mobile -->
         <div id="img1s"> <img src="images/hammer_mobile.png" title="Interior Construction" alt="service"/></div>
         <div id="img2s"> <img src="images/Plug_mobile.png" title="Electrical Maintenance" alt="service"/></div>
         <div id="img3s"> <img src="images/screwdriver_mobile.png" title="Trending Maintenance" alt="service"/></div>
         <div id="img4s"> <img src="images/laptop_mobile.png" title="Gudgets Maintenance" alt="service"/></div>
         <div id="img5s"> <img src="images/beauty_mobile.png" title="Beauty Support" alt="service"/></div>
         <div id="img6s"> <img src="images/bin_mobile.jpg" title="Carpenting" alt="service"/></div>
         <div id="img7s"> <img src="images/Carpet_mobile.png" title="Interior Construction" alt="service"/></div>
         <div id="img8s"> <img src="images/carpenter_mobile.jpg" title="Electrical Maintenance" alt="service"/></div>
         <div id="img9s"> <img src="images/housekeeping_mobile.jpg" title="Trending Maintenance" alt="service"/></div>
         <div id="img10s"> <img src="images/roofing_mobile.jpg" title="Gudgets Maintenance" alt="service"/></div>
         <div id="img11s"> <img src="images/lawn_mobile.png" title="Beauty Support" alt="service"/></div>
         <div id="img12s"> <img src="images/pest_mobile.jpg" title="Carpenting" alt="service"/></div>
         <div id="img13s"> <img src="images/lawn_mobile.png" title="Beauty Support" alt="service"/></div>
         <div id="img14s"> <img src="images/pest_mobile.jpg" title="Carpenting" alt="service"/></div>
         
        </div>
        
        <div id="trending"> <!--  Trending services block created -->
           <h2> Trending Services</h2>
           
           <a href="furnishing.php"> 
           <div id="tren1">
            
          <h3> Furnishing </h3> <!--Three different Trending services block Created-->
                                <!--Inline_Style-->
           <p style="font-size: 16px;">Directions for
              assembling a new piece of furniture can be confusing and 
              the assemblies themselves can be involved and time consuming<br/></p>
           </div>
           </a>
            
           <a href="index.php"><div id="tren2">
           <h3> Lawn Maintenance </h3>
           <p style="font-size: 15px;">A healthy green lawn can be yours.
            Grass seed or sod, either way, be prepared to take care of it.
            Proper lawncare takes bit more effort than just lawn mowing.
            
           </p>
           </div>
           </a>
            
            <a href="index.php">
            <div id="tren3">
            <h3> Beauty Services</h3>
            <p style="font-size: 15px;">A healthy green lawn can be yours.
            Grass seed or sod, either way, be prepared to take care of it.
            Proper lawncare takes bit more effort than just lawn mowing.
            
            </p>
            </div>
            </a>
         </div>
        
         <div id="satisfy"> <!-- Customer Reviews have been display-->
            <h3>SATISFACTION GUARANTEED</h3>
            <h4 style="font-size: 20px;">We guarantee you will be happy with every job!</h4>
            <div id="cmt1">
               <p >
               Their customer service is exceptional. 
               From the time you book your request and then being put in contact with your professional, 
               it's all done within one hour at the most. So happy I found out about Helping Hands.
               <br/><br/>
               <br/>
               Lyne F.
               </p>
               <img src="images/star.png" alt="rating"/>
               
            </div>
            
            <div id="cmt2">
               <p>
               Totally impressed with how quick everything was set up and 
               how quick the job was done.
               <br/>
               <br/>
               <br/>
               <br/>
                Jordana R.
                <img src="images/star.png" alt="rating"/>
               </p>
               
            </div>
            
            <div id="cmt3">
            <p>
               I'm so happy I decided to use Helping Hands the entire process is very
               streamlined and easy to use. Setting up a service is simple and 
               takes no time at all. The service providers who came were courteous, 
               prompt and went well above and beyond.
               I will definitely use them again for all other services,
               and will recommend them to my friends and family for sure!
               <br/>
               <br/>
               <br/>
              Rachela B.
              <img src="images/star_4.png" alt="rating"/>
            </p>
            
            </div>
            
            <div id="cmt4">
               <p>
               Tom was great, as was the entire process. Definitely two thumbs up.
               <br/>
               <br/>
               Heaton W.</p>
               <img src="images/star_3.png" alt="rating"/>
            </div>
            
            <div id="cmt5">
              <p>
              The website is super convenient in terms of finding someone, 
              telling me when they were on their way, paying automatically etc.
              and the pro was AMAZING!

              <br/><br/>Ambika T.
              </p>
              <img src="images/star.png" alt="rating"/>
              </div>
           </div>
       </main>
     </div>

<?php 
    // footer included
   include __DIR__ . '/../include/footer.inc.php';

?>
   

