<?php

   include  __DIR__ . '/../config/config.php';
   include __DIR__ . '/../config/functions.php';
   include __DIR__ . '/../config/connectDB.php';
   require __DIR__ . '/../classes/Validator.php';


   $TITLE = 'UserLogin';
   $body_id="userlogin";
   $title = "Login";

  if(filter_input(INPUT_GET, 'logout')) {
      
      session_destroy();
      setflash('success', "You've successfully logged out");

      header('Location:login.php');
      die;
      
      }

   $v = new Validator();

     if('POST' == filter_input(INPUT_SERVER, 'REQUEST_METHOD')){
       
        //validator both fields which require 
           $v->required('email');
           $v->email('email');
           $v->required('password');


           //if no v Errors
             if(!$v->errors()){
                 
                $post = filter_input_array(INPUT_POST); 

              //query data base for user with email
                  $query = 'SELECT * FROM users WHERE email = :email';
                  $stmt = $dbh->prepare($query);
                  $params = array(':email'=> $post['email']);
                  $stmt->execute ($params);
                  $user = $stmt->fetch(PDO::FETCH_ASSOC);
                    

                  $form_password = $post['password'];
                  $stored_password = $user['password'];

                   


                   //verify the form password matches the stored password
                 if(password_verify($form_password, $stored_password)){
                     
                      session_regenerate_id();

                     $_SESSION['user_id'] = $user['user_id'];

                     $_SESSION['logged_in'] = 1;
                     setFlash('success','You logged in successfully!');
                      
                                    
                      header('Location: users_detail.php');

                      die;
                         
                      }else{
                  
                      setFlash('error','There was a problem with your credentials');

                   }//if password verify ends  
       
            }//if errors end
     
  }//if post end
   

$errors= $v->errors();

include __DIR__ . '/../include/header.inc.php';


?><div id="wrapper">
     
     <?php include __DIR__ . '/../config/flash.php'; ?>

        <main>

            


          <div id="loginin">
              <h1><?=$heading?></h1>
                  
                  <?php require __DIR__ . '/../config/errors.php'; ?> 

                  <form method="post" action="<?=filter_input(INPUT_SERVER,'PHP_SELF', FILTER_SANITIZE_STRING)?>" >
                   <input type="hidden" name="csrf_token" value="<?=getToken()?>">
                    <p>
                      <input type="email" name="email" placeholder="email"
                       value="<?=clean('email')?>" />
                    </p>

                    <p>
                      <input type="password" name="password" placeholder="password"
                      value="<?=clean('password')?>" />
                    </p>

                    <p> 
                    <input value="Login" type="submit" /><!--Buttons-->
                    </p>  
                 
                </form>
          </div>
        
       </main>
    </div>




    
  <!--footer included -->
<?php 
    
   include __DIR__ . '/../include/footer.inc.php';

?>
   

