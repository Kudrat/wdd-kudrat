<?php 
     /*            
            *  
  ########*   *########
  #     *       *     #       
  #   *           *   #       
  # *               * #            
  *   Kudrat Sidhu    *                   
*   Php Aboutus file      *                     
  *                    *                  
  # *               * #              
  #   *           *   #       
  #     *       *     #
  ########*   *########
            *

*/
   $TITLE = 'About us';          //title variable created
   $body_id="information";       //body variable created
   $title = "Our Vision";        //embedded title on about us page
   
   include  __DIR__ . '/../config/config.php';
   include __DIR__ . '/../include/header.inc.php'; // php header file included

?>
    <div id="wrapper"> <!--html code begins-->
      <main>
         <div id="about_us">
           <h1><?=$heading?></h1> <!--Echo heading dynamically -->
           <p> Home Maintenanace should be reliable, quick and 
               easy to book services on one tap.</p>
           <img src="images/vision.jpg" alt="about_us image"/>
           </div>
           
           <div id="about_us_mobile">
            <img src="images/vision_mobile.jpg" alt="about_us image"/>
            <h1><?=$heading?></h1>
            <p> Home Maintenanace should be reliable, quick and 
               easy to book services on one tap.</p>
           </div>
          
           <div id="about_us_content">
           <h2> About us </h2>
           <div id="line">
               
           </div><!-- Inline style--> 
           <p><strong style="font-size: 30px">W</strong>e  believe that for small jobs
           around the house, homeowners would prefer to instantly book a great pro at a 
           pre-determined rate, rather than scour dozens of reviews, obtain multiple quotes
           and schedule around a specific company's availability. Our goal is to create a
           marketplace where both homeowners and home service professionals benefit tremendously.
           <br/><!--inline class applied-->
           For homeowners,<strong class="highlight"> our platform provides a seamless booking experience, easy-to-understand 
           pricing,responsive customer service and an additional guarantee on all work.</strong> 
           <br/>
           For home service professionals,<strong class="highlight"> Helping Hands sends firm jobs and takes care of payment, 
           invoicing, and customer service.</strong> Are you an amazing pro, or know of one?
           Apply here and we'll be in touch! 
           <br/>
           We heard many people say that it is impossible to find a plumber / electrician / carpenter
           who is reliable and can do a quality job. This set us thinking why the current search
           directories don't work. 
           The reasons were many:
           They were paid listings.
           Reviews were fake / not reliable. 
           And once we leave our contact number with search directories, we would get a plethora of calls making my life hell.
         
           <br/>
           Moreover, we still don't have a clue of which service provider to engage with as there is
           no guarantee of quality of work, fair pricing and the provider coming on time to
           do the job. This set us thinking, why this process has to be so painful.
           Why cant we buy local services the way we buy products from ecommerce sites
           on the click of a button.
           
          </div>
         <!--how the process go-->

         <div id="it_works">
            <h2> HOW IT WORKS </h2>
            
            <div id="img1">
             <img src="images/pay.png" alt="payment">
             <p> Step-4<br/>
                 Pay Seamlessly
             
            </div>
           
            <div id="img2">
            <img src="images/shipping-fast-Recovered.png" alt="Shipping" >
            <p> Step-3<br/>
                Track products
                <br/>in route</p>
            </div>
          
            <div id="img3">
            <img src="images/dispatch.png" alt="dispacth">
             <p> Step-2<br/>
                 We'll dispatch<br/>
                 near by pros</p>
            </div>
            
            <div id="line3">
            </div>
            
            <div id="img4">
            <img src="images/tasks.png" alt="making notes">
             <p> Step-1<br/>
                 Fill in the<br/>
                 basic job details</p> 
            </div>
            
            
         </div>
            
         <div id="it_works_mobile">
            <h2> HOW IT WORKS </h2>
            
            <div id="img1s">
             <img src="images/pay_mobile.png" alt="payment">
             <p> Step-4<br/>
                 Pay Seamlessly
             
            </div>
           
            <div id="img2s">
            <img src="images/shipping-fast-Recovered_mobile.png" alt="Shipping" >
            <p> Step-3<br/>
                Track products
                <br/>in route</p>
            </div>
          
            <div id="img3s">
            <img src="images/dispatch_mobile.png" alt="dispacth">
              <p>Step-2<br/>
                 We'll dispatch<br/>
                 near by pros</p>
            </div>
            
            
            
            <div id="img4s">
            <img src="images/tasks_mobile.png" alt="making notes">
             <p> Step-1<br/>
                 Fill in the<br/>
                 basic job details</p> 
            </div>
            
          </div>
             <!--Table created -->
            <table>
                  <caption>Our Budget Sheet</caption>
                  
                  <tr>
                    <th colspan="2"> Fixed Price Sheet</th>
                  </tr>

                  <tr>
                    <th> Service </th>
                    <th> Minimal Prices </th>
                  </tr>
                  
                  <tr>
                    <td> Home Cleaning</td>
                    <td> $500</td>
                  </tr>
                 
                  <tr>
                    <td> Plumbing</td>
                    <td> $600</td>
                 </tr>
                
                  <tr>
                     <td> Electric Maintenance</td>
                     <td> $1000</td>
                  </tr>

                  <tr>
                     <td> Lawn Maintenance</td>
                     <td> $800</td>
                  </tr>

                  <tr>
                      <td> Maids Required</td>
                      <td> $600</td>
                  </tr>

                  <tr>
                      <td> Roofing</td>
                      <td> $1000</td>
                  </tr>

                  <tr>
                      <td> Carpenting</td>
                      <td> $600</td>
                  </tr>

                  <tr>
                      <td> Carpet Maintenance</td>
                      <td> $800</td>
                 </tr>
                  
                  <tr>
                      <td> Painting </td>
                      <td> $1000</td>
                  </tr>
                  
                  <tr>
                      <td> Interior Designing</td>
                      <td> $2000</td>
                  </tr>
                  
                  <tr>
                      <td> Pick and Drop </td>
                      <td> $600</td>
                  </tr>
                  <tr>
                      <td> Snow Cleaning</td>
                      <td> $900</td>
                  <tr>
                      <td> Duct Cleaning</td>
                      <td> $800</td>
                  </tr>
                  
                  <tr>
                      <td>Garbage Cleaning</td>
                      <td> $700</td>

                  </tr>
             
              </table>
                  <!--table ends -->
        </main>
    </div>      <!--footer started-->
      <!--html code ends -->
<?php  
   //footer included through footer.inc.php 
   include __DIR__ . '/../include/footer.inc.php';

?>
   