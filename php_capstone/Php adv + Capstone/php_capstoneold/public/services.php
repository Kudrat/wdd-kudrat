<?php
     /*            
            *  
  ########*   *########
  #     *       *     #       
  #   *           *   #       
  # *               * #            
  *   Kudrat Sidhu    *                   
*   Php Service file     *                     
  *                    *                  
  # *               * #              
  #   *           *   #       
  #     *       *     #
  ########*   *########
            *

*/ 
   //varibales declared and header included
   $TITLE = 'Services';
   $body_id="provides";
   $title = "Our Vision";
   
   include  __DIR__ . '/../config/config.php';
   include __DIR__ . '/../include/header.inc.php';

?>
    
  <div id="wrapper">
    <main>
      <!-- dynamic way of representing heading -->
      <h1><?=$heading?></h1>
       <div id="cleaning_box">
         <div id="cleaning">
           <div id="book">
           <h3>Book Service</h3>
           <p>
              All Handy professionals are vetted, 
              background-checked and highly rated
              by customers like you.
           </p>  
          
            <div id="button_desktop">
            <p> BOOK <p>
            </div>
            <div id="button_mobile">
            <p> BOOK <p>
            </div>
          </div>
        </div>
      </div>
         
         <div id="service1">
            <h2> Cleaning </h2>
            <div id="CleaningImages">
              <!--Each service with the type of service provided-->
              <div id="srv1a_img">
               <h3>All Cleaning Services</h3>
               <p>
               Affordable Cleaning Services
               <br/>
               Bedroom Cleaning
               <br/>
               Maid Services
               <br/>
               Move out services
               <br/>
               Home Cleaning
               <br/>
               Deep Cleaning
               <br/>
               Local Cleaning
              
              </p>  
              </div>
              
              <div id="srv1b_img">
               <p>
               Affordable Cleaning Services
               <br/>
               Bedroom Cleaning
               <br/>
               Maid Services
               <br/>
               Move out services
               <br/>
               Home Cleaning
               <br/>
               Deep Cleaning
               <br/>
               Local Cleaning
              
              </div>
              
              <div id="srv1c_img">
               
               <p>
                 Affordable Cleaning Services
                 <br/>
                 Bedroom Cleaning
                 <br/>
                 Maid Services
                 <br/>
                 Move out services
                 <br/>
                 Home Cleaning
                 <br/>
                 Deep Cleaning
                 <br/>
                 Local Cleaning
              
              </div>
            </div>
           
            
         </div>
        
          <div id="service2">
            <h2>Electrical</h2>
            <div id="Electrical">
              <div id="srv2a_img">
               <h3>All Electrical Services</h3>
               <p>
                 Affordable Cleaning Services
                 <br/>
                 Bedroom Cleaning
                 <br/>
                 Maid Services
                 <br/>
                 Move out services
                 <br/>
                 Home Cleaning
                 <br/>
                 Deep Cleaning
                 <br/>
                 Local Cleaning
               
              </p>  
              </div>
              
              <div id="srv2b_img">
               <p>
                 Affordable Cleaning Services
                 <br/>
                 Bedroom Cleaning
                 <br/>
                 Maid Services
                 <br/>
                 Move out services
                 <br/>
                 Home Cleaning
                 <br/>
                 Deep Cleaning
                 <br/>
                 Local Cleaning
              
              </div>
              
              <div id="srv2c_img">
               
               <p>
                 Affordable Cleaning Services
                 <br/>
                 Bedroom Cleaning
                 <br/>
                 Maid Services
                 <br/>
                 Move out services
                 <br/>
                 Home Cleaning
                 <br/>
                 Deep Cleaning
                 <br/>
                 Local Cleaning
              
              </div>
            </div>
           
            
         </div>
       
        
           <div id="service3">
            <h2>Furniture Assembly</h2>
            <div id="Electrical1">
              <div id="srv3a_img">
               <h3>All Electrical Services</h3>
               <p>
                 Affordable Cleaning Services
                 <br/>
                 Bedroom Cleaning
                 <br/>
                 Maid Services
                 <br/>
                 Move out services
                 <br/>
                 Home Cleaning
                 <br/>
                 Deep Cleaning
                 <br/>
                 Local Cleaning

              </p>  
              </div>
              
              <div id="srv3b_img">
               <p>
                 Affordable Cleaning Services
                 <br/>
                 Bedroom Cleaning
                 <br/>
                 Maid Services
                 <br/>
                 Move out services
                 <br/>
                 Home Cleaning
                 <br/>
                 Deep Cleaning
                 <br/>
                 Local Cleaning
              
              </div>
              
              <div id="srv3c_img">
               
               <p>
                 Affordable Cleaning Services
                 <br/>
                 Bedroom Cleaning
                 <br/>
                 Maid Services
                 <br/>
                 Move out services
                 <br/>
                 Home Cleaning
                 <br/>
                 Deep Cleaning
                 <br/>
                 Local Cleaning

              </div>
            </div>
           
            
         </div>
        
         
           <div id="service4">
            <h2>Outdoor</h2>
            <div id="Electrical2">
              <div id="srv4a_img">
               <h3>All Electrical Services</h3>
               <p>
                 Affordable Cleaning Services
                 <br/>
                 Bedroom Cleaning
                 <br/>
                 Maid Services
                 <br/>
                 Move out services
                 <br/>
                 Home Cleaning
                 <br/>
                 Deep Cleaning
                 <br/>
                 Local Cleaning

              </p>  
              </div>
              
              <div id="srv4b_img">
               <p>
                 Affordable Cleaning Services
                 <br/>
                 Bedroom Cleaning
                 <br/>
                 Maid Services
                 <br/>
                 Move out services
                 <br/>
                 Home Cleaning
                 <br/>
                 Deep Cleaning
                 <br/>
                 Local Cleaning
              
              </div>
              
              <div id="srv4c_img">
               
               <p>
                 Affordable Cleaning Services
                 <br/>
                 Bedroom Cleaning
                 <br/>
                 Maid Services
                 <br/>
                 Move out services
                 <br/>
                 Home Cleaning
                 <br/>
                 Deep Cleaning
                 <br/>
                 Local Cleaning

              </div>
            </div>
           
        </div>
     
          
          <div id="service5">
            <h2>Plumbing</h2>
            <div id="Electrical2">
              <div id="srv5a_img">
               <h3>All Electrical Services</h3>
               <p>
                 Affordable Cleaning Services
                 <br/>
                 Bedroom Cleaning
                 <br/>
                 Maid Services
                 <br/>
                 Move out services
                 <br/>
                 Home Cleaning
                 <br/>
                 Deep Cleaning
                 <br/>
                 Local Cleaning
              
              </p>  
              </div>
              
              <div id="srv5b_img">
               <p>
                 Affordable Cleaning Services
                 <br/>
                 Bedroom Cleaning
                 <br/>
                 Maid Services
                 <br/>
                 Move out services
                 <br/>
                 Home Cleaning
                 <br/>
                 Deep Cleaning
                 <br/>
                 Local Cleaning
              
              </div>
              
              <div id="srv5c_img">
               
               <p>
                 Affordable Cleaning Services
                 <br/>
                 Bedroom Cleaning
                 <br/>
                 Maid Services
                 <br/>
                 Move out services
                 <br/>
                 Home Cleaning
                 <br/>
                 Deep Cleaning
                 <br/>
                 Local Cleaning
              
              </div>
            </div>
         </div>
        
           
           <div id="service6">
            <h2>Painting</h2>
            <div id="Painting">
              <div id="srv6a_img">
               <h3>All Electrical Services</h3>
               <p>
                 Affordable Cleaning Services
                 <br/>
                 Bedroom Cleaning
                 <br/>
                 Maid Services
                 <br/>
                 Move out services
                 <br/>
                 Home Cleaning
                 <br/>
                 Deep Cleaning
                 <br/>
                 Local Cleaning
              </p>  
              </div>
              
              <div id="srv6b_img">
               <p>
                 Affordable Cleaning Services
                 <br/>
                 Bedroom Cleaning
                 <br/>
                 Maid Services
                 <br/>
                 Move out services
                 <br/>
                 Home Cleaning
                 <br/>
                 Deep Cleaning
                 <br/>
                 Local Cleaning

              </div>
              
              <div id="srv6c_img">
               <p>
                 Affordable Cleaning Services
                 <br/>
                 Bedroom Cleaning
                 <br/>
                 Maid Services
                 <br/>
                 Move out services
                 <br/>
                 Home Cleaning
                 <br/>
                 Deep Cleaning
                 <br/>
                 Local Cleaning

              </div>
            </div>
         
         </div> 
      </main>
     
    </div>

<?php 
    //footer included
   include __DIR__ . '/../include/footer.inc.php';

?>
   