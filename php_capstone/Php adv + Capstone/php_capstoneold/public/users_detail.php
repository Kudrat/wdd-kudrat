<?php


   include __DIR__ . '/../config/config.php';
   include __DIR__ . '/../config/functions.php';
   include __DIR__ . '/../config/connectDB.php';



   $TITLE = 'User Details';
   $body_id="user_details";
   $title = "PROFILE";

if(empty($_SESSION['logged_in'])) {

  setFlash('error','You must be logged in to view this page');
 
  header('Location:login.php');
  die;
}   


if(empty( $_SESSION['user_id'])) {
	die('User id required');
}

// a little bit of sanitization
$id = intval( $_SESSION['user_id']);


// Create query to select an author based on id
$query = "SELECT first_name,
                 last_name,
                 street,
                 city,
                 postal_code,
                 province,
                 country,
                 phone_no,
                 email
                 FROM users 
			     WHERE user_id = :user_id";

// prepare the query
$stmt = $dbh->prepare($query);

// Prepare params array
$params = array(
	':user_id' => $id
);

// execute the query
$stmt->execute($params);

// get the result
$result = $stmt->fetch(PDO::FETCH_ASSOC);

include __DIR__ . '/../include/header.inc.php';

?> <div id="wrapper">
        <?php include __DIR__ . '/../config/flash.php'; ?>
        <main>

             
     
             <h1><?=$heading?></h1>
                 
                 <div id="detail">
						<?php if($result) : ?>
            
						<h1><?=$result['first_name']?> Welcome</h1>

						<ul>
							<?php foreach($result as $key => $value) : ?>
								<li><strong><?=$key?></strong>: <?=$value?></l>
							<?php endforeach; ?>
						</ul>

						<p><a href="register.php">Add another user</a></p>

						<?php else : ?>

							<h2>Sorry there was a problem adding users</h2>

						<?php endif; ?>

                </div>

       </main>
    </div>




    
  <!--footer included -->
<?php 
    
   include __DIR__ . '/../include/footer.inc.php';

?>
   

