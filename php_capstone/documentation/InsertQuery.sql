INSERT INTO `services` VALUES (1,
      'Deep-Cleaning',
      'Home-Cleaning',
      'Deep cleaning involves removing as much dirt as possible from all surfaces in your house',
      'Deep cleaning involves removing as much dirt as possible from all surfaces in your house FIRST- Cleaning Appliances SECOND- Cleaning Bathroom THIRD- Removing Dust FOURTH- Targetting your Windows, Walls and Floor Deep cleaning is a top to bottom, thorough and complete cleaning which you can follow up with standard or more routine cleaning tasks
       Deep cleaning entails moving all your knickknacks and moving the furniture to clean under it. Everything is touched, all the way from the top of the ceiling, down to the baseboards and the floor. Everything from the top to bottom and in between is scrubbed until it is free of dirt and dust and shines like new.
       Even the most-cared-for home needs an occasional deep-cleaning, so here a simple list to help you get your home in tip-top shape.',
       1,
       250.00,
      'Dust mop,
      Dust cloths and dusters,
      Rags, towels and soft cloths,
      Indoor broom,
      Outdoor broom,
      Dust pan,
      Vacuum,
      Wet mop,
      Toilet brush (and brush caddy),
      Plunger,
      Spray bottles,
      Squirt bottles,
      Squeegee,
      Sponges,
      Scrub brush,
      Scraper,
      Toothbrush,
      Buckets,
      Rubber gloves,
      Cleaning apron,
      Cleaning caddy,
      equip_details',
      'Minimun 7 othervise according to task and number of days',
      'Minimun 08:00:00 othervise according to task and days',
       50.00,
      'KUP45768',
      '2019-04-10 00:59:47',
      '2019-04-10 00:59:47',
      'cleaning.jpg');

INSERT INTO `services` VALUES (2,
      'Carpet-Cleaning',
      'Home-Cleaning',
      'Our expert carpet cleaning technicians will deep clean your carpets, leaving your carpets smelling fresh for days and adding years to the life of your carpets.',
      'When you book our carpet cleaning service you will get a dedicated technician who is fully trained to meet our strict guidelines and industry standards. We have a network of supervisors who carry out random spot checks to ensure our standards are constantly maintained.
       Your dedicated technician will talk you through the process on the day and show you around your sparkling clean carpets at the end.
       Shampooing is done by applying carpet cleaning foam to the carpet and having a good scrub to get the dirt or stain off. However, this method of cleaning can be counterproductive as shampoo residue is sticky and can attract more dirt after cleaning.
       Steam cleaning is done by blasting steam or hot water through the carpet to dislodge dirt. The dirty water is then vacuumed up by the machine.
       This carpet cleaning method may be better than shampooing because: 
       it can remove bacteria and germs.
       It also does not leave sticky residue so you do not have to worry about re-soiling (provided you let the carpet dry completely before you use it).
       However, steam cleaning may not be effective at removing heavy stains and soils from carpets. Make sure you ask your cleaning service provider for advice on which method to use! ',
       1,
       300.00,
      'We use state of the art equipment and pro-chem detergents to ensure a high quality finish every time
       Professional carpet cleaning machines use
       Vacuum Cleaner
       shampoo
       steamers
       drers',
      'Minimun 3 othervise according to task and number of days',
      'Minimun 12:00:00 othervise according to task and days',
       30.00,
      'KUP45768',
      '2019-04-10 00:59:47',
      '2019-04-10 00:59:47',
      'Carpet.jpg');

INSERT INTO `services` VALUES (3,
      'Power-Washing',
      'Home-Cleaning',
      'We use water to clean your home siding, gutter exteriors, fascia, decks, fences, driveways and patios. Chemical treatment is available upon request. We do not guarantee that all stains will be removed due to the risk of excessive pressure to your property.',
      'We do not power wash sofits.
      We may cancel and reschedule your outside service due to weather conditions:
      Low temperatures – forecasted temperatures below 32°F may cause unevaporated water to freeze and cause damage to property.
      High winds – ladders are unsafe for crews and can blow onto your home.
      Rain – makes roofs and ladders slippery and unsafe for crews.
      Safety of your property and our crews are a top priority. Crews will not clean any areas they consider to be inaccessible, unsafe or likely to be damaged by the washing. You will not be charged for these exclusions.
      We reserve the right to not clean surfaces that are damaged or not in good repair.
      Preparing For Service
      Customer is not required to be home at the time of service.
      A water spigot must be activated and accessible.
      Please have all windows and doors shut tightly and all pets inside of your home.
      Clear the work areas of all items and remove all flags, doormats, vehicles, or sensitive materials from the areas being washed. For decks, please remove furniture, décor, grills, etc.
      We suggest that power to exterior outlets, light fixtures or other electrically powered features be shut off. This is especially important for uncovered outlets.',
       1,
       400.00,
      'We use water and commercial-grade power washers to clean your home siding, decks, fences, driveways and patios.
       Use of residential-grade cleaning chemicals – additional charges apply.
       Soft Washing - use of low-pressure, usually with chemicals.
       Hand scrubbing – additional charges apply.',
      'Minimun 3 othervise according to task and number of days',
      'Minimun 6:00:00 othervise according to task and days',
       50.00,
      'KUP45768',
      '2019-04-10 00:59:47',
      '2019-04-10 00:59:47',
      'powerwash.jpg');
    

INSERT INTO `services` VALUES (4,
      'Gutter-Cleaning',
      'Home-Cleaning',
      'We remove all debris from inside gutters and make sure downspouts are not clogged.',
      'For gutters with covers or screens we will remove the covers and reinstall them after the gutter cleaning at an extra charge. Our estimator cannot see covers or screens from the ground, so please let us know if you have them when scheduling.
       Exterior cleaning of gutters – available at extra cost.
       We may cancel and reschedule your outside service due to weather conditions:
      Low temperatures – below 32°F any wet debris will freeze. Remember, temperatures need to be above 32°F for an extended period of time for debris to thaw.
      High winds – ladders are unsafe for crews and can blow onto your home.
      Rain – makes roofs and ladders slippery and unsafe for crews.
      Safety of your property and our crews are a top priority. Crews will not clean any gutters they consider to be inaccessible or unsafe. You will not be charged for these exclusions.
      We reserve the right to not clean gutters that are damaged or not in good repair.',
       1,
       500.00,
      'Remove debris from inside gutters. Wet materials will be hand scooped and dry debris may be removed by blowers or by hand.
       Downspouts will be checked and cleared as needed.
       Debris will be bagged and removed from premises',
      'Minimun 3 othervise according to task and number of days',
      'Minimun 6:00:00 othervise according to task and days',
       50.00,
      'KUP45768',
      '2019-04-10 00:59:47',
      '2019-04-10 00:59:47',
      'gutter-cleaning.jpg'); 


INSERT INTO `services` VALUES (5,
      'Window-Cleaning',
      'Home-Cleaning',
      'We clean panes of glass on windows and doors. We also clean chandeliers, interior and exterior light fixtures, skylights, deck railing glass, glass cabinets, mirrors and fireplace glass.',
      'Glass panes, including storm windows
       Wipe outside frame, Screen cleaning – remove screen, wipe down with rag, replace screen.
      Jamb cleaning – clean “tracks” inside of window.
      Hard water stain removal – additional charges apply.
      Construction cleaning – remove stickers and construction dust from windows.
      Cobweb removal.
      Bug spray – using a residential-grade insecticide.
      Light fixture cleaning – disassemble, clean out bugs, clean glass, assemble.
      Chandelier cleaning – customer to provide picture for estimate.
      Skylight cleaning.
      Deck railing glass cleaning.
      Jalousie window cleaning.
      Mirror cleaning.
      Fireplace glass cleaning.',
       1,
       300.00,
      'Spray, Fuller Brush, Squeeze, unger 2 in 1',
      'Minimun 3 othervise according to task and number of days',
      'Minimun 6:00:00 othervise according to task and days',
       50.00,
      'KUP45768',
      '2019-04-10 00:59:47',
      '2019-04-10 00:59:47',
      'gutter-cleaning.jpg');                                  
                              

INSERT INTO `services` VALUES (6,
      'Carpentry',
      'Construction',
      'Carpenters normally framed post-and-beam buildings until the end of the 19th century; now this old fashioned carpentry is called timber framing. Carpenters learn this trade by being employed through an apprenticeship training—normally 4 years—and qualify by successfully completing that country competence test in places such as the United Kingdom, the United States, Australia and South Africa. It is also common that the skill can be learned by gaining work experience other than a formal training program,',
      'Our work is performed by an experienced carpentry handyman who will walk you through every step of the process for a simple and seamless experience. Our goal is your complete satisfaction.
       JOBS WE SPECIALISED IN are : 
       Interior and exterior door installation or replacement
      Interior and exterior trim installation or replacement
      Base trim and crown molding installation
      Beadboard and wainscoting installation
      Adding pet doors and storm doors
      Complete cabinet repairs or replacement
      Window installation or replacement',
       1,
       700.00,
      'Spray, Fuller Brush, Squeeze, unger 2 in 1',
      'Minimun 10 othervise according to task and number of days',
      'Minimun 48:00:00 othervise according to task and days',
       100.00,
      'KUP45768',
      '2019-04-10 00:59:47',
      '2019-04-10 00:59:47',
      'carpentry.jpg');

INSERT INTO `services` VALUES (7,
      'Building houses',
      'Construction',
      'Building a new home is an opportunity to meet your exact design and usability requirements. But from-scratch construction can be a daunting task, especially when it comes to price. This in-depth look at the prices associated with home building will help you make the right budgetary decisions',
      'One of the amazing things about Canadian homes is that the huge majority of them are built using completely standardized building practices. One reason for this consistency is a set of uniform building codes that apply across the country. Another reason is cost -- the techniques used to build homes produce reliable housing quickly at a low cost (relatively speaking). If you ever watch any house being built, you will find that it goes through the following steps:
      Grading and site preparation
      Foundation construction
      Framing
      Installation of windows and doors
      Roofing
      Siding
      Rough electrical
      Rough plumbing
      Rough HVAC
      Insulation
      Drywall
      Underlayment
      Trim
      Painting
      Finish electrical
      Bathroom and kitchen counters and cabinets
      Finish plumbing
      Carpet and flooring
      Finish HVAC
      Hookup to water main, or well drilling
      Hookup to sewer or installation of a septic system
      Punch list',
       1,
       900,
      'null',
      'Minimun 10 othervise according to task and number of days',
      'Minimun 48:00:00 othervise according to task and days',
       900.00,
      'KUP45768',
      '2019-04-10 00:59:47',
      '2019-04-10 00:59:47',
      'Building.jpg');

INSERT INTO `services` VALUES (8,
      'Painting',
      'Furnishing',
      'A new coat of paint or new wall treatments are easy ways to refresh the look and feel of your home. Whether you need interior or exterior painting, you can count on the pros from Handyman Connection to make you fall in love with your home all over again. Our painting services are provided by professionals with the specialized skills to get the job done right, from careful prep work through the final coat and cleanup.',
      'Our work is performed by experienced craftsmen who walk you through the process every step of the way for a simple and seamless experience. Our goal is your complete satisfaction.
       WE ARE SPECIALISED IN :
       Complete interior and exterior painting
      Faux finish painting and texture
      Painting accents, base boards and trim
      Hanging wallpaper
      Adding wallpaper boarders
      Powerwashing Decks and Fences
      Staining Decks and Fences',
       1,
       500,
      'null',
      'Minimun 10 othervise according to task and number of days',
      'Minimun 48:00:00 othervise according to task and days',
       100.00,
      'KUP45768',
      '2019-04-10 00:59:47',
      '2019-04-10 00:59:47',
      'painter2.jpg');


INSERT INTO `services` VALUES (9,
      'Furniture-Repair',
      'Furnishing',
      'Furniture is one of the most use items,, made from different materials, serves different means of use: residential commercial, hospitality, medical, stationary, functional, mechanical, power operated, mechanism controlled, reclining, rocking, swiveling, integrated with automations, lights, chargers, massagers, heat components, multi-function controls and more, because of such differences no one person or freelancer can be proficient and have experience enough to handle them all that is another reason why we are superior in what we do as we always assign/send the best qualified professional technician for the task.',
      'WE OUR SPECIALISED IN :
      Complete Reupholstery
      Seam Separation
      Patching
      Fabric Pull Repair
      Webbing
      Tufting / Buttons
      Micro-Fiber Fill
      Parts Installation
      Piping / Nail- Heads
      Partial Reupholstery
      Tear Repair
      Spot Cleaning
      Full Machine Cleaning
      Animal Damage Repair
      Customization / Design
      Wrinkle Release
      Cigarette Burn Removal
      Hand Stitching
      Machine Stitching
      Re-Stuffing / Re-Padding
      Touch-Ups and More!',
       1,
       600,
      'null',
      'Minimun 10 othervise according to task and number of days',
      'Minimun 48:00:00 othervise according to task and days',
       100.00,
      'KUP45768',
      '2019-04-10 00:59:47',
      '2019-04-10 00:59:47',
      'furnishing.jpg');

INSERT INTO `services` VALUES (10,
      'Yard Cleaning up',
      'Gardening',
      'A beautiful garden can transform a property. Whether youre trying to recreate the gardens at Versailles, or something a bit less extravagant, Lawn Love can help. We will get your yard looking just the way you like it, and ensure it stays that way. Think of your yard as Cinderella and Lawn Love as a Fairy Godmother, just without the midnight cutoff. ',
      'We can help take care of a broad array of gardening tasks, some of which include: bush and shrub trimming, flower planting, weeding, green waste disposal, mowing, edging, and more. Each of our gardeners have an average of 10 years of experience caring for gardens of all shapes and sizes. 
       Yard clean up service
       We get it. Sometimes life can get in the way of your regular maintenance, and your once beautiful yard quickly starts to resemble something out of The Jungle Book. That is where we come in. We will clean up your yard from top to bottom, and help bring your outdoor areas back to a state of beauty. 
       We can handle a wide variety of clean up tasks, some of which include: leaf raking, mulching, mowing and edging, hedge trimming, weed control, tree maintenance, green waste disposal, and more. ',
       1,
       600,
      'null',
      'Minimun 10 othervise according to task and number of days',
      'Minimun 48:00:00 othervise according to task and days',
       100.00,
      'KUP45768',
      '2019-04-10 00:59:47',
      '2019-04-10 00:59:47',
      'lawn.jpg');


INSERT INTO `services` VALUES (11,
      'Leaf-Removal',
      'Gardening',
      'For several weeks every year the trees on your property shed their leaves like an Alaskan Husky does fur. If left unmanaged, leaves can suffocate your lawn and prevent photosynthesis. Leaf removal or clean-up is your only hope.',
      'Top Rated Leaf Removal & Leaf Clean-Up Services
       For several weeks every year the trees on your property shed their leaves like an Alaskan Husky does fur. If left unmanaged, leaves can suffocate your lawn and prevent photosynthesis. Leaf removal or clean-up is your only hope. 
       Dont be a lawn strangler; call Lawn Love. Our fast and affordable leaf clean-up service will have your yard clear of leaves and sticks in no time. We will quote you right over the phone or online. Before you know it, Lawn Love will have your leaf issue under control.  ',
       1,
       600,
      'null',
      'Minimun 10 othervise according to task and number of days',
      'Minimun 48:00:00 othervise according to task and days',
       100.00,
      'KUP45768',
      '2019-04-10 00:59:47',
      '2019-04-10 00:59:47',
      'leaf_removal.jpg');


INSERT INTO `services` VALUES (12,
      'Lawn Fertilizer service',
      'Gardening',
      'The key to achieving a beautiful lawn lies in properly educating yourself. Your lawn needs the correct nutrients in order to survive (and thrive). You need to know what your lawn needs and how often. This will vary depending upon the area in which you live and the grass you have. Additionally, diseases, insects, environmental and weather conditions, and inconsistent moisture levels in the soil can all jeopardize the overall health of your lawn. One of the best ways to combat these potential problems is to provide your lawn with the right nutrients. ',
      'Benefits of lawn fertilization include:
      Faster Recovery: If your lawn is experiencing discoloration or dry patches, it has likely been affected by a lack of water, high temperatures, insect damage, lawn disease, and/or harmful weeds. A proper lawn fertilizing schedule will contribute to a rapid recovery.
      Faster Growth: Grass needs to grow quickly in order to reach a high lawn quality. 
      Resistance to Weather/Environmental Conditions: An adequately fertilized lawn provides resistance to stress conditions, like extreme weather and heavy foot traffic. The more nutrients your lawn has, the better equipped it is to survive through these conditions. 
      Vibrant Color: Lawn fertilization is a primary contributor to providing that richly vibrant and green lawn you love. The combination of adequate moisture levels and nitrogen (the ingredient highly associated with deeper hues) gives your lawn a healthy sheen.
      High Density: Thick grass makes your lawn look better, but it doesn’t stop there. It also aids in weed control by taking up all available space, so weeds have no room to take root.
      Lower Temperature: Healthy grass can eliminate up to 50% of heat in the surrounding area because of a process called transpiration.',
       1,
       600,
      'null',
      'Minimun 10 othervise according to task and number of days',
      'Minimun 48:00:00 othervise according to task and days',
       100.00,
      'KUP45768',
      '2019-04-10 00:59:47',
      '2019-04-10 00:59:47',
      'fertilizer.jpg');



INSERT INTO `services` VALUES (13,
      'Interior-Designing',
      'Architecture and Design',
      'The key to achieving a beautiful lawn lies in properly educating yourself. Your lawn needs the correct nutrients in order to survive (and thrive). You need to know what your lawn needs and how often. This will vary depending upon the area in which you live and the grass you have. Additionally, diseases, insects, environmental and weather conditions, and inconsistent moisture levels in the soil can all jeopardize the overall health of your lawn. One of the best ways to combat these potential problems is to provide your lawn with the right nutrients. ',
      'Benefits of lawn fertilization include:
      Faster Recovery: If your lawn is experiencing discoloration or dry patches, it has likely been affected by a lack of water, high temperatures, insect damage, lawn disease, and/or harmful weeds. A proper lawn fertilizing schedule will contribute to a rapid recovery.
      Faster Growth: Grass needs to grow quickly in order to reach a high lawn quality. 
      Resistance to Weather/Environmental Conditions: An adequately fertilized lawn provides resistance to stress conditions, like extreme weather and heavy foot traffic. The more nutrients your lawn has, the better equipped it is to survive through these conditions. 
      Vibrant Color: Lawn fertilization is a primary contributor to providing that richly vibrant and green lawn you love. The combination of adequate moisture levels and nitrogen (the ingredient highly associated with deeper hues) gives your lawn a healthy sheen.
      High Density: Thick grass makes your lawn look better, but it doesn’t stop there. It also aids in weed control by taking up all available space, so weeds have no room to take root.
      Lower Temperature: Healthy grass can eliminate up to 50% of heat in the surrounding area because of a process called transpiration.',
       1,
       600,
      'null',
      'Minimun 10 othervise according to task and number of days',
      'Minimun 48:00:00 othervise according to task and days',
       100.00,
      'KUP45768',
      '2019-04-10 00:59:47',
      '2019-04-10 00:59:47',
      'fertilizer.jpg');



INSERT INTO `services` VALUES (14,
      'Wiring and Fittings',
      'Electronics',
      'If you are looking for electrical services beyond your capabilities, you will need to hire a professional. Handyman Connection provides electricians to handle all of your electrical repair and installation needs, from changing out switches to mounting ceiling fans and hard-wired carbon monoxide detectors. ',
      'Our electrical work is performed by an experienced, electrical handyman who will walk you through every step of the process for a simple and seamless experience. Our goal is your complete satisfaction.
      Jobs We Specialize In
      Installing and repairing electrical fixtures, outlets and switches
      Ceiling fan installation
      Light fixture installation
      Upgrading electrical panels and sub-panels
      Installing GFCIs (ground fault circuit interrupters) and electrical surge protectors
      Installing motion detectors and other security equipment
      Adding hard-wired smoke detectors
      Installing carbon monoxide detectors
      Installing recessed lighting
      Installing under cabinet lighting
      Installing and wiring for backup generators',
       1,
       600,
      'null',
      'Minimun 10 othervise according to task and number of days',
      'Minimun 48:00:00 othervise according to task and days',
       100.00,
      'KUP45768',
      '2019-04-10 00:59:47',
      '2019-04-10 00:59:47',
      'electrical2.jpg');

