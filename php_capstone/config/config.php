<?php


      ini_set('display_errors', 1);  //show only browser errors
      ini_set('error_reporting', E_ALL);  //show every kind off errors even warnings

      ob_start(); //buffer overflow

      session_start(); //session start

      
      define('BASE_PATH', __DIR__ . '/..');

        //csr
if (empty($_SESSION['csrf_token'])) {
    $_SESSION['csrf_token'] = md5(rand());
}

if ('POST' == $_SERVER['REQUEST_METHOD']) {
    if ($_POST['csrf_token'] != $_SESSION['csrf_token']) {
        die('CSRF TOKEN MISMATCH');
    }
}


  spl_autoload_register('my_autoload');

function my_autoload($class)
{

    $class = trim($class, '\\');
    $class = str_replace('\\', '/', $class);
    $class = $class . '.php';

    $file = BASE_PATH . '/' . $class;

    if (file_exists($file)) {
        require $file;
    }
}


        //if $_SESSION cart is empty set $_SESSION['cart'] as an array
if (empty($_SESSION['cart'])) {
    $_SESSION['cart'] = [];
}

$cart = $_SESSION['cart'];

        //as config file included these two files will included automaticallys

include __DIR__ . '/../config/connectDB.php';
include __DIR__ . '/../config/log.php';
