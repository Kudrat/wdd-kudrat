<?php if ($errors) : ?>
 <ul>
   <!-- for each key value pair getting error store in error array -->
  <?php foreach ($errors as $key => $value) : ?>
   <!--  escape the value -->
   <li class="errors"><?=e($value)?></li>
<?php endforeach; ?>
</ul>
<?php endif; ?>