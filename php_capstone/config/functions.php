<?php


define('GST', .05);
define('PST', .07);

    /**
     * Escape string for general use in HTML
     * @param  String $string data to be sanitized
     * @return String
     */
function e($string)
{
    return htmlentities($string, null, 'UTF-8');
}

    /**
     * Escape string for use in attribute (quotes entitized)
     * @param  String $string data to be sanitized
     * @return String
     */
function e_attr($string)
{
    return htmlentities($string, ENT_QUOTES, 'UTF-8');
}

    /**
     *Makes field values sticky
     *!empty@return $field values
     */
function clean($field)
{
    if (!empty($_POST[$field])) {
        return htmlentities($_POST[$field], ENT_QUOTES, "UTF-8");
    } else {
        return '';
    }
}

    /**
     *
     *@return csrf_token
     */
function getToken()
{
    return $_SESSION['csrf_token'];
}


function dd($var)
{
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
}


function setFlash($type, $message)
{
    $_SESSION['message'] = [$type, $message];
}

    
    /*
     *
     */

function getCart()
{
    if (empty($_SESSION['cart'])) {
        
        return 'Your cart is empty';
    } else {
        return "1 items in cart";
    }
}

    /**
     * Single Item cart
     * @param Array $services
     */
function addToCart($service)
{
    global $dbh;
    $query = 'SELECT * FROM services WHERE service_id = :service_id';
    $params = array(
       ':service_id' => $service['service_id']);

    $stmt = $dbh->prepare($query);
    $stmt->execute($params);
    $qty = $service['qty'];
    $service = $stmt->fetch();

    $item = array(
     'service_title'=>$service['service_title'],
     'service_cost' =>$service['service_cost'],
     'hours'=>$qty,
     'line_total'=> $qty * $service['service_cost']
    );
    $_SESSION['cart'] = $item;
}

function cart()
{
    return count($_SESSION['cart']);
};



function getCartSubTotal($cart)
{
      
    $sub = 0;
    foreach ($cart as $service) {
        $sub += $service['line_total'];
    }
    return $sub;
}

function getPst($sub)
{
    return $sub * PST;
}

function getGST($sub)
{
    return $sub * GST;
}
