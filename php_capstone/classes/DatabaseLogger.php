<?php

namespace classes;

use classes\ILogger;

class DatabaseLogger implements ILogger
{
//class DatabaseLogger Begins

    protected $dbh;

    public function __construct(\PDO $dbh)
    {
            //PDO is an interface or it extends interface
            $this->dbh = $dbh;
    }
       
        /**
        *write $event
        * @param $event
        */
    public function write($event)
    {

            $query ='INSERT INTO log (event) VALUES (:event)';
            $stmt = $this->dbh->prepare($query);
            $params = array(':event' => $event);
            $stmt->execute($params);
    }
}//class DatabaseLogger ends
