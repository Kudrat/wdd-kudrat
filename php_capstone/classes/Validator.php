<?php


class Validator
{

    private $errors = [];

    /**
     * Ensure a value is provided for required field
     * @param  String $field the field name
     * @return void
     */
    public function required($field)
    {
            
        if (!filter_input(INPUT_POST, $field)) {
                $this->setError($field, $field . ' is a required field');
        }//if ends
    }//required ends

    
    public function email($field)
    {

        if (!filter_input(INPUT_POST, $field, FILTER_VALIDATE_EMAIL)) {
                $this->setError($field, 'Please provide a valid email address');
        }//if ends
    }//email function ends
    
    /**
     *card validation
     *@param $field
     *@return void
     */
    public function card_num($field){
        if(!preg_match("/^[0-9]{16}$/", filter_input(INPUT_POST, $field) )){
            $this->setError($field, $field. ' is not valid must be 16 digit number');
        }
    }
    
     /**
     *cvv validation function
     *@param $field
     *@return void
     */
    public function cvv($field){
        if(!preg_match("/^[0-9]{3}$/", filter_input(INPUT_POST, $field))){
           $this->setError($field, $field . ' is not valid must be 3 digit number');
        }

    }

    /**
     * Return the errors array
     * @return Array
     */
    public function errors()
    {
            
            return $this->errors;
    }

    private function setError($field, $message)
    {
        if (empty($this-> errors[$field])) {
                $this-> errors[$field] = $message ;
        }//if ends
    }//setError ends
}//Validator class ends
