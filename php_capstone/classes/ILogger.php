<?php

namespace classes;

interface ILogger
{

    public function write($event);
}
