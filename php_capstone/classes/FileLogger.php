<?php

namespace classes;

use classes\ILogger;

class FileLogger implements ILogger
{

    protected $logfile;

       /**
        * __construct
        *@param $logfile
        */
    public function __construct($logfile)
    {

            $this->logfile = $logfile;
    }//construct function ends
    

        /**
        * Write func
        *@param $event
        *
        */
        
    public function write($event)
    {
       
        if (is_string($event)) {
                file_put_contents($this->logfile, date("Y-m-d H:i:s")." ".$event."\n", FILE_APPEND);
        }//if ends
    }//write function ends
}//class ends
