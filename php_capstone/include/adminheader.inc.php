<?php
/*
*
########*   *########
#     *       *     #
#   *           *   #
# *               * #
*   Kudrat Sidhu    *
*     Php Include file   *
*                    *
# *               * #
#   *           *   #
#     *       *     #
########*   *########
*

*/



?><!doctype html>

<html lang="en">
<head> 
    <title><?=$TITLE?></title> <!--dynamic way of creating title -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width">

    <link rel="icon" href="images/logo_pj1_icon.png" type="image/gif">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

    <link rel="stylesheet" 
    href="style/admin_style_sheet.css"
    type="text/css"
    media="screen and (min-width:751px)"
    />



    <!-- Conditional comments for IE browsers -->

<!-- [if LTE IE 8]>
<link rel="stylesheet" href="style/old_ie1.css" type="text/css" />
<![endif] -->


<?php

$active = "breadcrumbs";  //Breadcrumbs(highlighted activ class created)

//embedded css file included on basis of title if title == our vision then this  embedded css works

// each page assign body id if body_id == id on each page then heading will be displayed dynamically
if ($body_id == 'logs') {
    $heading = "Log Details";
}elseif($body_id == 'products'){
    $heading = "List of services";
}elseif($body_id == 'orders'){
    $heading ="Order Details";
}elseif($body_id == 'users'){
    $heading = "Customer Details";
}elseif($body_id == 'delete'){
    $heading = "Delete the service";
}elseif($body_id == 'create'){
    $heading = "Create the service";
}elseif($body_id == 'admin'){
    $heading = "Helping Hands overall Analysis";
}elseif($body_id == 'employees'){
    $heading = "Employees Details";
}else ($body_id == 'edit'){
    $heading = "Edit the Services"
}

?>

<script src="style/old_ie1.js"></script>
<style>

</style>
</head>


<body>
    <div id="<?=$body_id?>"><!--echo id = "body_id" as per different pages-->
        <header>
           <div id="inner_header">

                <div id="logo"><img src="images/logo_pj1.png" alt="Logo" /></div>

                <div id="name">Helping Hands Admin</div>
            </div>
        </header>

        <nav>
            <div id="navdiv">

            <ul>
                <!-- if body_id == specific id on each page then it outputs active class with navstyle highlighting the particular navlist block (showing breadcrumbs)-->

                <li>
                    <a href="admin.php"
                    class="<?php
                    if ($body_id == 'aggre') {
                        echo $active;
                    }?>" >Admin
                   </a>

                </li>

                <li>
                    <a href="products.php"
                    class="<?php
                    if ($body_id == 'products') {
                        echo $active;
                    }?>" >Services
                   </a>

                </li>

            <li class="menu2">
                <a href="orders.php"
                class="<?php
                if ($body_id == 'orders') {
                    echo $active;
                }?>" >Orders
            </a>

        </li>

        <li class="menu3">
            <a href="logs.php"
            class="<?php
            if ($body_id == 'logs') {
                echo $active;
            }?>">Logs
        </a>
        </li>

    <li>
        <a href="user_view.php"
        class="<?php
        if ($body_id == 'user_info') {
            echo $active;
        }?>" >Users View
    </a>
</li>


    <li>
        <a href="/index.php"
        class="<?php
        if ($body_id == 'homepage') {
            echo $active;
        }?>" >Home
       </a>
  </li>

  <li>
        <a href="employees_details.php"
        class="<?php
        if ($body_id == 'employees') {
            echo $active;
        }?>" >Employees Details
    </a>
  
  </li>





<li>
    <a href="\login.php?logout=1"
    class="<?php
    if ($body_id == 'userlogin') {
        echo $active;
    }?>">Logout
</a>
</li>

</ul>
</div>

</nav>
