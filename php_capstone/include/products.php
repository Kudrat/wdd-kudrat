<?php
/*
*
########*   *########
#     *       *     #
#   *           *   #
# *               * #
*   Kudrat Sidhu    *
*   Php Service file     *
*                    *
# *               * #
#   *           *   #
#     *       *     #
########*   *########
*

*/
//varibales declared and header included
$TITLE = 'Services List View';
$body_id="products";
$title = "Our Vision";

//included config and function files
include  __DIR__ . '/../../config/config.php';
include __DIR__ . '/../../config/functions.php';


//if not empty get the search string and match with the service_title and service_description
if (!empty($_GET['search'])) {
    $query = "SELECT 
              * 
              FROM services 
              WHERE 
              service_title LIKE :search
              OR service_short_description LIKE :search";

//params assign specific word searched to :search
    $params= [
        ':search' => "%{$_GET['search']}%"
    ];

// prepare the query
    $stmt = $dbh->prepare($query);

// execute the query
    $stmt->execute($params);
// get the result
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
} elseif (!empty($_GET['type'])) {
//if not empty and getting type of service then select all information from services.
    $query = "SELECT 
              * 
              FROM services 
              WHERE 
              service_type = :type";


    $params= [
        ':type' => e_attr($_GET['type'])
    ];

// prepare the query
    $stmt = $dbh->prepare($query);

// execute the query
    $stmt->execute($params);
// get the result
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
} else {
// Create query to select an author based on id
    $query = "SELECT * FROM services";

// prepare the query
    $stmt = $dbh->prepare($query);

// execute the query
    $stmt->execute();
// get the result
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
}

include __DIR__ . '/../../include/adminheader.inc.php';

?>
<div id="wrapper">
    <main>

        <div class="container">
            <div id = srv_img><img src="images/services/servicee_tools.jpg" alt="main_image" /></div>
            <!-- dynamic way of representing heading -->


            <div id='listview_nav'>
                <ul>
                   <!--  type of service declared === to type in DB -->
                    <li><a href="services.php?type=cleaning">Home Cleaning</a></li>
                    <li><a href="services.php?type=plumbing">Plumbing</a></li>
                    <li><a href="services.php?type=yard">Gardening</a></li>
                    <li><a href="services.php?type=yard">Furnishing</a></li>
                    <li><a href="services.php?type=yard">Electronics</a></li>
                    <li><a href="services.php?type=yard">Architect and Designing</a></li>


                </ul>
            </div> 


            <h1><?=$heading?></h1>
            <!-- action of the form performedon page itself -->
            <form action="<?=$_SERVER['PHP_SELF']?>" method= "get" novalidate>
              <!--   convert value to encrypted token -->
                <input type="hidden" name="csrf_token" value="<?=getToken()?>"/>
                <input type="text" name="search" placeholder="Search.."/>
                <button class="button" value="Search">Search</button>
            </form>

                <?php foreach ($result as $value) : ?>
                    <!-- fectching details from Database in listView -->
                    
                    <ul>
                        <li><img src="/images/services/<?=$value['image']?>" alt="<?=$value['service_title']?>" /></li>
                        <li>Service :<?=e_attr($value['service_title'])?></li><!--LINK-->
                        <li>Price :<?=e_attr($value['service_cost'])?></li>
                        <li>Man-Required :<?=e_attr($value['total_man_required'])?></li>
                        <li>Description :<?=e_attr($value['service_short_description'])?></li>
                        <li><a href="/service_details.php?id=<?=$value['service_id']?>">DETAILS</a></li><!--LINK-->

                    </ul>
                <?php endforeach; ?><!-- end for each -->

        
        </div>

    </main>

</div>

<?php
//footer included
include __DIR__ . '/../../include/footer.inc.php';

?>
