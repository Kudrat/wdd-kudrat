<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package MyVision
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function vision_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'vision_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function vision_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'vision_pingback_header' );


/**

 * Excerpt functions

 */



/**

 * This changes the length of our automatic excerpts

 */

add_filter('excerpt_length', function( $length ) { return 35; } );



/**

 * When a manual excerpt is created by a user this filter runs

 */

function vision_manual_excerpt_more( $excerpt ) {

	$excerpt_more = '';

	if( has_excerpt() ) {

    	$excerpt_more = '&nbsp;<div class="wp-block-button"><a class="wp-block-button__link" href="' . get_permalink() . '" rel="nofollow" class="button">Read More</a></div>';

	}

	return $excerpt . $excerpt_more;

}

add_filter( 'get_the_excerpt', 'vision_manual_excerpt_more' );



/**

 * When a automatic excerpt is created by WP this filter runs

 */

function vision_excerpt_more($more) {

	global $post;

	return '… <div class="wp-block-button"><a class="wp-block-button__link"  href="'. get_permalink($post->ID) . '" class="button">' . 'Read More' . '</a></div>';

}

add_filter('excerpt_more', 'vision_excerpt_more');