<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package MyVision
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'vision' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
					<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'vision' ); ?></p>

					<?php
					get_search_form();

					
					?>
				</div>
                      <h3>Recent Post</h3>
					 <div class="archive wrapper">
					 	    
					        <?php

					 

					  
					          $sticky_posts = get_option('sticky_posts');//find no. of sticky posts 
					          $sticky_count = count($sticky_posts);//and get an array...
					          $ideal_posts_amt = 3;
					          $posts_amt = $ideal_posts_amt - $sticky_count;
					          if( $sticky_count > $ideal_posts_amt){
					            $posts_amt = $sticky_count;
					          }
                             
					          $home_args = array(
					             'post_type' => 'post',
					             'posts_per_page' => $posts_amt,
					             'post_status' => 'publish',


					          );
					          $home_query = new WP_Query( $home_args );

					          while($home_query->have_posts()) : 
					            $home_query->the_post();
					            get_template_part('template-parts/content', 'archive');
					          endwhile;
					        

					        ?>
					     </div>

					
				
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
