<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package MyVision
 */

?>

	</div> <!-- content -->
    </div> <!-- wrapper -->
	<footer id="colophon" class="site-footer">

<?php	 
if ( is_active_sidebar( 'footer-1' ) ) {


?>

<aside id="secondary" class="widget-area">
	<?php dynamic_sidebar( 'footer-1' ); ?>

</aside><!-- #secondary -->

<?php } ?>
		<div class="site-info">

			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'vision' ) ); ?>">
				<?php
				/* translators: %s: CMS name, i.e. WordPress. */
				printf( esc_html__( 'Proudly powered by %s', 'vision' ), 'WordPress' );
				?>
			</a>
			<span class="sep"> | </span>
				<?php
				/* translators: 1: Theme name, 2: Theme author. */
				printf( esc_html__( 'Theme: %1$s by %2$s.', 'vision' ), 'vision', '<a href="http://underscores.me/">Kudrat Sidhu</a>' );
				?>
		</div><!-- .site-info -->

		
    	
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
