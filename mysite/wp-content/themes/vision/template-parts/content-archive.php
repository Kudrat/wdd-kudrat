<?php
/**
 * Template part for displaying posts on archive 
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MyVision
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="entry-header">
        <?php
      
            the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
        
        if ( 'post' === get_post_type() ) :
            ?>
            <div class="entry-meta">
                <?php
                vision_posted_on();
                ?>
            </div><!-- .entry-meta -->
        <?php endif; ?>
    </header><!-- .entry-header -->

    <?php vision_post_thumbnail(); ?>

    <div class="entry-content">
        <?php
       the_excerpt();

        wp_link_pages( array(
            'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'vision' ),
            'after'  => '</div>',
        ) );
        ?>

       <!--  <button class="excerpt_button">Read More</button> -->
    </div><!-- .entry-content -->

    <footer class="entry-footer">
        
    </footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
