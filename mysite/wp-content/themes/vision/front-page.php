<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MyVision
 */

get_header();
?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main">

        <?php
        if( is_page()){
        while ( have_posts() ) :
            the_post();

            get_template_part( 'template-parts/content', 'page' );

        endwhile; // End of the loop
        }
        ?>




       <div class="archive wrapper">
        <?php
         if( is_home()){ //when lastest post selected then ocurs
        while ( have_posts() ) :
            the_post();

            get_template_part( 'template-parts/content', 'archive' );

        endwhile; // End of the loop
        } 

        if(is_page()){ // when static page is slected
          $sticky_posts = get_option('sticky_posts');//find no. of sticky posts 
          $sticky_count = count($sticky_posts);//and get an array...
          $ideal_posts_amt = 6;
          $posts_amt = $ideal_posts_amt - $sticky_count;
          if( $sticky_count > $ideal_posts_amt){
            $posts_amt = $sticky_count;
          }

          $home_args = array(
             'post_type' => 'post',
             'posts_per_page' => $posts_amt,
             'post_status' => 'publish'


          );
          $home_query = new WP_Query( $home_args );

          while($home_query->have_posts()) : 
            $home_query->the_post();
            get_template_part('template-parts/content', 'archive');
          endwhile;
        }



        ?>

        
        </div>
        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_sidebar();
get_footer();
