<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '8|1t2Dn<CcE6x,1krLcts%?AmTa(B,5S:6II}sCP U2}{h|i~Y%SW?*6``:&TF+p' );
define( 'SECURE_AUTH_KEY',  'A&I^ulY<6]77Sq%V0Yb#`*(BItJ2 thN(Y@.vW=t[[;+-5W?pM`,{&fI)<tmYefM' );
define( 'LOGGED_IN_KEY',    '>mI:Tqwge&(`O*XhteM&>qQ<d,L~2a^5%<[blm:)apL?{*%=[S{^H`Ojq)in4r<E' );
define( 'NONCE_KEY',        '_0hwAt0o!LtulGPK~oesXAQu bBgENL1YlrZ!&f=IV606A0yUUQ;+p@;qBu0ib$8' );
define( 'AUTH_SALT',        'qamryF7od>/Th=`8w1w&4e~/!~LF)PCq}-M=;w@L>qui5#&HD?U&w|)lF}7xp(AL' );
define( 'SECURE_AUTH_SALT', 'p5)9`nk<2.i9&`v_y?#&-8sd]ovNW.QAP &q&]+T X>9t$HR:W>pptc{6gChN|F|' );
define( 'LOGGED_IN_SALT',   'vc[,WSd=Q_)T>GD#!<9BW~QU$9~iYU&VI9sR}!)e2 +4a:pGvSUeZzgOwKHsQ-TG' );
define( 'NONCE_SALT',       '{7fj`<rLsPfhgqy#2Dvy?w,#c=Kr0G2h`u#P~S8B@j}R_a+u5%p%HM!,w^,AbC@M' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
//
define( 'WP_DEBUG', true );
define( 'SAVEQUERIES',true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
