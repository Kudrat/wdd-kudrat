<?xml version="1.0" encoding="utf-8" ?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes" encoding="utf-8" />


<xsl:template match="/"><!--document root-->

 <!-- Create Simple HTML file -->
 <html>
    <head>
      <title>First XSL Experiment</title>
      <style>
         body{
           font-family: Tahoma, sans-serif;
           font-size: 16px;
         }
         
         table{
           border: 1px solid #000;
           border-collapse: collapse;
           width: 700px;
           
         }
         
         th,td{
           border: 1px solid #444;
           padding: 18px;
           verticle-align: top;
         }
         
         th{
           background-color: #333;
           color: #fff;
         }
         
         .title{
           font-weight: bold;
           font-size: 1.2em;
         }
         
      </style>
    </head>
    
    <body>
      <!--Regular "Static" HTML-->
      <h1> Qwerty News XSL Sheet</h1>
      <table>
        <!--Apply-Templates-->
        <xsl:apply-templates select="news-article"/>
        
      </table>
      
    </body>
 </html>
  
</xsl:template>

<xsl:template match="news-article">
  <tr>
    <th>Tag Line</th>
    <td colspan="4"><xsl:value-of select="tag-line" /></td>
  </tr>
  <tr>
    <th>Menu</th>
    <td><p><a href="{menu/link_url1}"><xsl:value-of select="menu/link_text1"/></a></p>
         Link: <xsl:value-of select="menu/link_url1"/></td>
    
    <td><p><a href="{menu/link_url2}"><xsl:value-of select="menu/link_text2"/></a></p>
         Link: <xsl:value-of select="menu/link_url2"/></td>
    
    <td><p><a href="{menu/link_url3}"><xsl:value-of select="menu/link_text3"/></a></p>
         Link: <xsl:value-of select="menu/link_url3"/></td>
    
    <td><p><a href="{menu/link_url4}"><xsl:value-of select="menu/link_text4"/></a></p>
         Link: <xsl:value-of select="menu/link_url4"/></td>
         
  </tr>
  <tr>
    <th>Breaking News</th>
    <td><strong><xsl:value-of select="breaking-news/para-heading1" /></strong>
        <br/><xsl:value-of select="breaking-news/description" /></td>
    <td colspan="3"><strong><xsl:value-of select="breaking-news/para-heading2" /></strong>
        <br/><xsl:value-of select="breaking-news/description2" /></td> 
  </tr>
  <tr>
    <th>News</th>
    <td colspan="2"> <xsl:apply-templates select="news1" /></td>
    <td colspan="2"> <xsl:apply-templates select="news2" /></td>
  </tr>
 
</xsl:template>

<xsl:template match="news1">
   <xsl:variable name="newslink" select="href-news"/>
   <p><a href="{$newslink}"><xsl:value-of select="title-news"/></a></p>
   <p><xsl:value-of select="intro"/> <a href="{$newslink}">(More)</a></p>
   <p><xsl:value-of select="$newslink"/></p>
   <p><xsl:value-of select="date"/></p>
</xsl:template>

<xsl:template match="news2">
   <xsl:variable name="newslink" select="href-news"/>
   <p><a href="{$newslink}"><xsl:value-of select="title-news"/></a></p>
   <p><xsl:value-of select="intro"/> <a href="{$newslink}">(More)</a></p>
   <p><xsl:value-of select="$newslink"/></p>
   <p><xsl:value-of select="date"/></p>
</xsl:template>





</xsl:stylesheet>

